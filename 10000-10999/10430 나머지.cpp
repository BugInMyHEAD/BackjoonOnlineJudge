// https://www.acmicpc.net/problem/10430

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // Test cases given by the text
  "5 8 4 "
);
static std::istringstream test_answer(
  // Given by the text
  "1 1 0 0 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static constexpr bool IS_TEST_CASE_COUNT_GIVEN = false;

static void precalculate() {
  // No op
}

static bool solve_case() {
  int aa = 0, bb = 0, cc = 0;
  in >> aa >> bb >> cc;
  if (!in) {
    return false;
  }

  out
    << (aa + bb) % cc << "\n"
    << (aa % cc + bb % cc) % cc << "\n"
    << (aa * bb) % cc << "\n"
    << ((aa % cc) * (bb % cc)) % cc << "\n";

  return true;
}

static void solve() {
  precalculate();
  if (IS_TEST_CASE_COUNT_GIVEN) {
    int test_case_count = 0;
    in >> test_case_count;
    for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
  } else {
    while (solve_case()) ;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}