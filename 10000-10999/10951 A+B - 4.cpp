// https://www.acmicpc.net/problem/10951

#include <iostream>

int main() {
  {
    int a, b;
    // 'explicit operator bool()' or 'operator void*()'
    // in basic_ios.h
    // https://en.cppreference.com/w/cpp/io/basic_ios/operator_bool
    while (std::cin >> a >> b) {
      // NOTE: print a whitespace for the judge to be able to distinguish
      // each result
      std::cout << a + b << "\n";
    }
  }
  return 0;
}