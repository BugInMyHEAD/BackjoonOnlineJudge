// https://www.acmicpc.net/problem/10950

#include <iostream>

void solve() {
  int a, b;
  std::cin >> a >> b;
  // NOTE: print a whitespace for the judge to be able to distinguish
  // each result
  std::cout << a + b << std::endl;
}

int main() {
  int test_case_count;
  std::cin >> test_case_count;
  for (int i1 = 0; i1 < test_case_count; ++i1) {
    solve();
  }
  return 0;
}