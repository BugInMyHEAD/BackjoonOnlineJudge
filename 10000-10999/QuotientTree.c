#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

void problem(void);
void solve(void);

void putln(void)
{
	puts("");
}

int main(void)
{
	problem();

	return 0;
}

void problem(void)
{
	int testCase;
	scanf("%d", &testCase);

	for (int i1 = 1; i1 <= testCase; i1++)
	{
		//printf("Case #%d: ", i1);
		solve();
	}
}

typedef unsigned long long common_t;

typedef struct Quotient Quotient;
struct Quotient
{
	common_t p, q;
};

Quotient seekPQ(common_t s)
{
	if (1 == s)
	{
		Quotient one = { 1, 1 };

		return one;
	}

	Quotient parent = seekPQ(s / 2);
	Quotient retVal;
	if (s % 2)
	{
		retVal.p = parent.p + parent.q;
		retVal.q = parent.q;
	}
	else
	{
		retVal.p = parent.p;
		retVal.q = parent.p + parent.q;
	}

	return retVal;
}

common_t seekN(Quotient quotient)
{
	if (1 == quotient.p && 1 == quotient.q)
	{
		return 1;
	}

	Quotient tmp;
	common_t retVal;
	if (quotient.p < quotient.q)
	{
		tmp.p = quotient.p;
		tmp.q = quotient.q - quotient.p;
		retVal = 2 * seekN(tmp);
	}
	else
	{
		tmp.p = quotient.p - quotient.q;
		tmp.q = quotient.q;
		retVal = 2 * seekN(tmp) + 1;
	}

	return retVal;
}

Quotient seekNextPQ(Quotient quotient)
{
	Quotient retVal;

	if (1 == quotient.q)
	{
		retVal.p = 1;
		retVal.q = quotient.p + 1;
	}
	else
	{
		common_t ii = quotient.p / quotient.q;
		common_t jj = quotient.p - quotient.q * ii;
		//common_t jj = quotient.p % quotient.q;
		retVal.p = quotient.q;
		retVal.q = quotient.q - jj + ii * quotient.q;
	}

	return retVal;
}

void solve(void)
{
	Quotient quotient;
	int numFromUser;
	scanf("%d%lld%*c%lld", &numFromUser, &quotient.p, &quotient.q);

	quotient = seekNextPQ(quotient);
	printf("%d %lld/%lld\n", numFromUser, quotient.p, quotient.q);
}