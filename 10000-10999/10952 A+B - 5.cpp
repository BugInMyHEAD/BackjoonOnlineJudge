// https://www.acmicpc.net/problem/10952

#include <iostream>

int main() {
  while (true) {
    int a, b;
    std::cin >> a >> b;
    // when the exit condition is given, don't print.
    if (a == 0 && b == 0) {
      break;
    }
    std::cout << a + b << std::endl;
  }
  return 0;
}