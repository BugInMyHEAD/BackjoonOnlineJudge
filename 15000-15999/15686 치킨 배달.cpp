﻿// https://www.acmicpc.net/problem/15686
// 치킨 배달


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 3 "
  "0 0 1 0 0 "
  "0 0 2 0 1 "
  "0 1 2 0 0 "
  "0 0 1 0 0 "
  "0 0 0 0 2 "

  "5 2 "
  "0 2 0 1 0 "
  "1 0 1 0 0 "
  "0 0 0 0 0 "
  "2 0 0 1 1 "
  "2 2 0 1 2 "

  "5 1 "
  "1 2 0 0 0 "
  "1 2 0 0 0 "
  "1 2 0 0 0 "
  "1 2 0 0 0 "
  "1 2 0 0 0 "

  "5 1 "
  "1 2 0 2 1 "
  "1 2 0 2 1 "
  "1 2 0 2 1 "
  "1 2 0 2 1 "
  "1 2 0 2 1 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "5 10 11 32 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

using Coord2 = std::pair<Int, Int>;

static constexpr Int WIDTH_LIMIT = 52;
static constexpr Int DISTANCE_INF = std::numeric_limits<Int>::max() / 2;

static Int grid_width; // N
static Int chicken_max; // M
static std::vector<Coord2> homes;
static std::vector<Coord2> chickens;

static std::vector<Coord2> selected_chickens;


static Int measure_distance(const Coord2& a, const Coord2& b) {
  return std::abs(b.first - a.first) + std::abs(b.second - a.second);
}


static Int dfs(Int chicken_total, Int level) {
  if (chicken_total < level) {
    return DISTANCE_INF;
  }

  if (level <= 0) {
    Int result = 0;
    for (const auto& home : homes) {
      Int home_result = DISTANCE_INF;
      for (const auto& chicken : selected_chickens) {
        home_result = std::min(home_result, measure_distance(home, chicken));
      }
      result += home_result;
    }
    return result;
  }

  Int result = DISTANCE_INF;
  for (Int chickens_idx = 0; chickens_idx < chicken_total; ++chickens_idx) {
    selected_chickens.push_back(chickens.at(chickens_idx));
    result = std::min(result, dfs(chickens_idx, level - 1));
    selected_chickens.pop_back();
  }
  return result;
}


static Int invoke_dfs() {
  selected_chickens.clear();

  return dfs(Int(chickens.size()), chicken_max);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  grid_width = chicken_max = -1;
  homes.clear();
  chickens.clear();

  in >> grid_width >> chicken_max;
  if (!in) {
    return false;
  }
  assert(0 <= chicken_max);

  for (Int row_cnt = 1; row_cnt <= grid_width; ++row_cnt) {
    for (Int col_cnt = 1; col_cnt <= grid_width; ++col_cnt) {
      Int cell = -1;
      in >> cell;
      assert(0 <= cell);

      switch (cell) {
        case 1 : homes.push_back({ row_cnt, col_cnt }); break;
        case 2 : chickens.push_back({ row_cnt, col_cnt }); break;
      }
    }
  }

  const auto result = invoke_dfs();
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}