// https://www.acmicpc.net/problem/13459
// https://www.acmicpc.net/problem/13460
// https://www.acmicpc.net/problem/15644
// https://www.acmicpc.net/problem/15653

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <queue>
#include <stdexcept>

#define PROBLEM 15653

#if PROBLEM != 13459 && PROBLEM != 13460 && PROBLEM != 15644 && PROBLEM != 15653
#error Illegal problem number
#endif

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  "5 5 "
  "##### "
  "#..B# "
  "#.#.# "
  "#RO.# "
  "##### "

  "7 7 "
  "####### "
  "#...RB# "
  "#.##### "
  "#.....# "
  "#####.# "
  "#O....# "
  "####### "

  "7 7 "
  "####### "
  "#..R#B# "
  "#.##### "
  "#.....# "
  "#####.# "
  "#O....# "
  "####### "

#if PROBLEM == 15653
  "10 10 "
  "########## "
  "#R#...##B# "
  "#...#.##.# "
  "#####.##.# "
  "#......#.# "
  "#.######.# "
  "#.#...##.# "
  "#.#.#.#..# "
  "#...#.O#.# "
  "########## "
#else // PROBLEM == 15653
  "10 10 "
  "########## "
  "#R#...##B# "
  "#...#.##.# "
  "#####.##.# "
  "#......#.# "
  "#.######.# "
  "#.#....#.# "
  "#.#.#.#..# "
  "#...#.O#.# "
  "########## "
#endif // PROBLEM == 15653

  "3 7 "
  "####### "
  "#R.O.B# "
  "####### "

  "10 10 "
  "########## "
  "#R#...##B# "
  "#...#.##.# "
  "#####.##.# "
  "#......#.# "
  "#.######.# "
  "#.#....#.# "
  "#.#.##...# "
  "#O..#....# "
  "########## "

  "3 10 "
  "########## "
  "#.O....RB# "
  "########## "
);

static std::istringstream test_answer(
#if PROBLEM == 13459
  "1 1 1 0 1 1 0 "
#elif PROBLEM == 13460
  "1 5 5 -1 1 7 -1 "
#elif PROBLEM == 15644
  "1 R "
  "5 LDRDL "
  "5 LDRDL "
  "-1 "
  "1 R "
  "7 DRURDLD "
  "-1 "
#elif PROBLEM == 15653
  "1 5 5 12 1 7 -1 "
#endif
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

struct Marble {
  char collision = '.';
  Point2D<int> position;
};

struct MarbleState {
  Marble red;
  Marble blue;

  bool is_red_upper_than_blue() const {
    return red.position.y < blue.position.y;
  }

  bool is_red_left_of_blue() const {
    return red.position.x < blue.position.x;
  }
};

static std::array<std::array<char, 10>, 10> maze;
// red.position.y, red.position.x, blue.position.y, blue.position.x
static std::array<std::array<std::array<std::array<std::string, 10>, 10>, 10>, 10> paths;
static std::queue<MarbleState> que;

static std::string& paths_ms(const MarbleState& ms) {
  return paths[ms.red.position.y][ms.red.position.x][ms.blue.position.y][ms.blue.position.x];
}

static Marble collide(const Marble& current, const Point2D<int>& offset) {
  auto result_position = current.position;
  auto next_position = current.position;

  do {
    result_position = next_position;
    next_position += offset;
  } while (maze[next_position.y][next_position.x] == '.');

  if (maze[next_position.y][next_position.x] == 'O') {
    result_position = next_position;
  }

  return { maze[next_position.y][next_position.x], result_position };
}

static MarbleState tilt(const MarbleState& ms, char direction) {
  Point2D<int> offset;
  switch (direction) {
    case 'U' : offset = { -1, +0 }; break;
    case 'D' : offset = { +1, +0 }; break;
    case 'L' : offset = { +0, -1 }; break;
    case 'R' : offset = { +0, +1 }; break;
    default : throw std::logic_error("switch case exception"); break;
  }
  auto next_red = collide(ms.red, offset);
  auto next_blue = collide(ms.blue, offset);
  if (next_red.position == next_blue.position && next_red.collision != 'O') {
    if (offset.y != 0) {
      const bool has_red_collided_into_blue = offset.y < 0 ^ ms.is_red_upper_than_blue();
      auto& next = has_red_collided_into_blue ? next_red : next_blue;
      next.position.y -= offset.y;
      next.collision = has_red_collided_into_blue ? 'B' : 'R';
    }
    if (offset.x != 0) {
      const bool has_red_collided_into_blue = offset.x < 0 ^ ms.is_red_left_of_blue();
      auto& next = has_red_collided_into_blue ? next_red : next_blue;
      next.position.x -= offset.x;
      next.collision = has_red_collided_into_blue ? 'B' : 'R';
    }
  }
  return { next_red, next_blue };
}

static void prepare_bfs(const MarbleState& ms) {
  while (!que.empty()) {
    que.pop();
  }
  paths_ms(ms) = "F"; // F means 'flat', which is the initial state.
  que.emplace(ms);
}

template <typename S, typename T>
static int invoke_bfs(S s, T t) {
  while (!que.empty()) {
    if (que.front().red.collision == 'O' && que.front().blue.collision == '#') {
      break;
    }
    constexpr std::array<char, 4> udlr { 'U', 'D', 'L', 'R' };
    if (que.front().blue.collision != 'O') for (const auto& udlr_el : udlr) {
      const auto next_ms = tilt(que.front(), udlr_el);
      if (paths_ms(next_ms).empty() && s(paths_ms(que.front()).size() - 1)) {
        paths_ms(next_ms) = paths_ms(que.front()) + udlr_el;
        que.emplace(next_ms);
      }
    }
    que.pop();
  }
  // In the latter expression, exclude starting point token 'S'.
  return t(que.empty() ? -1 : paths_ms(que.front()).size() - 1);
}

static bool limit(size_t s) {
#if PROBLEM == 15653
  return true;
#else
  return s < 10;
#endif
}

static int represent(int i) {
#if PROBLEM == 13459
  return i >= 0;
#else
  return i;
#endif
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  int row_total = 0;
  int col_total = 0;
  in >> row_total >> col_total;
  if (!in) {
    return false;
  }

  // Initialize `maze`.
  for (auto& maze_el : maze) {
    maze_el.fill('.');
  }
  // Initialize `paths`
  for (auto& paths_el : paths) {
    for (auto& paths_el_el : paths_el) {
      for (auto& paths_el_el_el : paths_el_el) {
        paths_el_el_el.fill("");
      }
    }
  }

  // Load the map in `maze`.
  MarbleState ms;
  for (int row_count = 0; row_count < row_total; ++row_count) {
    for (int col_count = 0; col_count < col_total; ++col_count) {
      char terrain;
      in >> terrain;
      switch (terrain)
      {
        case 'R' : {
          ms.red.position = { row_count, col_count };
        } break;
        case 'B' : {
          ms.blue.position = { row_count, col_count };
        } break;
        default: {
          maze[row_count][col_count] = terrain;
        } break;
      }
    }
    // Flush line feed character (not guaranteed).
    in.ignore(1);
  }

  prepare_bfs(ms);
  const auto move_total = invoke_bfs(limit, represent);
  out << move_total << "\n";

#if PROBLEM == 15644
  if (move_total > 0) {
    out << paths_ms(que.front()).c_str() + 1 << "\n";
  }
#endif

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}