﻿// https://www.acmicpc.net/problem/15684
// 사다리 조작


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "2 0 3 "

  "2 1 3 "
  "1 1 "

  "5 5 6 "
  "1 1 "
  "3 2 "
  "2 3 "
  "5 1 "
  "5 4 "

  "6 5 6 "
  "1 1 "
  "3 2 "
  "1 3 "
  "2 5 "
  "5 5 "

  "5 8 6 "
  "1 1 "
  "2 2 "
  "3 3 "
  "4 4 "
  "3 1 "
  "4 2 "
  "5 3 "
  "6 4 "

  "5 12 6 "
  "1 1 "
  "1 3 "
  "2 2 "
  "2 4 "
  "3 1 "
  "3 3 "
  "4 2 "
  "4 4 "
  "5 1 "
  "5 3 "
  "6 2 "
  "6 4 "

  "5 6 6 "
  "1 1 "
  "3 1 "
  "5 2 "
  "4 3 "
  "2 3 "
  "1 4 "

  // Custom test cases
  "10 0 30 "

  "10 5 30 "
  "1 1 "
  "1 3 "
  "1 5 "
  "1 7 "
  "1 9 "

  "4 3 4 "
  "1 1 "
  "2 2 "
  "1 3 "
);

static std::istringstream test_answer(
  // Given by the text
  "0 1 3 3 -1 -1 2 "

  // Custom
  "0 -1 3 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;


static constexpr Int VERTICAL_LINE_LIMIT = 12;
static constexpr Int HORIZONTAL_DOT_LINE_LIMIT = 32;

static constexpr Int DOT_LINE = 0;
static constexpr Int LINE = 3;

template <typename T>
using MatrixRow = std::array<T, VERTICAL_LINE_LIMIT>;
template <typename T>
using MatrixBase = std::array<MatrixRow<T>, HORIZONTAL_DOT_LINE_LIMIT>;


template <typename T>
class Matrix : public MatrixBase<T> {

public:

  using MatrixBase<T>::at;

  T& at(Int row, Int col) {
    return MatrixBase<T>::at(row).at(col);
  }
};


static Int vertical_line_total;
static Int horizontal_line_total;
static Int horizontal_dot_line_total;
static Matrix<Int> ladder;

static MatrixRow<Int> ladder_result;
static MatrixRow<Int> target_ladder_result;


static Int dfs(
  Int horizontal_dot_line_begin_number, Int horizontal_line_count, Int level
) {
  bool success = true;
  for (
    Int vertical_line_number = 1;
    vertical_line_number <= vertical_line_total;
    ++vertical_line_number
    ) {
    Int position = vertical_line_number;
    for (
      Int horizontal_dot_line_number = 1;
      horizontal_dot_line_number <= horizontal_dot_line_total;
      ++horizontal_dot_line_number
      ) {
      const auto& left = ladder.at(horizontal_dot_line_number, position - 1);
      const auto& right = ladder.at(horizontal_dot_line_number, position);
      assert(left != LINE || right != LINE);
      if (left == LINE) {
        --position;
      } else if (right == LINE) {
        ++position;
      }
    }
    if (position != vertical_line_number) {
      success = false;
      break;
    }
  }
  if (success) {
    return horizontal_line_count;
  }

  if (level <= horizontal_line_count) {
    return std::numeric_limits<Int>::max();
  }

  Int result = std::numeric_limits<Int>::max();
  
  for (
    Int vertical_line_number = 1;
    vertical_line_number + 1 <= vertical_line_total;
    ++vertical_line_number
    ) {
    for (
      Int horizontal_dot_line_number = horizontal_dot_line_begin_number;
      horizontal_dot_line_number <= horizontal_dot_line_total;
      ++horizontal_dot_line_number
      ) {
      auto& horizontal_dot_line = ladder.at(horizontal_dot_line_number, vertical_line_number);

      if (horizontal_dot_line != DOT_LINE) {
        continue;
      }

      auto& left_horizontal_dot_line =
        ladder.at(horizontal_dot_line_number, vertical_line_number - 1);
      auto& right_horizontal_dot_line =
        ladder.at(horizontal_dot_line_number, vertical_line_number + 1);

      horizontal_dot_line = LINE;
      ++left_horizontal_dot_line;
      ++right_horizontal_dot_line;

      const auto child_result =
        dfs(
          horizontal_dot_line_number,
          horizontal_line_count + 1, std::min(level, result)
        );

      horizontal_dot_line = DOT_LINE;
      --left_horizontal_dot_line;
      --right_horizontal_dot_line;

      result = std::min(result, child_result);
      if (result == horizontal_line_count + 1) {
        return result;
      }

      while (
        ++horizontal_dot_line_number <= horizontal_dot_line_total
        && ladder.at(horizontal_dot_line_number, vertical_line_number) == DOT_LINE
        );
    }
  }

  return result;
}


static Int invoke_dfs(Int level) {
  return dfs(1, 0, level);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  vertical_line_total = horizontal_line_total = horizontal_dot_line_total = -1;
  for (auto& horizontal_line_checks : ladder) {
    horizontal_line_checks.fill(DOT_LINE);
  }
  std::iota(target_ladder_result.begin(), target_ladder_result.end(), 0);

  in >> vertical_line_total >> horizontal_line_total >> horizontal_dot_line_total;
  if (!in) {
    return false;
  }
  assert(0 <= vertical_line_total && 0 <= horizontal_line_total && 0 <= horizontal_dot_line_total);

  for (
    Int horizontal_line_count = 0;
    horizontal_line_count < horizontal_line_total;
    ++horizontal_line_count
    ) {
    Int horizontal_dot_line_number = -1;
    Int left_vertical_line_number = -1;
    in >> horizontal_dot_line_number >> left_vertical_line_number;
    assert(0 <= horizontal_dot_line_number && 0 <= left_vertical_line_number);

    ladder.at(horizontal_dot_line_number, left_vertical_line_number) = LINE;
    ++ladder.at(horizontal_dot_line_number, left_vertical_line_number - 1);
    ++ladder.at(horizontal_dot_line_number, left_vertical_line_number + 1);
  }

  const auto result = invoke_dfs(3);
  out << (result > 3 ? -1 : result) << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}