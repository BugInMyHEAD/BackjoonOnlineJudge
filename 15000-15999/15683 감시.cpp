// https://www.acmicpc.net/problem/15683
// 감시


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 6 "
  "0 0 0 0 0 0 "
  "0 0 0 0 0 0 "
  "0 0 1 0 6 0 "
  "0 0 0 0 0 0 "

  "6 6 "
  "0 0 0 0 0 0 "
  "0 2 0 0 0 0 "
  "0 0 0 0 6 0 "
  "0 6 0 0 2 0 "
  "0 0 0 0 0 0 "
  "0 0 0 0 0 5 "

  "6 6 "
  "1 0 0 0 0 0 "
  "0 1 0 0 0 0 "
  "0 0 1 0 0 0 "
  "0 0 0 1 0 0 "
  "0 0 0 0 1 0 "
  "0 0 0 0 0 1 "

  "6 6 "
  "1 0 0 0 0 0 "
  "0 1 0 0 0 0 "
  "0 0 1 5 0 0 "
  "0 0 5 1 0 0 "
  "0 0 0 0 1 0 "
  "0 0 0 0 0 1 "

  "1 7 "
  "0 1 2 3 4 5 6 "

  "3 7 "
  "4 0 0 0 0 0 0 "
  "0 0 0 2 0 0 0 "
  "0 0 0 0 0 0 4 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "20 15 6 2 0 0 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;


static constexpr Int WIDTH_LIMIT = 10;
static constexpr Int CAMERA_TYPE_LIMIT = 6;

static constexpr Int EMPTY = 0;
static constexpr Int WALL = 6;
static constexpr Int SIGHT = 7;

using MatrixRow = std::array<Int, WIDTH_LIMIT>;
using MatrixBase = std::array<MatrixRow, WIDTH_LIMIT>;


struct Coordinate {
  Int row = 0;
  Int col = 0;

  Coordinate operator + (const Coordinate& other) const {
    return { row + other.row, col + other.col };
  }

  Coordinate& operator += (const Coordinate& other) {
    return *this = *this + other;
  }
};


class Matrix : public MatrixBase {

public:

  using MatrixBase::at;

  Int& at(Int row, Int col) {
    return MatrixBase::at(row).at(col);
  }

  Int& at(const Coordinate& c) {
    return at(c.row, c.col);
  }
};


static constexpr Coordinate UP { -1, 0 };
static constexpr Coordinate DOWN { 1, 0 };
static constexpr Coordinate LEFT { 0, -1 };
static constexpr Coordinate RIGHT { 0, 1 };

static std::array<std::vector<std::vector<Coordinate>>, CAMERA_TYPE_LIMIT> camera_types {
  std::vector<std::vector<Coordinate>>
  {
  },
  {
    std::vector
    { UP }, { DOWN }, { LEFT }, { RIGHT }
  },
  {
    std::vector
    { UP, DOWN }, { LEFT, RIGHT }
  },
  {
    std::vector
    { UP, LEFT }, { UP, RIGHT }, { DOWN, LEFT }, { DOWN, RIGHT }
  },
  {
    std::vector
    { UP, DOWN, LEFT }, { UP, DOWN, RIGHT }, { LEFT, RIGHT, UP }, { LEFT, RIGHT, DOWN }
  },
  {
    std::vector
    { UP, DOWN, LEFT, RIGHT }
  }
};


static Coordinate matrix_size;
static Matrix matrix;
static std::vector<std::pair<Int, Coordinate>> cameras;


static Int dfs(const Matrix& acc, Int level) {
  if (level <= 0) {
    Int empty_total = 0;
    for (Int row_count = 1; row_count <= matrix_size.row; ++row_count) {
      const auto& row = acc.at(row_count);
      empty_total += Int(std::count(row.cbegin() + 1, row.cbegin() + 1 + matrix_size.col, EMPTY));
    }
    return empty_total;
  }

  Int result = WIDTH_LIMIT * WIDTH_LIMIT;
  const auto next_level = level - 1;
  const auto& [camera_types_idx, camera_coordinate] = cameras.at(next_level);
  for (const auto& directions : camera_types.at(camera_types_idx)) {
    auto next_acc = acc;
    for (const auto& direction : directions) {
      auto sight_coordinate = camera_coordinate + direction;
      while (next_acc.at(sight_coordinate) != WALL) {
        next_acc.at(sight_coordinate) = SIGHT;
        sight_coordinate += direction;
      }
    }
    result = std::min(result, dfs(next_acc, next_level));
  }
  return result;
}


static Int invoke_dfs(Int level = cameras.size()) {
  return dfs(matrix, level);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  matrix_size = { -1, -1 };
  for (auto& row : matrix) {
    row.fill(WALL);
  }
  cameras.clear();

  in >> matrix_size.row >> matrix_size.col;
  if (!in) {
    return false;
  }
  assert(0 <= matrix_size.row && 0 <= matrix_size.col);

  for (Int row_count = 1; row_count <= matrix_size.row; ++row_count) {
    for (Int col_count = 1; col_count <= matrix_size.col; ++col_count) {
      auto& cell = matrix.at(row_count, col_count);
      in >> cell;
      if (cell != EMPTY && cell != WALL) {
        cameras.push_back({ cell, { row_count, col_count } });
      }
    }
  }
  
  const auto result = invoke_dfs();
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}