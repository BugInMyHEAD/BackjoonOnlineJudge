﻿// https://www.acmicpc.net/problem/15685
// 드래곤 커브


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 "
  "3 3 0 1 "
  "4 2 1 3 "
  "4 2 2 1 "

  "4 "
  "3 3 0 1 "
  "4 2 1 3 "
  "4 2 2 1 "
  "2 7 3 4 "

  "10 "
  "5 5 0 0 "
  "5 6 0 0 "
  "5 7 0 0 "
  "5 8 0 0 "
  "5 9 0 0 "
  "6 5 0 0 "
  "6 6 0 0 "
  "6 7 0 0 "
  "6 8 0 0 "
  "6 9 0 0 "

  "4 "
  "50 50 0 10 "
  "50 50 1 10 "
  "50 50 2 10 "
  "50 50 3 10 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "4 11 8 1992 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;


static constexpr Int WIDTH_LIMIT = 101;
static constexpr Int GENERATION_LIMIT = 10;


struct Coord2 {
  Int y = 0;
  Int x = 0;

  Coord2 operator + (const Coord2& other) {
    return { y + other.y, x + other.x };
  }

  Coord2 operator += (const Coord2& other) {
    return *this = *this + other;
  }
};


template <typename T>
using MatrixRow = std::array<T, WIDTH_LIMIT>;
template <typename T>
using MatrixBase = std::array<MatrixRow<T>, WIDTH_LIMIT>;


template <typename T>
class Matrix : public MatrixBase<T> {

public:

  using MatrixBase<T>::at;

  constexpr T& at(Int row, Int col) {
    return MatrixBase<T>::at(row).at(col);
  }

  constexpr T& at(const Coord2& c) {
    return at(c.y, c.x);
  }
};


static Int curve_total;
static Matrix<bool> grid;
static std::array<Coord2, 1 << GENERATION_LIMIT> directions;


static constexpr Coord2 ccw(const Coord2& c) {
  return { -c.x, c.y };
}


static constexpr Coord2 ccw(Coord2 c, Int ccw_total) {
  for (Int ccw_count = 0; ccw_count < ccw_total; ++ccw_count) {
    c = ccw(c);
  }
  return c;
}


static constexpr Int get_directions_size(Int generation) {
  return 1 << generation;
}


static void draw_curve(Coord2 coord, Int ccw_total, Int generation) {
  grid.at(coord) = true;
  const auto directions_size = get_directions_size(generation);
  for (Int directions_idx = 0; directions_idx < directions_size; ++directions_idx) {
    coord += ccw(directions.at(directions_idx), ccw_total);
    grid.at(coord) = true;
  }
}


static void precalculate() {
  directions.at(0) = { 0, 1 };
  for (Int generation = 0; generation < GENERATION_LIMIT; ++generation) {
    const auto directions_size = get_directions_size(generation);
    for (Int direction_count = 0; direction_count < directions_size; ++direction_count) {
      const auto& previous_direction = directions.at(directions_size - 1 - direction_count);
      auto& next_direction = directions.at(directions_size + direction_count);
      next_direction = ccw(previous_direction);
    }
  }
}


static bool solve_case(int test_case_count) {
  curve_total = -1;
  for (auto& row : grid) {
    row.fill(false);
  }

  in >> curve_total;
  if (!in) {
    return false;
  }
  assert(0 <= curve_total);

  for (
    Int curve_count = 0;
    curve_count < curve_total;
    ++curve_count
    ) {
    Coord2 coord = { -1, -1 };
    Int direction = -1;
    Int generation = -1;
    in >> coord.x >> coord.y >> direction >> generation;
    assert(0 <= coord.x && 0 <= coord.y && 0 <= direction && 0 <= generation);

    draw_curve(coord, direction, generation);
  }

  Int result = 0;
  for (Int row = 0; row + 1 < WIDTH_LIMIT; ++row) {
    for (Int col = 0; col + 1 < WIDTH_LIMIT; ++col) {
      if (
        grid.at(row, col) && grid.at(row, col + 1)
        && grid.at(row + 1, col) && grid.at(row + 1, col + 1)
        ) {
        ++result;
      }
    }
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}