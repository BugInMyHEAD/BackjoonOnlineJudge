#include <iostream>
#include <sstream>
#include <cassert>


using namespace std;


int main()
{
	double a = 8603356474445742881;
	long long b = 8603356474445742881;
	float c = 8603356474445742881;
	if (a == b && b == c && c != a) {
		cout << "true" << '\n';
	} else {
		cout << "false" << '\n';
	}

	string line = "8603356474445742881";
	istringstream sin(line);
	float temp;
	assert(sin >> temp);
	cout << temp << endl;

	return 0;
}