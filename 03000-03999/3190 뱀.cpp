// https://www.acmicpc.net/problem/3190
// 뱀


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  // #0
  "6 "
  "3 "
  "3 4 "
  "2 5 "
  "5 3 "
  "3 "
  "3 D "
  "15 L "
  "17 D "

  // #1
  "10 "
  "4 "
  "1 2 "
  "1 3 "
  "1 4 "
  "1 5 "
  "4 "
  "8 D "
  "10 D "
  "11 D "
  "13 L "

  // #2
  "10 "
  "5 "
  "1 5 "
  "1 3 "
  "1 2 "
  "1 6 "
  "1 7 "
  "4 "
  "8 D "
  "10 D "
  "11 D "
  "13 L "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "9 21 13 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

constexpr Int WIDTH_LIMIT = 102;
constexpr Int TIME_LIMIT = 10'101;
constexpr auto DISTANCE_INF = std::numeric_limits<Int>::max() / 2;

enum class Cell { EMPTY, WALL, APPLE, SNAKE };

using Arr1 = std::array<Cell, WIDTH_LIMIT>; // col
using Arr2 = std::array<Arr1, WIDTH_LIMIT>; // row

static Int width; // N
static Int apple_total; // K
static Int command_total; // L
static Arr2 board;

static Int elapsed_time;
static Int snake_head_row;
static Int snake_head_col;
static Int snake_row_offset;
static Int snake_col_offset;
static bool snake_alive;
static std::queue<std::pair<Int, Int>> snake_row_cols;


static void board_initialize() {
  for (auto& row : board) {
    row.fill(Cell::EMPTY);
  }
  board.at(0).fill(Cell::WALL);
  board.at(width + 1).fill(Cell::WALL);
  for (auto& row : board) {
    row.at(0) = Cell::WALL;
    row.at(width + 1) = Cell::WALL;
  }
  board.at(1).at(1) = Cell::SNAKE;

  elapsed_time = 0;
  snake_head_row = 1;
  snake_head_col = 1;
  snake_row_offset = 0;
  snake_col_offset = 1;
  snake_alive = true;
  while (!snake_row_cols.empty()) {
    snake_row_cols.pop();
  }
  snake_row_cols.emplace(1, 1);
}


static Int snake_move(Int time) {
  while (snake_alive && elapsed_time < time) {
    assert(snake_row_offset ^ snake_col_offset);
    const auto velocity = snake_row_offset + snake_col_offset;
    assert(velocity == 1 || velocity == -1);

    snake_head_row += snake_row_offset;
    snake_head_col += snake_col_offset;
    auto& cell = board.at(snake_head_row).at(snake_head_col);

    switch (cell) {
      case Cell::EMPTY : {
        const auto tail = snake_row_cols.front();
        snake_row_cols.pop();
        board.at(tail.first).at(tail.second) = Cell::EMPTY;
      } break;
      case Cell::APPLE :
        // No op
      break;
      default :
        snake_alive = false;
      break;
    }

    cell = Cell::SNAKE;
    snake_row_cols.emplace(snake_head_row, snake_head_col);

    ++elapsed_time;
  }

  return elapsed_time;
}


static void record_command(Int time, char command) {
  assert(command == 'L' || command == 'D');
  snake_move(time);
  if (snake_row_offset == 0) {
    snake_row_offset = snake_col_offset * (command == 'L' ? -1 : 1);
    snake_col_offset = 0;
  } else {
    snake_col_offset = snake_row_offset * (command == 'L' ? 1 : -1);
    snake_row_offset = 0;
  }
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = -1;
  apple_total = -1;
  command_total = -1;

  in >> width >> apple_total;
  if (!in) {
    return false;
  }
  assert(width > 0 && apple_total >= 0);

  board_initialize();

  for (Int apple_count = 0; apple_count < apple_total; ++apple_count) {
    Int row = -1;
    Int col = -1;
    in >> row >> col;
    assert(row > 0 && col > 0);
    board.at(row).at(col) = Cell::APPLE;
  }

  in >> command_total;
  assert(command_total > 0);

  for (Int command_count = 0; command_count < command_total; ++command_count) {
    Int time = -1;
    std::string command;
    in >> time >> command;
    assert(time > 0 && command.size() == 1);
    record_command(time, command.at(0));
  }

  out << snake_move(TIME_LIMIT) << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}