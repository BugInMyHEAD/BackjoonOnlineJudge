// https://www.acmicpc.net/problem/1546

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // Test cases given by the text
  "3\n"
  "40 80 60\n"
  "3\n"
  "10 20 30\n"
  "4\n"
  "1 100 100 100\n"
  "5\n"
  "1 2 4 8 16\n"
  "2\n"
  "3 10\n"
);
static std::istringstream test_answer(
  // Given by the text
  "75.00\n"
  "66.666667\n"
  "75.25\n"
  "38.75\n"
  "65.00\n"
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

enum class TestFormat { ONE, GIVEN, CONDITION };
static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

std::array<int, 1000> arr;

static void precalculate() {
  // No op
}

static bool solve_case() {
  int subject_count = 0;
  in >> subject_count;
  if (!in) {
    return false;
  }

  for (int subject_index = 0; subject_index < subject_count; ++subject_index) {
    in >> arr[subject_index];
  }

  double score_max =
    *std::max_element(arr.cbegin(), arr.cbegin() + subject_count);
  auto score_sum =
    std::accumulate(arr.cbegin(), arr.cbegin() +subject_count, 0);
  auto result =
    100 * score_sum / (score_max * subject_count);
  out << result << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}