#include <iostream>
#include <algorithm>
#include <array>
#include <limits>
#include <deque>


std::array<int, 1000001> arr;


int _sol(int i)
{
	if (arr[i] >= 0)
	{
		return arr[i];
	}

	int div3 = i % 3 ? std::numeric_limits<int>::max() : _sol(i / 3);
	int div2 = i % 2 ? std::numeric_limits<int>::max() : _sol(i / 2);
	int sub1 = _sol(i - 1);

	return arr[i] = std::min(div3, std::min(div2, sub1)) + 1;
}


int sol(int i)
{
	for (int i1 = 1; i1 <= i; ++i1) _sol(i1);
	return _sol(i);
}


int main(int argc, char** argv)
{
	arr.fill(-1);
	arr[1] = 0;

	int i0;
	std::cin >> i0;
	std::cout << sol(i0);
}