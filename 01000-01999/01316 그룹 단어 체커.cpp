// https://www.acmicpc.net/problem/1316

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (nothing is given by the text)


  // Test cases given by the text
  "3 "
  "happy "
  "new "
  "year "

  "4 "
  "aba "
  "abab "
  "abcabc "
  "a "

  // Custom test cases

);
static std::istringstream test_answer(
  // Given by the text
  "3 "
  "1 "

  // Custom

);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static void precalculate() {
  // No op
}

static bool solve_case() {
  int word_count;
  in >> word_count;
  if (!in) {
    return false;
  }

  int group_word_count = 0;

  for (int word_index = 0; word_index < word_count; ++word_index) {
    std::string word;
    in >> word;
    std::array<bool, 'z' - 'a' + 1> arr { false };
    int inc = 1;
    char last = '\0';
    for (const auto& word_el : word) {
      if (last != word_el) {
        auto& arr_el = arr[word_el - 'a'];
        if (arr_el) {
          inc = 0;
          break;
        }
        arr_el = true;
      }
      last = word_el;
    }
    group_word_count += inc;
  }

  out << group_word_count;
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}