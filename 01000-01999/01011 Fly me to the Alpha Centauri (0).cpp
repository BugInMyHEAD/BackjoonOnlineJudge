// https://www.acmicpc.net/problem/1011
// This solution is easier to understand than (1).

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (3 is given by the text)
  "3 "

  // Test cases given by the text
  "0 3 "
  "1 5 "
  "45 50 "

  // Custom test cases
  " "
);
static std::istringstream test_answer(
  // Given by the text
  "3 3 4 "

  // Custom
  " "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static void precalculate() {
  // No op
}

static bool solve_case() {
  int xx = 0; // x
  int yy = 0; // y
  in >> xx >> yy;
  if (!in) {
    return false;
  }

  const auto distance = yy - xx;

  // Sum of arithmetic sequence
  // aa * (1 + aa) / 2

  // Minimum moving distance for the top speed, aa
  // (aa * (1 + aa) / 2) + ((aa - 1) * (1 + (aa - 1)) / 2)
  // == aa * aa

  // He can move (aa * aa) light years in (2 * aa - 1) unit time,
  // can move (aa * (aa + 1)) light years at most
  // in (2 * aa) unit time,
  // and can move ((aa + 1) * (aa + 1) - 1) light years at most
  // in (2 * aa + 1) unit time.

  // The top speed for the given distance
  const auto top_speed = static_cast<int>(::floor(sqrt(distance)));
  if (distance == top_speed * top_speed) {
    out << 2 * top_speed - 1;
  } else if (distance <= top_speed * (top_speed + 1)) {
    out << 2 * top_speed;
  } else {
    out << 2 * top_speed + 1;
  }
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}