// https://www.acmicpc.net/problem/1753
// 최단경로

// Updating distance before poping the `Route` from the priority queue.


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 6   "
  "1     "
  "5 1 1 "
  "1 2 2 "
  "1 3 3 "
  "2 3 4 "
  "2 4 5 "
  "3 4 6 "

  // Custom test cases
  "5 6   "
  "1     "
  "2 3 4 "
  "2 4 5 "
  "3 4 6 "
  "5 1 1 "
  "1 2 2 "
  "1 3 3 "

  "2 2 "
  "2 "
  "1 2 1 "
  "2 1 1 "

  "2 1 "
  "2 "
  "1 2 1 "

  "2 3 "
  "1 "
  "1 2 10 "
  "1 2 5 "
  "1 2 10 "

  "3 10 "
  "1 "
  "1 2 1 "
  "1 2 1 "
  "1 2 1 "
  "1 2 1 "
  "1 2 1 "
  "2 1 1 "
  "2 1 1 "
  "2 1 1 "
  "2 1 1 "
  "2 1 1 "

  "3 14 "
  "1 "
  "1 3 3 "
  "3 1 1 "
  "1 2 3 "
  "1 2 2 "
  "1 2 1 "
  "2 1 3 "
  "2 1 2 "
  "2 1 1 "
  "2 3 2 "
  "2 3 3 "
  "2 3 1 "
  "3 2 2 "
  "3 2 3 "
  "3 2 1 "

  "1 0 "
  "1 "

  "3 3 "
  "1 "
  "1 2 2 "
  "2 3 2 "
  "1 3 3 "
);

static std::istringstream test_answer(
  // Given by the text
  "0   "
  "2   "
  "3   "
  "7   "
  "INF "

  // Custom
  "0   "
  "2   "
  "3   "
  "7   "
  "INF "

  "1 "
  "0 "

  "INF "
  "0 "

  "0 "
  "5 "

  "0 "
  "1 "
  "INF "

  "0 "
  "1 "
  "2 "

  "0 "

  "0 "
  "2 "
  "3 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;


constexpr Int VERTEX_LIMIT = 20'001;
constexpr Int DISTANCE_INF = std::numeric_limits<Int>::max() / 2;


struct Edge {
  Int to;
  Int weight;

  bool operator < (const Edge& other) const {
    return this->weight < other.weight;
  }

  bool operator > (const Edge& other) const {
    return this->weight > other.weight;
  }

  bool operator == (const Edge& other) const {
    return this->weight == other.weight;
  }
};


struct Route {
  Int destination;
  Int distance;

  bool operator < (const Route& other) const {
    return this->distance < other.distance;
  }

  bool operator > (const Route& other) const {
    return this->distance > other.distance;
  }

  bool operator == (const Route& other) const {
    return this->distance == other.distance;
  }
};


static Int vertex_total; // V
static Int edge_total; // E
static Int starting_vertex; // K
static std::array<std::vector<Edge>, VERTEX_LIMIT> vertices;
static std::array<Int, VERTEX_LIMIT> distances;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  in >> vertex_total >> edge_total >> starting_vertex;
  if (!in) {
    return false;
  }

  for (auto& vertex : vertices) {
    vertex.clear();
  }
  distances.fill(DISTANCE_INF);

  for (Int edge_count = 1; edge_count <= edge_total; ++edge_count) {
    Int from, to, weight; // u, v, w
    in >> from >> to >> weight;
    vertices.at(from).push_back({ to, weight });
  }

  std::priority_queue<Route, std::vector<Route>, std::greater<Route>> pq;
  pq.push({ starting_vertex, 0 });
  distances.at(starting_vertex) = 0;

  while (!pq.empty()) {
    const auto [destination, distance] = pq.top();
    pq.pop();
    const auto& next_destination = vertices.at(destination);
    for (const auto& edge : next_destination) {
      auto& next_distance = distances.at(edge.to);
      const auto next_distance_candidate = distance + edge.weight;
      if (next_distance <= next_distance_candidate) {
        continue;
      }
      pq.push({ edge.to, next_distance_candidate });
      next_distance = next_distance_candidate;
    }
  }

  for (Int vertex_count = 1; vertex_count <= vertex_total; ++vertex_count) {
    const auto& distance = distances.at(vertex_count);
    if (distance == DISTANCE_INF) {
      out << "INF";
    } else {
      out << distance;
    }
    out << "\n";
  }

  out << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
  case TestFormat::GIVEN: in >> test_case_total; break;
  case TestFormat::ONE: test_case_total = 1; break;
  case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}