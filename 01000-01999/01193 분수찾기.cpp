// https://www.acmicpc.net/problem/1193

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (nothing is given by the text)
  ""

  // Test cases given by the text
  "14 "

  // Custom test cases
  "1 2 3 4 "
);
static std::istringstream test_answer(
  // Given by the text
  "2/4 "

  // Custom
  "1/1 1/2 2/1 3/1 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static void precalculate() {
  // No op
}

static bool solve_case() {
  long long number = 0; // X
  in >> number;
  if (!in) {
    return false;
  }

  // 등차수열의 합
  // aa * (1 + aa) / 2
  // 근의 공식
  const auto aa = (-1 + ::sqrt(1 * 1 - 4 * 1 * (0 - 2 * number))) / (2 * 1);
  const long long diagonal = static_cast<long long>(::ceil(aa));
  // 등차수열의 합 (이전항까지의 합)
  const auto ss = (diagonal - 1) * diagonal / 2;
  // `number`에 해당하는 원소의 대각선 방향의 수열의 1부터 시작하는 index
  const auto index_in_diagonal = number - ss;
  const auto reverse_index_in_diagonal = 1 + diagonal - index_in_diagonal;
  if (diagonal % 2 == 0) {
    out << index_in_diagonal << "/" << reverse_index_in_diagonal;
  } else {
    out << reverse_index_in_diagonal << "/" << index_in_diagonal;
  }
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}