// https://www.acmicpc.net/problem/4344

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (5 is given by the text)
  "5\n"
  // Test cases given by the text
  "5 50 50 70 80 100\n"
  "7 100 95 90 80 70 60 50\n"
  "3 70 90 80\n"
  "3 70 90 81\n"
  "9 100 99 98 97 96 95 94 93 91\n"
);
static std::istringstream test_answer(
  // Given by the text
  "40.000%\n"
  "57.143%\n"
  "33.333%\n"
  "66.667%\n"
  "55.556%\n"
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

enum class TestFormat { ONE, GIVEN, CONDITION };
static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

std::array<int, 1000> arr;

static void precalculate() {
  // No op
}

static bool solve_case() {
  int people_count = 0;
  in >> people_count;
  if (!in) {
    return false;
  }

  for (int people_index = 0; people_index < people_count; ++people_index) {
    in >> arr[people_index];
  }

  const auto score_total =
    std::accumulate(arr.cbegin(), arr.cbegin() + people_count, 0);
  const auto score_average =
    static_cast<double>(score_total) / people_count;
  const auto above_count =
    std::count_if(
      arr.cbegin(),
      arr.cbegin() + people_count,
      [&](const auto& el) { return el > score_average; }
    );
  const auto above_rate =
    static_cast<double>(above_count) / people_count;
  out << std::fixed << std::setprecision(3) << above_rate * 100 << "%\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}