// https://www.acmicpc.net/problem/4963

#include <exception>
#include <iostream>
#include <sstream>
#include <iterator>
#include <array>
#include <queue>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // given by the text
  "1 1 "
  "0 "
  "2 2 "
  "0 1 "
  "1 0 "
  "3 2 "
  "1 1 1 "
  "1 1 1 "
  "5 4 "
  "1 0 1 0 0 "
  "1 0 0 0 0 "
  "1 0 1 0 1 "
  "1 0 0 1 0 "
  "5 4 "
  "1 1 1 0 1 "
  "1 0 1 0 1 "
  "1 0 1 0 1 "
  "1 0 1 1 1 "
  "5 5 "
  "1 0 1 0 1 "
  "0 0 0 0 0 "
  "1 0 1 0 1 "
  "0 0 0 0 0 "
  "1 0 1 0 1 "

  // custom test cases
  // #1
  "2 2 "
  "0 0 "
  "0 0 "
  // #2
  "2 2 "
  "1 1 "
  "1 1 "
  // #3
  "2 2 "
  "1 0 "
  "0 1 "
  // #4
  "3 3 "
  "1 0 1 "
  "0 0 0 "
  "1 1 1 "

  // exit condition
  "0 0 "
);
static std::istringstream test_answer(
  // given by the text
  "0 "
  "1 "
  "1 "
  "3 "
  "1 "
  "9 "

  // custom test case answers
  // #1
  "0 "
  // #2
  "1 "
  // #3
  "1 "
  // #4
  "3 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_out) {
    if (!test_answer) {
      std::cerr << "Too much output.\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_answer) {
    std::cerr << "Some tests have not been tried.\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

enum class Terrain {
  NONE, LAND, SEA
};

struct Area {
  Terrain terrain;
  bool is_visited;
};

template <typename T>
struct Point2d {
  T y, x;
};

static std::array<std::array<Area, 52>, 52> areas;

static std::queue<Point2d<size_t>> pt_queue;

static void attempt_to_enqueue(size_t y, size_t x) {
  Area& area = areas[y][x];
  if (!area.is_visited && area.terrain == Terrain::LAND) {
    area.is_visited = true;
    pt_queue.push({ y, x });
  }
}

static void look_around(const Point2d<size_t>& pt2d) {
  areas[pt2d.y][pt2d.x].is_visited = true;

  attempt_to_enqueue(pt2d.y - 1, pt2d.x - 1);
  attempt_to_enqueue(pt2d.y - 1, pt2d.x + 0);
  attempt_to_enqueue(pt2d.y - 1, pt2d.x + 1);

  attempt_to_enqueue(pt2d.y + 0, pt2d.x - 1);
  attempt_to_enqueue(pt2d.y + 0, pt2d.x + 1);

  attempt_to_enqueue(pt2d.y + 1, pt2d.x - 1);
  attempt_to_enqueue(pt2d.y + 1, pt2d.x + 0);
  attempt_to_enqueue(pt2d.y + 1, pt2d.x + 1);
}

static void solve() {
  while (true) {
    size_t width, height;
    in >> width >> height;

    // exit condition
    if (width == 0 && height == 0) {
      break;
    }

    // clear `areas`
    for (size_t yy = 0; yy < std::size(areas); ++yy) {
      for (size_t xx = 0; xx < std::size(areas[yy]); ++xx) {
        areas[yy][xx].terrain = Terrain::NONE;
        areas[yy][xx].is_visited = false;
      }
    }

    // drawing `areas` land map
    for (size_t yy = 1; yy <= height; ++yy) {
      for (size_t xx = 1; xx <= width; ++xx) {
        int terrain_num;
        in >> terrain_num;
        Terrain terrain;
        switch (terrain_num) {
          case 0 : terrain = Terrain::SEA; break;
          case 1 : terrain = Terrain::LAND; break; 
          default : throw std::out_of_range(std::to_string(__LINE__)); break;
        }
        areas[yy][xx].terrain = terrain;
      }
    }

    // BFS
    int island_count = 0;
    for (size_t yy = 1; yy <= height; ++yy) {
      for (size_t xx = 1; xx <= width; ++xx) {
        const Area& area = areas[yy][xx];
        if (!area.is_visited && area.terrain == Terrain::LAND) {
          pt_queue.push({ yy, xx });
          ++island_count;
        }
        while (!pt_queue.empty()) {
          look_around(pt_queue.front());
          pt_queue.pop();
        }
      }
    }

    // output
    out << island_count << "\n";
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();

  return 0;
}