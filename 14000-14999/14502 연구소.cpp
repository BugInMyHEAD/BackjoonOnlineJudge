// https://www.acmicpc.net/problem/14502
// 연구소


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "7 7 "
  "2 0 0 0 1 1 0 "
  "0 0 1 0 1 2 0 "
  "0 1 1 0 1 0 0 "
  "0 1 0 0 0 0 0 "
  "0 0 0 0 0 1 1 "
  "0 1 0 0 0 0 0 "
  "0 1 0 0 0 0 0 "

  "4 6 "
  "0 0 0 0 0 0 "
  "1 0 0 0 0 2 "
  "1 1 1 0 0 2 "
  "0 0 0 0 0 2 "

  "8 8 "
  "2 0 0 0 0 0 0 2 "
  "2 0 0 0 0 0 0 2 "
  "2 0 0 0 0 0 0 2 "
  "2 0 0 0 0 0 0 2 "
  "2 0 0 0 0 0 0 2 "
  "0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "27 9 3 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 10;
static constexpr Int LEVEL_LIMIT = 3;

using BoardRow = std::array<Int, WIDTH_LIMIT>;
using BoardBase = std::array<BoardRow, WIDTH_LIMIT>;

struct Coordinate {
  Int row = 0;
  Int col = 0;

  Coordinate operator + (const Coordinate& other) const {
    return { row + other.row, col + other.col };
  }
};

class Board : public BoardBase {

public :

  using BoardBase::at;

  Int& at(Int row, Int col) {
    return BoardBase::at(row).at(col);
  }

  Int& at(const Coordinate& c) {
    return at(c.row, c.col);
  }
};

constexpr std::array<Coordinate, 4> offsets {
  Coordinate { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 }
};

static Coordinate board_size;
static Board board;

static std::queue<Coordinate> que;
static Board bfs_board;


static Int invoke_bfs() {
  while (!que.empty()) {
    que.pop();
  }

  bfs_board = board;

  for (Int row = 1; row <= board_size.row; ++row) {
    for (Int col = 1; col <= board_size.col; ++col) {
      if (bfs_board.at(row, col) == 2) {
        que.push({ row, col });
      }
    }
  }

  while (!que.empty()) {
    const auto virus = que.front();
    que.pop();

    for (const auto& offset : offsets) {
      const auto virus_candidate_coordinate = virus + offset;
      auto& virus_candidate = bfs_board.at(virus_candidate_coordinate);
      if (virus_candidate == 0) {
        virus_candidate = 2;
        que.emplace(virus_candidate_coordinate);
      }
    }
  }

  Int result = 0;
  for (Int row = 1; row <= board_size.row; ++row) {
    for (Int col = 1; col <= board_size.col; ++col) {
      result += bfs_board.at(row, col) == 0;
    }
  }
  return result;
}


static Int invoke_dfs(
  Int cell_total = board_size.row * board_size.col,
  Int level = LEVEL_LIMIT
) {
  if (cell_total < level) {
    return 0;
  }
  if (level == 0) {
    return invoke_bfs();
  }

  Int result = 0;
  for (Int cell_count = 0; cell_count < cell_total; ++cell_count) {
    auto& cell = board.at(cell_count / board_size.col + 1, cell_count % board_size.col + 1);
    if (cell != 0) {
      continue;
    }
    cell = 1;
    result = std::max(result, invoke_dfs(cell_count, level - 1));
    cell = 0;
  }
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  board_size.row = board_size.col = -1;
  for (auto& board_row : board) {
    board_row.fill(1);
  }

  in >> board_size.row >> board_size.col;
  if (!in) {
    return false;
  }
  assert(board_size.row >= 0 && board_size.col >= 0);

  for (Int row = 1; row <= board_size.row; ++row) {
    for (Int col = 1; col <= board_size.col; ++col) {
      in >> board.at(row, col);
    }
  }

  out << invoke_dfs() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
  );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}