// https://www.acmicpc.net/problem/14499
// 주사위 굴리기


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 2 0 0 8 "
  "0 2 "
  "3 4 "
  "5 6 "
  "7 8 "
  "4 4 4 1 3 3 3 2 "

  "3 3 1 1 9 "
  "1 2 3 "
  "4 0 5 "
  "6 7 8 "
  "1 3 2 2 4 4 1 1 3 "

  "2 2 0 0 16 "
  "0 2 "
  "3 4 "
  "4 4 4 4 1 1 1 1 3 3 3 3 2 2 2 2 "

  "3 3 0 0 16 "
  "0 1 2 "
  "3 4 5 "
  "6 7 8 "
  "4 4 1 1 3 3 2 2 4 4 1 1 3 3 2 2 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "0 "
  "0 "
  "3 "
  "0 "
  "0 "
  "8 "
  "6 "
  "3 "

  "0 "
  "0 "
  "0 "
  "3 "
  "0 "
  "1 "
  "0 "
  "6 "
  "0 "

  "0 "
  "0 "
  "0 "
  "0 "

  "0 "
  "0 "
  "0 "
  "6 "
  "0 "
  "8 "
  "0 "
  "2 "
  "0 "
  "8 "
  "0 "
  "2 "
  "0 "
  "8 "
  "0 "
  "2 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 20;

struct Coordinate {
  Int row, col;

  Coordinate operator + (const Coordinate& other) const {
    return { this->row + other.row, this->col + other.col };
  }
};

static struct {
  Coordinate size;
  std::array<std::array<Int, WIDTH_LIMIT>, WIDTH_LIMIT> cells;

  bool is_valid_coordinate(const Coordinate& c) {
    return 0 <= c.row && c.row < size.row && 0 <= c.col && c.col < size.col;
  }

  Int& at(const Coordinate& c) {
    return at(c.row, c.col);
  }

  Int& at(Int row, Int col) {
    assert(0 <= row && row < size.row && 0 <= col && col < size.col);
    return cells.at(row).at(col);
  }
} board;

struct {
  Coordinate coordinate;

  //   2
  // 4 0 5
  //   3
  //   1   bottom side
  std::array<Int, 6> sides;

  Int& top() {
    return sides.at(0);
  }

  Int& bottom() {
    return sides.at(1);
  }

  void communicate() {
    auto& cell = board.at(coordinate);
    if (cell == 0) {
      cell = bottom();
    } else {
      bottom() = cell;
      cell = 0;
    }
  }

  //   2        2
  // 4 0 5 -> 0 5 1
  //   3        3
  //   1        4
  void roll_west() {
    std::swap(sides.at(0), sides.at(4));
    std::swap(sides.at(0), sides.at(1));
    std::swap(sides.at(0), sides.at(5));
  }

  //   2        0
  // 4 0 5 -> 4 3 5
  //   3        1
  //   1        2
  void roll_north() {
    std::swap(sides.at(0), sides.at(2));
    std::swap(sides.at(0), sides.at(1));
    std::swap(sides.at(0), sides.at(3));
  }

  void roll_south() {
    roll_north();
    roll_north();
    roll_north();
  }

  void roll_east() {
    roll_west();
    roll_west();
    roll_west();
  }

  bool execute_command(Int direction) {
    constexpr std::array<Coordinate, 5> offsets {
      Coordinate
      { 0, 0 }, // unused
      { 0, 1 }, // east
      { 0, -1 }, // west
      { -1, 0 }, // north
      { 1, 0 } // south
    };

    const auto coordinate_candidate = coordinate + offsets.at(direction);
    if (!board.is_valid_coordinate(coordinate_candidate)) {
      return false;
    }

    coordinate = coordinate_candidate;
    switch (direction) {
      case 1 : roll_east(); break;
      case 2 : roll_west(); break;
      case 3 : roll_north(); break;
      case 4 : roll_south(); break;
      default : throw std::logic_error("switch error");
    }
    communicate();
    return true;
  }
} dice;

Int command_total;


static Int ceil_div(Int dividend, Int divisor) {
  return (dividend + divisor - 1) / divisor;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  board.size = { -1, -1 };
  dice.coordinate = { -1, -1 };
  dice.sides.fill(0);
  command_total = -1;

  in >> board.size.row >> board.size.col
    >> dice.coordinate.row >> dice.coordinate.col
    >> command_total;
  if (!in) {
    return false;
  }
  assert(board.size.row > 0);
  assert(board.size.col > 0);
  assert(dice.coordinate.row >= 0);
  assert(dice.coordinate.col >= 0);
  assert(command_total > 0);

  for (Int row = 0; row < board.size.row; ++row) {
    for (Int col = 0; col < board.size.col; ++col) {
      Int number = -1;
      in >> number;
      assert(number >= 0);
      board.at(row, col) = number;
    }
  }

  for (Int command_count = 0; command_count < command_total; ++command_count) {
    Int direction = -1;
    in >> direction;
    assert(direction > 0);
    if (dice.execute_command(direction)) {
      out << dice.top() << "\n";
    }
  }

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}