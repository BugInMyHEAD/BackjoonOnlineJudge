// https://www.acmicpc.net/problem/14890
// 경사로


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "6 2 "
  "3 3 3 3 3 3 "
  "2 3 3 3 3 3 "
  "2 2 2 3 2 3 "
  "1 1 1 2 2 2 "
  "1 1 1 3 3 1 "
  "1 1 2 3 3 2 "

  "6 2 "
  "3 2 1 1 2 3 "
  "3 2 2 1 2 3 "
  "3 2 2 2 3 3 "
  "3 3 3 3 3 3 "
  "3 3 3 3 2 2 "
  "3 3 3 3 2 2 "

  "6 3 "
  "3 2 1 1 2 3 "
  "3 2 2 1 2 3 "
  "3 2 2 2 3 3 "
  "3 3 3 3 3 3 "
  "3 3 3 3 2 2 "
  "3 3 3 3 2 2 "

  "6 1 "
  "3 2 1 1 2 3 "
  "3 2 2 1 2 3 "
  "3 2 2 2 3 3 "
  "3 3 3 3 3 3 "
  "3 3 3 3 2 2 "
  "3 3 3 3 2 2 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "3 7 3 11 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 100;

using MatrixRow = std::array<Int, WIDTH_LIMIT>;
using MatrixBase = std::array<MatrixRow, WIDTH_LIMIT>;


class Matrix : public MatrixBase {

public:

  using MatrixBase::at;

  Int& at(Int row, Int col) {
    return MatrixBase::at(row).at(col);
  }

  void transpose() {
    for (Int row_idx = 0; row_idx < size(); ++row_idx) {
      assert(size() == at(row_idx).size());
      for (Int col_idx = row_idx + 1; col_idx < size(); ++col_idx) {
        std::swap(at(row_idx, col_idx), at(col_idx, row_idx));
      }
    }
  }
};


static Int width;
static Int ramp_length;
static Matrix matrix;


static Int count_possible_case() {
  Int result = 0;
  for (Int row_idx = 0; row_idx < width; ++row_idx) {
    const auto& row = matrix.at(row_idx);
    Int height = row.at(0);
    Int length = 1;
    bool possible = true;
    for (Int col_idx = 1; col_idx < width; ++col_idx) {
      const auto& cell = row.at(col_idx);
      if (cell == height) {
        ++length;
      } else if (cell == height + 1) {
        if (ramp_length <= length) {
          length = 1;
          height = cell;
        } else {
          possible = false;
          break;
        }
      } else if (cell == height - 1) {
        if (0 <= length) {
          length = 1 - ramp_length;
          height = cell;
        } else {
          possible = false;
          break;
        }
      } else {
        possible = false;
        break;
      }
    }
    if (length < 0) {
      possible = false;
    }
    result += possible;
  }
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = ramp_length = -1;

  in >> width >> ramp_length;
  if (!in) {
    return false;
  }
  assert(width >= 0 && ramp_length >= 0);

  for (Int row_count = 0; row_count < width; ++row_count) {
    for (Int col_count = 0; col_count < width; ++col_count) {
      in >> matrix.at(row_count, col_count);
    }
  }

  const auto row_wise = count_possible_case();
  matrix.transpose();
  const auto col_wise = count_possible_case();
  out << row_wise + col_wise << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}