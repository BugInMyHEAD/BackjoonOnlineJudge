// https://www.acmicpc.net/problem/14503
// Without Point2D

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>
#include <bitset>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 3 "
  "1 1 0 "
  "1 1 1 "
  "1 0 1 "
  "1 1 1 "

  "11 10 "
  "7 4 0 "
  "1 1 1 1 1 1 1 1 1 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 0 0 0 1 1 1 1 0 1 "
  "1 0 0 1 1 0 0 0 0 1 "
  "1 0 1 1 0 0 0 0 0 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 0 0 0 0 0 0 1 0 1 "
  "1 0 0 0 0 0 1 1 0 1 "
  "1 0 0 0 0 0 1 1 0 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 1 1 1 1 1 1 1 1 1 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "1 57 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

enum class CellState {
  WALL, DIRTY, CLEAN
};

static int row_total; // N
static int col_total; // M
static int robot_row; // r
static int robot_col; // c
//  0
// 3 1
//  2
static int robot_direction; // d
static int clean_count;
static std::array<std::array<CellState, 50>, 50> room;

static CellState& cell_at(size_t row, size_t col) {
  return room.at(row).at(col);
}

static void robot_clean_cell() {
  cell_at(robot_row, robot_col) = CellState::CLEAN;
  ++clean_count;
}

static void robot_ccw() {
  robot_direction = (robot_direction + 3) % 4;
}

static bool robot_is_left_cell_dirty() {
  switch (robot_direction) {
    case 0 : return cell_at(robot_row + 0, robot_col - 1) == CellState::DIRTY;
    case 1 : return cell_at(robot_row - 1, robot_col + 0) == CellState::DIRTY;
    case 2 : return cell_at(robot_row + 0, robot_col + 1) == CellState::DIRTY;
    case 3 : return cell_at(robot_row + 1, robot_col + 0) == CellState::DIRTY;
    default : throw std::logic_error("Illegal robot_direction");
  }
}

static bool robot_forward(int distance = 1) {
  int next_robot_row = robot_row;
  int next_robot_col = robot_col;
  switch (robot_direction) {
    case 0 : next_robot_row -= distance; break;
    case 1 : next_robot_col += distance; break;
    case 2 : next_robot_row += distance; break;
    case 3 : next_robot_col -= distance; break;
    default : throw std::logic_error("Illegal robot_direction");
  }
  if (cell_at(next_robot_row, next_robot_col) == CellState::WALL) {
    return false;
  }
  robot_row = next_robot_row;
  robot_col = next_robot_col;
  return true;
}

static bool robot_backward(int distance = 1) {
  return robot_forward(-distance);
}

static bool robot_look_around_and_forward_or_backward() {
  for (int ccw_count = 0; ccw_count < 4; ++ccw_count) {
    const auto decision_to_forward = robot_is_left_cell_dirty();
    robot_ccw();
    if (decision_to_forward) {
      robot_forward();
      robot_clean_cell();
      return true;
    }
  }
  return robot_backward();
}

static void print_debug_info() {
#ifndef ONLINE_JUDGE
  for (int row_count = 0; row_count < row_total; ++row_count) {
    for (int col_count = 0; col_count < col_total; ++col_count) {
      int cell_state_number;
      switch (cell_at(row_count, col_count)) {
        case CellState::DIRTY : cell_state_number = 0; break;
        case CellState::WALL : cell_state_number = 1; break;
        case CellState::CLEAN : cell_state_number = 2; break;
        default : throw std::logic_error("Illegal CellState");
      }
      cell_state_number += row_count == robot_row && col_count == robot_col ? 3 : 0;
      std::cerr << cell_state_number << " ";
    }
    std::cerr << "\n";
  }
  std::cerr
    << "clean_count : " << clean_count << "\n"
    << "-----------------------------------\n";
#endif // ONLINE_JUDGE
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  in >> row_total >> col_total >> robot_row >> robot_col >> robot_direction;
  if (!in) {
    return false;
  }

  clean_count = 0;

  for (int row_count = 0; row_count < row_total; ++row_count) {
    for (int col_count = 0; col_count < col_total; ++col_count) {
      int cell_state_number;
      in >> cell_state_number;
      cell_at(row_count, col_count) =
        cell_state_number == 0 ? CellState::DIRTY : CellState::WALL;
    }
  }

  robot_clean_cell();
  print_debug_info();
  while (robot_look_around_and_forward_or_backward()) {
    print_debug_info();
  }
  out << clean_count;

  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}