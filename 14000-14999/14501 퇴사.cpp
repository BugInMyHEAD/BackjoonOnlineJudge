// https://www.acmicpc.net/problem/14501
// 퇴사


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "7 "
  "3 10 "
  "5 20 "
  "1 10 "
  "1 20 "
  "2 15 "
  "4 40 "
  "2 200 "

  "10 "
  "1 1 "
  "1 2 "
  "1 3 "
  "1 4 "
  "1 5 "
  "1 6 "
  "1 7 "
  "1 8 "
  "1 9 "
  "1 10 "

  "10 "
  "5 10 "
  "5 9 "
  "5 8 "
  "5 7 "
  "5 6 "
  "5 10 "
  "5 9 "
  "5 8 "
  "5 7 "
  "5 6 "

  "10 "
  "5 50 "
  "4 40 "
  "3 30 "
  "2 20 "
  "1 10 "
  "1 10 "
  "2 20 "
  "3 30 "
  "4 40 "
  "5 50 "

  // Custom test cases
  "1 "
  "1 10 "
);

static std::istringstream test_answer(
  // Given by the text
  "45 55 20 90 "

  // Custom
  "10 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int DAY_LIMIT = 16;

struct Task {
  Int time;
  Int pay;
};

struct ChoiceSum {
  Int when_work;
  Int when_pass;

  Int best() {
    return std::max(when_work, when_pass);
  }
};

static Int day_total; // N
static std::array<Task, DAY_LIMIT> tasks;

static std::array<ChoiceSum, DAY_LIMIT> choice_sums;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  day_total = -1;

  in >> day_total;
  if (!in) {
    return false;
  }
  assert(day_total >= 0);

  for (Int day = 0; day < day_total; ++day) {
    Int time = -1;
    Int pay = -1;
    in >> time >> pay;
    assert(time >= 0 && pay >= 0);
    tasks.at(day) = { time, pay };
  }

  choice_sums.at(day_total) = { 0, 0 };
  for (Int day = day_total - 1; day >= 0; --day) {
    const auto& task = tasks.at(day);
    auto& choice_sum = choice_sums.at(day);
    const auto next_task_day = task.time + day;
    choice_sum.when_work =
      next_task_day <= day_total ? choice_sums.at(next_task_day).best() + task.pay : 0;
    choice_sum.when_pass = choice_sums.at(day + 1).best();
  }

  out << choice_sums.at(0).best() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}