// https://www.acmicpc.net/problem/14891
// 톱니바퀴


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "10101111 "
  "01111101 "
  "11001110 "
  "00000010 "
  "2 "
  "3 -1 "
  "1 1 "

  "11111111 "
  "11111111 "
  "11111111 "
  "11111111 "
  "3 "
  "1 1 "
  "2 1 "
  "3 1 "

  "10001011 "
  "10000011 "
  "01011011 "
  "00111101 "
  "5 "
  "1 1 "
  "2 1 "
  "3 1 "
  "4 1 "
  "1 -1 "

  "10010011 "
  "01010011 "
  "11100011 "
  "01010101 "
  "8 "
  "1 1 "
  "2 1 "
  "3 1 "
  "4 1 "
  "1 -1 "
  "2 -1 "
  "3 -1 "
  "4 -1 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "7 15 6 5 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int COGWHEEL_LIMIT = 4;
static constexpr Int TOOTH_LIMIT = 8;

using Cogwheel = std::bitset<TOOTH_LIMIT>;


static std::array<Cogwheel, COGWHEEL_LIMIT> cogwheels;
static Int operation_total;

static std::array<Int, COGWHEEL_LIMIT> directions;


static bool matches(Int left, Int right) {
  return cogwheels.at(left).test(5) ^ cogwheels.at(right).test(1);
}


static void propagate_direction(Int cogwheel_idx, Int idx_offset) {
  Int from = cogwheel_idx;
  Int to = cogwheel_idx + idx_offset;
  while (0 <= to && to < cogwheels.size()) {
    const auto left = std::min(from, to);
    const auto right = std::max(from, to);
    directions.at(to) = matches(left, right) ? -directions.at(from) : 0;
    from += idx_offset;
    to += idx_offset;
  }
}


static void update_cogwheels(Int cogwheel_idx, Int direction) {
  directions.at(cogwheel_idx) = direction;
  propagate_direction(cogwheel_idx, -1);
  propagate_direction(cogwheel_idx, 1);

  for (Int cogwheels_idx = 0; cogwheels_idx < cogwheels.size(); ++cogwheels_idx) {
    auto& cogwheel = cogwheels.at(cogwheels_idx);
    bool endpoint;
    switch (directions.at(cogwheels_idx)) {
      case 1 :
        endpoint = cogwheel.test(0);
        cogwheel >>= 1;
        cogwheel.set(cogwheel.size() - 1, endpoint);
      break;

      case -1 :
        endpoint = cogwheel.test(cogwheel.size() - 1);
        cogwheel <<= 1;
        cogwheel.set(0, endpoint);
      break;
    }
  }
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  operation_total = -1;

  for (auto& cogwheel : cogwheels) {
    in >> cogwheel;
    if (!in) {
      return false;
    }
  }

  in >> operation_total;
  assert(0 <= operation_total);

  for (Int operation_count = 0; operation_count < operation_total; ++operation_count) {
    Int cogwheel_idx = -1;
    Int direction = 0;
    in >> cogwheel_idx >> direction;
    assert(cogwheel_idx >= 0 && direction != 0);
    --cogwheel_idx;

    update_cogwheels(cogwheel_idx, direction);
  }

  Int result = 0;
  for (Int cogwheels_idx = 0; cogwheels_idx < cogwheels.size(); ++cogwheels_idx) {
    result += cogwheels.at(cogwheels_idx).test(7) << cogwheels_idx;
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}