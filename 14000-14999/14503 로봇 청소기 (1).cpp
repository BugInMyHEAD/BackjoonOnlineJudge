// https://www.acmicpc.net/problem/14503
// With Point2D

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>
#include <bitset>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 3 "
  "1 1 0 "
  "1 1 1 "
  "1 0 1 "
  "1 1 1 "

  "11 10 "
  "7 4 0 "
  "1 1 1 1 1 1 1 1 1 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 0 0 0 1 1 1 1 0 1 "
  "1 0 0 1 1 0 0 0 0 1 "
  "1 0 1 1 0 0 0 0 0 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 0 0 0 0 0 0 1 0 1 "
  "1 0 0 0 0 0 1 1 0 1 "
  "1 0 0 0 0 0 1 1 0 1 "
  "1 0 0 0 0 0 0 0 0 1 "
  "1 1 1 1 1 1 1 1 1 1 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "1 57 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D operator * (const T& t) const {
    return { t * this->y, t * this->x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }

  Point2D& operator *= (const T& t) {
    this->y *= t;
    this->x *= t;
    return *this;
  }
};

template <typename T>
static Point2D<T> operator * (T t, const Point2D<T>& other) {
  return other * t;
}

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

enum class CellState {
  WALL, DIRTY, CLEAN
};

//  0
// 3 1
//  2
static constexpr std::array<Point2D<int>, 4> direction {
  Point2D<int> { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 }
};

static Point2D<int> ccw(const Point2D<int>& point) {
  return { -point.x, point.y };
}

static struct Room {
  Point2D<int> size; // N, M
  std::array<std::array<CellState, 50>, 50> cells;

  CellState& cell_at(size_t row, size_t col) {
    return cells.at(row).at(col);
  }

  CellState& cell_at(const Point2D<int>& point) {
    return cell_at(point.y, point.x);
  }
} room;

static struct Robot {
  Point2D<int> direction; // from d
  Point2D<int> position; // r, c
  int clean_count; // 0

  void clean_cell() {
    room.cell_at(position) = CellState::CLEAN;
    ++clean_count;
  }

  void ccw() {
    direction = ::ccw(direction);
  }

  bool is_left_cell_dirty() {
    return room.cell_at(position + ::ccw(direction)) == CellState::DIRTY;
  }

  bool forward(int distance = 1) {
    const auto next_position = position + distance * direction;
    if (room.cell_at(next_position) == CellState::WALL) {
      return false;
    }
    position = next_position;
    return true;
  }

  bool backward(int distance = 1) {
    return forward(-distance);
  }

  bool look_around_and_forward_or_backward() {
    for (int ccw_count = 0; ccw_count < 4; ++ccw_count) {
      const auto decision_to_forward = is_left_cell_dirty();
      ccw();
      if (decision_to_forward) {
        forward();
        clean_cell();
        return true;
      }
    }
    return backward();
  }
} robot;

static void print_debug_info() {
#ifndef ONLINE_JUDGE
  for (int row_count = 0; row_count < room.size.y; ++row_count) {
    for (int col_count = 0; col_count < room.size.x; ++col_count) {
      int cell_state_number;
      switch (room.cell_at(row_count, col_count)) {
        case CellState::DIRTY : cell_state_number = 0; break;
        case CellState::WALL : cell_state_number = 1; break;
        case CellState::CLEAN : cell_state_number = 2; break;
        default : throw std::logic_error("Illegal CellState");
      }
      cell_state_number +=
        row_count == robot.position.y && col_count == robot.position.x ? 3 : 0;
      std::cerr << cell_state_number << " ";
    }
    std::cerr << "\n";
  }
  std::cerr
    << "clean_count : " << robot.clean_count << "\n"
    << "-----------------------------------\n";
#endif // ONLINE_JUDGE
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  int robot_direction_number; // d
  in
    >> room.size.y >> room.size.x
    >> robot.position.y >> robot.position.x >> robot_direction_number;
  if (!in) {
    return false;
  }
  robot.direction = direction.at(robot_direction_number);

  robot.clean_count = 0;

  for (int row_count = 0; row_count < room.size.y; ++row_count) {
    for (int col_count = 0; col_count < room.size.x; ++col_count) {
      int cell_state_number;
      in >> cell_state_number;
      room.cell_at(row_count, col_count) =
        cell_state_number == 0 ? CellState::DIRTY : CellState::WALL;
    }
  }

  robot.clean_cell();
  print_debug_info();
  while (robot.look_around_and_forward_or_backward()) {
    print_debug_info();
  }
  out << robot.clean_count;

  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}