// https://www.acmicpc.net/problem/14500
// 테트로미노


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 5 "
  "1 2 3 4 5 "
  "5 4 3 2 1 "
  "2 3 4 5 6 "
  "6 5 4 3 2 "
  "1 2 1 2 1 "

  "4 5 "
  "1 2 3 4 5 "
  "1 2 3 4 5 "
  "1 2 3 4 5 "
  "1 2 3 4 5 "

  "4 10 "
  "1 2 1 2 1 2 1 2 1 2 "
  "2 1 2 1 2 1 2 1 2 1 "
  "1 2 1 2 1 2 1 2 1 2 "
  "2 1 2 1 2 1 2 1 2 1 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "19 20 7 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int TETROMINO_LENGTH_LIMIT = 4;
static constexpr Int BOARD_WIDTH_LIMIT = 503;

using Board = std::array<std::array<Int, BOARD_WIDTH_LIMIT>, BOARD_WIDTH_LIMIT>;
using TetrominoRow = std::array<bool, TETROMINO_LENGTH_LIMIT>;
using Tetromino = std::array<TetrominoRow, TETROMINO_LENGTH_LIMIT>;

static Int row_total; // N
static Int col_total; // M
static Board board;

static constexpr std::array<Tetromino, 19> tetrominos {
  Tetromino
  {
    TetrominoRow
    { 1, 1, 0, 0 },
    { 1, 1, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 1, 1, 1, 1 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 0, 0, 0 },
    { 1, 0, 0, 0 },
    { 1, 0, 0, 0 },
    { 1, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 1, 0, 0, 0 },
    { 1, 0, 0, 0 },
    { 1, 1, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 1, 1, 0 },
    { 1, 0, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 1, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 0, 0, 1, 0 },
    { 1, 1, 1, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 0, 1, 0, 0 },
    { 0, 1, 0, 0 },
    { 1, 1, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 0, 0, 0 },
    { 1, 1, 1, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 1, 0, 0 },
    { 1, 0, 0, 0 },
    { 1, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 1, 1, 0 },
    { 0, 0, 1, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 1, 0, 0, 0 },
    { 1, 1, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 0, 1, 1, 0 },
    { 1, 1, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 0, 1, 0, 0 },
    { 1, 1, 0, 0 },
    { 1, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 1, 0, 0 },
    { 0, 1, 1, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },

  {
    TetrominoRow
    { 1, 1, 1, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 0, 1, 0, 0 },
    { 1, 1, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 0, 1, 0, 0 },
    { 1, 1, 1, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
  {
    TetrominoRow
    { 1, 0, 0, 0 },
    { 1, 1, 0, 0 },
    { 1, 0, 0, 0 },
    { 0, 0, 0, 0 }
  },
};


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  row_total = col_total = -1;
  for (auto& row : board) {
    row.fill(0);
  }

  in >> row_total >> col_total;
  if (!in) {
    return false;
  }

  for (Int row = 0; row < row_total; ++row) {
    for (Int col = 0; col < col_total; ++col) {
      Int number = -1;
      in >> number;
      assert(number >= 0);
      board.at(row).at(col) = number;
    }
  }

  Int result = 0;
  for (Int row = 0; row < row_total; ++row) {
    for (Int col = 0; col < col_total; ++col) {
      for (const auto& tetromino : tetrominos) {
        Int sum = 0;
        for (Int tetromino_row = 0; tetromino_row < TETROMINO_LENGTH_LIMIT; ++tetromino_row) {
          for (Int tetromino_col = 0; tetromino_col < TETROMINO_LENGTH_LIMIT; ++tetromino_col) {
            sum += tetromino.at(tetromino_row).at(tetromino_col) ? board.at(row + tetromino_row).at(col + tetromino_col) : 0;
          }
        }
        result = std::max(result, sum);
      }
    }
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}