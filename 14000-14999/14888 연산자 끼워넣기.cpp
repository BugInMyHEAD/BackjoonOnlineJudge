// https://www.acmicpc.net/problem/14888
// 연산자 끼워넣기


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "2 "
  "5 6 "
  "0 0 1 0 "

  "3 "
  "3 4 5 "
  "1 0 1 0 "

  "6 "
  "1 2 3 4 5 6 "
  "2 1 1 1 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "30 30 "
  "35 17 "
  "54 -24 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int NUMBER_LIMIT = 11;

static Int number_total;
static std::array<Int, NUMBER_LIMIT> numbers;
static Int addition = 0;
static Int subtraction = 0;
static Int multiplication = 0;
static Int division = 0;


static void precalculate() {
  // No op
}


static std::pair<Int, Int> invoke_dfs(
  std::pair<Int, Int> acc = { numbers.at(0), numbers.at(0) },
  Int level = number_total - 1
) {
  if (level == 0) {
    return acc;
  }

  using Operation = std::tuple<Int*, Int(*)(Int, Int)>;
  constexpr std::array<Operation, 4> operations {
    Operation
  { &addition, [](Int a, Int b) { return a + b; } },
  { &subtraction, [](Int a, Int b) { return a - b; } },
  { &multiplication, [](Int a, Int b) { return a * b; } },
  { &division, [](Int a, Int b) { return a / b; } },
  };

  const auto& number = numbers.at(number_total - level);
  std::pair result { std::numeric_limits<Int>::min(), std::numeric_limits<Int>::max() };
  for (const auto& [operator_count, operator_function] : operations) if (*operator_count > 0) {
    --* operator_count;
    const auto result_candidate =
      invoke_dfs(
        { operator_function(acc.first, number), operator_function(acc.second, number) },
        level - 1
      );
    result = {
      std::max(result.first, result_candidate.first),
      std::min(result.second, result_candidate.second)
    };
    ++* operator_count;
  }
  return result;
}


static bool solve_case(int test_case_count) {
  number_total = -1;

  in >> number_total;
  if (!in) {
    return false;
  }
  assert(number_total);

  for (Int number_count = 0; number_count < number_total; ++number_count) {
    in >> numbers.at(number_count);
  }

  in >> addition;
  in >> subtraction;
  in >> multiplication;
  in >> division;

  const auto [max, min] = invoke_dfs();
  out << max << "\n" << min << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
  case TestFormat::GIVEN: in >> test_case_total; break;
  case TestFormat::ONE: test_case_total = 1; break;
  case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}