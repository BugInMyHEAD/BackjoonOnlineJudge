// https://www.acmicpc.net/problem/14889
// 스타트와 링크


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 "
  "0 1 2 3 "
  "4 0 5 6 "
  "7 1 0 2 "
  "3 4 5 0 "

  "6 "
  "0 1 2 3 4 5 "
  "1 0 2 3 4 5 "
  "1 2 0 3 4 5 "
  "1 2 3 0 4 5 "
  "1 2 3 4 0 5 "
  "1 2 3 4 5 0 "

  "8 "
  "0 5 4 5 4 5 4 5 "
  "4 0 5 1 2 3 4 5 "
  "9 8 0 1 2 3 1 2 "
  "9 9 9 0 9 9 9 9 "
  "1 1 1 1 0 1 1 1 "
  "8 7 6 5 4 0 3 2 "
  "9 1 9 1 9 1 0 9 "
  "6 5 4 3 2 1 9 0 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "0 2 1 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 20;

using MatrixRow = std::array<Int, WIDTH_LIMIT>;
using MatrixBase = std::array<MatrixRow, WIDTH_LIMIT>;


class Matrix : public MatrixBase {

public:

  using MatrixBase::at;

  Int& at(Int row, Int col) {
    return MatrixBase::at(row).at(col);
  }
};


static Int width;
static Matrix matrix;


static std::vector<Int> members;
static std::vector<Int> others;


static Int dfs(Int person_total, Int member_total, Int member_acc = 0, Int other_acc = 0) {
  assert(person_total >= 0);

  if (person_total < member_total) {
    return std::numeric_limits<Int>::max();
  }
  if (person_total <= 0) {
    return std::abs(other_acc - member_acc);
  }

  auto result = std::numeric_limits<Int>::max();
  const auto next_person_total = person_total - 1;

  if (member_total > 0) {
    Int next_member_acc = member_acc;
    for (const auto& member : members) {
      next_member_acc += matrix.at(member, next_person_total);
      next_member_acc += matrix.at(next_person_total, member);
    }
    members.push_back(next_person_total);
    result = std::min(
      result,
      dfs(next_person_total, member_total - 1, next_member_acc, other_acc)
    );
    members.pop_back();
  }

  if (member_total <= next_person_total) {
    Int next_other_acc = other_acc;
    for (const auto& other : others) {
      next_other_acc += matrix.at(other, next_person_total);
      next_other_acc += matrix.at(next_person_total, other);
    }
    others.push_back(next_person_total);
    result = std::min(
      result,
      dfs(next_person_total, member_total, member_acc, next_other_acc)
    );
    others.pop_back();
  }

  return result;
}


static Int invoke_dfs() {
  members.clear();
  others.clear();
  return dfs(width, width / 2);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = -1;

  in >> width;
  if (!in) {
    return false;
  }
  assert(4 <= width && width <= 20);

  for (Int row_count = 0; row_count < width; ++row_count) {
    for (Int col_count = 0; col_count < width; ++col_count) {
      in >> matrix.at(row_count, col_count);
    }
  }

  const auto result = invoke_dfs();
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}