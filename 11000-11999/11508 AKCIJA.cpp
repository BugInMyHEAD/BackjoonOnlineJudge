#include <iostream>
#include <array>
#include <exception>
#include <stdexcept>


int main()
{
	std::array<int, 100000> priceArray { 0 };
	std::array<int, priceArray.size()> yy;
	int numData;
	long long answer = 0;
	std::cin >> numData;
	for (int i1 = 0; i1 < numData; ++i1)
	{
		int price;
		std::cin >> price;
		//priceArray.push_back(price);
		if (++priceArray[price] == 3)
		{
			priceArray[price] = 0;
			answer += 2 * price;
		}
	}
	int charged = 2;
	for (size_t i1 = priceArray.size() - 1; i1 < priceArray.size(); )
	{
		if (priceArray[i1] == 0)
		{
			--i1;
		}
		else if (charged > 0)
		{
			--charged;
			--priceArray[i1];
			answer += i1;
		}
		else
		{
			charged = 2;
			--priceArray[i1];
		}
	}
	
	std::cout << answer << std::endl;

	return 0;
}