// https://www.acmicpc.net/problem/11438
// LCA 2

// This can also be a solution for https://www.acmicpc.net/problem/11437 (LCA)


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "15    "
  "1 2   "
  "1 3   "
  "2 4   "
  "3 7   "
  "6 2   "
  "3 8   "
  "4 9   "
  "2 5   "
  "5 11  "
  "7 13  "
  "10 4  "
  "11 15 "
  "12 5  "
  "14 7  "
  "6     "
  "6 11  "
  "10 9  "
  "2 6   "
  "7 6   "
  "8 13  "
  "8 15  "

  // Custom test cases
  "3 "
  "1 2 "
  "1 3 "
  "4 "
  "1 1 "
  "1 2 "
  "2 2 "
  "2 3 "

  "4 "
  "4 3 "
  "2 3 "
  "1 2 "
  "8 "
  "1 1 "
  "1 2 "
  "2 2 "
  "2 3 "
  "1 3 "
  "3 3 "
  "4 2 "
  "4 1 "

  "5 "
  "2 1 "
  "5 2 "
  "4 2 "
  "3 2 "
  "1 "
  "2 3 "

  "8 "
  "4 2 "
  "3 2 "
  "5 3 "
  "4 6 "
  "2 7 "
  "8 3 "
  "1 2 "
  "2 "
  "3 6 "
  "5 6 "

  "11 "
  "1 2 "
  "1 3 "
  "2 4 "
  "3 5 "
  "4 6 "
  "5 7 "
  "6 8 "
  "7 9 "
  "8 10 "
  "9 11 "
  "1 "
  "10 11 "
);

static std::istringstream test_answer(
  // Given by the text
  "2 4 2 1 3 1 "

  // Custom
  "1 1 2 1 "
  "1 1 2 2 1 3 2 1 "
  "2 "
  "2 2 "
  "1 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int NODE_INDEX_LIMIT = 100'001;

struct LinkNode {
  std::vector<Int> out_links;
  bool visit_check = false;
};

struct TreeNode {
  std::vector<Int> ancestors;
  Int depth = 0;
};

static Int node_total; // N
static Int query_total; // M
static std::array<TreeNode, NODE_INDEX_LIMIT> tree;
static std::array<LinkNode, NODE_INDEX_LIMIT> links;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  for (auto& node : tree) {
    node.ancestors.clear();
  }
  for (auto& node : links) {
    node.out_links.clear();
  }

  in >> node_total;
  if (!in) {
    return false;
  }

  for (Int link_count = 1; link_count < node_total; ++link_count) {
    Int node_index0 = 0;
    Int node_index1 = 0;
    in >> node_index0 >> node_index1;
    assert(node_index0 > 0 && node_index1 > 0);
    auto& node0 = links.at(node_index0);
    auto& node1 = links.at(node_index1);
    node0.out_links.emplace_back(node_index1);
    node0.visit_check = false;
    node1.out_links.emplace_back(node_index0);
    node1.visit_check = false;
  }

  std::queue<Int> que;
  que.emplace(1);
  links.at(1).visit_check = true;
  tree.at(1).depth = 0;
  Int level = 0;
  Int level_element_count = 1;
  Int next_level_element_count = 0;

  while (!que.empty()) {
    auto& node_index = que.front();
    auto& link_node = links.at(node_index);
    auto& tree_node = tree.at(node_index);
    tree_node.depth = level;
    for (auto& out_link : link_node.out_links) {
      auto& out_link_node = links.at(out_link);
      if (!out_link_node.visit_check) {
        auto& child_node = tree.at(out_link);
        out_link_node.visit_check = true;
        ++next_level_element_count;
        que.emplace(out_link);
        child_node.ancestors.emplace_back(node_index);
      }
    }
    que.pop();
    if (--level_element_count == 0) {
      level_element_count = next_level_element_count;
      next_level_element_count = 0;
      ++level;
    }
  }

  bool loop_condition = true;
  Int exp = 0;
  while (loop_condition) {
    loop_condition = false;
    for (Int node_count = 1; node_count <= node_total; ++node_count) {
      auto& node = tree.at(node_count);
      if ((long long)node.ancestors.size() > exp) {
        auto& ancestor = tree.at(node.ancestors.at(exp));
        if ((long long)ancestor.ancestors.size() > exp) {
          node.ancestors.emplace_back(ancestor.ancestors.at(exp));
          loop_condition = true;
        }
      }
    }
    ++exp;
  }

  in >> query_total;

  for (Int query_count = 0; query_count < query_total; ++query_count) {
    Int node_index0 = 0;
    Int node_index1 = 0;
    in >> node_index0 >> node_index1;
    assert(node_index0 > 0 && node_index1 > 0);

    auto deep_node_index = node_index0;
    auto shallow_node_index = node_index1;
    const auto* node_pointer0 = &tree.at(node_index0);
    const auto* node_pointer1 = &tree.at(node_index1);
    if (node_pointer0->depth < node_pointer1->depth) {
      std::swap(node_pointer0, node_pointer1);
      deep_node_index = node_index1;
      shallow_node_index = node_index0;
    }
    const auto& shallow_node = *node_pointer1;
    const auto& shallow_depth = shallow_node.depth;

    const auto depth_difference = tree.at(deep_node_index).depth - shallow_depth;
    auto ancestors_size = tree.at(deep_node_index).ancestors.size();
    auto exp = ancestors_size;
    while (--exp < ancestors_size) {
      if (depth_difference & 1 << exp) {
        deep_node_index = tree.at(deep_node_index).ancestors.at(exp);
      }
    }
    assert(tree.at(deep_node_index).depth == shallow_depth);
    assert(tree.at(deep_node_index).ancestors.size() == shallow_node.ancestors.size());

    if (deep_node_index != shallow_node_index) {
      exp = ancestors_size = tree.at(deep_node_index).ancestors.size();
      while (--exp < ancestors_size) {
        const auto& ancestor0 = tree.at(deep_node_index).ancestors.at(exp);
        const auto& ancestor1 = tree.at(shallow_node_index).ancestors.at(exp);
        if (ancestor0 != ancestor1) {
          deep_node_index = ancestor0;
          shallow_node_index = ancestor1;
          exp = ancestors_size = std::min(exp, tree.at(deep_node_index).ancestors.size());
        }
      }
      deep_node_index = tree.at(deep_node_index).ancestors.at(0);
      shallow_node_index = tree.at(shallow_node_index).ancestors.at(0);
    }
    assert(deep_node_index == shallow_node_index);

    out << deep_node_index << "\n";
  }

  out << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}