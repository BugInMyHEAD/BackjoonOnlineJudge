// https://www.acmicpc.net/problem/11718

#include <stdio.h>

int main(void)
{
    char buf[BUFSIZ];
    
    while(fgets(buf, BUFSIZ, stdin))
    {
        printf("%s", buf);
    }
    
    return 0;
}