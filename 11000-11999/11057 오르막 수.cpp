// https://www.acmicpc.net/problem/11057

#include <iostream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // Test cases given by the text
  "1 2 3 "
);
static std::istringstream test_answer(
  // Given by the text
  "10 55 220 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static constexpr bool IS_TEST_CASE_COUNT_GIVEN = false;

static constexpr int DIVISOR = 10007;

static std::array<std::array<int, 10>, 1000> arr;

static void precalculate() {
  arr[0].fill(1);
  for (size_t i1 = 1; i1 < arr.size(); ++i1) {
    for (size_t i3 = 0; i3 < arr[i1].size(); ++i3) {
      arr[i1][i3] =
        std::accumulate(arr[i1 - 1].cbegin() + i3, arr[i1 - 1].cend(), 0)
          % DIVISOR;
    }
  }
}

static bool solve_case() {
  int number = 0;
  in >> number;
  if (!in) {
    return false;
  }

  auto result =
    std::accumulate(arr[number - 1].cbegin(), arr[number - 1].cend(), 0)
      % DIVISOR;
  out << result << "\n";

#ifndef ONLINE_JUDGE
  return true;
#else // ONLINE_JUDGE
  return false;
#endif // ONLINE_JUDGE
}

static void solve() {
  precalculate();
  if (IS_TEST_CASE_COUNT_GIVEN) {
    int test_case_count = 0;
    in >> test_case_count;
    for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
  } else {
    while (solve_case());
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_out) {
    if (!test_answer) {
      std::cerr << "Too much output\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_answer) {
    std::cerr << "Not all test cases have been tried\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}