// https://www.acmicpc.net/problem/11721

#include <iostream>

static constexpr int ARR_SIZE_IN_MB = 200;
static char arr[ARR_SIZE_IN_MB * (1 << 20)];

int main() {
  while (std::cin.get(arr, 10 + 1, '\0'))
  {
    std::cout << arr;
    if (!std::cin) {
      break;
    }
    std::cout << "\n";
  }
  return 0;
}