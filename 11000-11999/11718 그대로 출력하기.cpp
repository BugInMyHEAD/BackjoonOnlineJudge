// https://www.acmicpc.net/problem/11718

#include <iostream>
#include <iterator>

static constexpr int ARR_SIZE_IN_MB = 200;
static char arr[ARR_SIZE_IN_MB * (1 << 20)];

int main() {
  {
    while (std::cin) {
      std::cin.getline(arr, std::size(arr), '\0');
      std::cout << arr;
    }
  }
  return 0;
}