// https://www.acmicpc.net/problem/11559
// Puyo Puyo


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  ".Y....\n"
  ".YG...\n"
  "RRYG..\n"
  "RRYGG.\n"

  // Custom test cases
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "R.....\n"
  "RBRBB.\n"
  "RGGGBG\n"

  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "......\n"
  "..RR..\n"
  "..PP..\n"
  "RRPPRR\n"
);

static std::istringstream test_answer(
  // Given by the text
  "3 0 2 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static std::array<std::array<char, 8>, 14> board;

static std::array<std::pair<Int, Int>, 12 * 6> dfs_result;
static Int dfs_result_size;


static void precalculate() {
  // No op
}


static void drop() {
  for (Int col = 1; col <= 6; ++col) {
    Int destination_idx = 12;
    Int thrower_idx = 12;
    while (thrower_idx >= 1) {
      auto& destination = board.at(destination_idx).at(col);
      auto& thrower = board.at(thrower_idx).at(col);
      if (thrower != '.') {
        std::swap(destination, thrower);
        --destination_idx;
      }
      --thrower_idx;
    }
  }
}


static void dfs(char puyo, Int row, Int col) {
  if (puyo != 'R' && puyo != 'G' && puyo != 'B' && puyo != 'P' && puyo != 'Y') {
    return;
  }

  constexpr std::array<Int, 4> row_offsets { 1, -1, 0, 0 };
  constexpr std::array<Int, 4> col_offsets { 0, 0, 1, -1 };

  auto& cell = board.at(row).at(col);
  if (cell != puyo) {
    return;
  }

  cell = ::tolower(cell); // visit check
  dfs_result.at(dfs_result_size++) = { row, col };

  for (Int direction = 0; direction < row_offsets.size(); ++direction) {
    dfs(puyo, row + row_offsets.at(direction), col + col_offsets.at(direction));
  }
}


static bool invoke_dfs() {
  bool chain = false;
  for (Int row = 1; row <= 12; ++row) {
    for (Int col = 1; col <= 6; ++col) {
      dfs_result_size = 0;
      dfs(board.at(row).at(col), row, col);
      if (dfs_result_size >= 4) {
        chain = true;
        for (Int dfs_result_idx = 0; dfs_result_idx < dfs_result_size; ++dfs_result_idx) {
          const auto& coordinate = dfs_result.at(dfs_result_idx);
          board.at(coordinate.first).at(coordinate.second) = '.';
        }
      }
    }
  }
  return chain;
}


static void recover() {
  for (auto& row : board) {
    for (auto& cell : row) {
      cell = ::toupper(cell);
    }
  }
}


static bool solve_case(int test_case_count) {
  for (auto& row : board) {
    row.fill('.');
  }

  for (Int row = 1; row <= 12; ++row) {
    std::string one_row;
    in >> one_row;
    if (!in) {
      return false;
    }
    assert(one_row.size() == 6);
    std::copy(one_row.cbegin(), one_row.cend(), board.at(row).begin() + 1);
  }

  Int result = 0;
  bool chain = true;
  while (chain) {
    chain = false;
    drop();
    chain = invoke_dfs();
    recover();
    if (chain) {
      ++result;
    }
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}