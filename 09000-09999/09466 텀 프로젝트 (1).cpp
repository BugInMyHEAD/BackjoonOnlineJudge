// https://www.acmicpc.net/problem/9466
// Using queue.

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text
  "2 "

  // Test cases given by the text
  "7 "
  "3 1 3 7 3 4 6 "

  "8 "
  "1 2 3 4 5 6 7 8 "

  // Custom test cases
  "2 "
  "2 2 "

  "2 "
  "2 1 "

  "2 "
  "1 1 "

  "2 "
  "1 2 "

  "3 "
  "3 1 1"
);

static std::istringstream test_answer(
  // Given by the text
  "3 0 "

  // Custom
  "1 0 1 0 1 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static constexpr int GROUP_UNDEFINED = 0;
static constexpr int GROUP_NONE = -1;

static std::array<int, 100001> choices;
static std::array<int, 100001> groups;
static std::queue<int> choice_queue;

static long long ceil_div(long long dividend, long long divisor) {
  return (dividend + divisor - 1) / divisor;
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  int student_count = -1;
  in >> student_count;
  if (!in) {
    return false;
  }

  for (int choices_idx = 1; choices_idx <= student_count; ++choices_idx) {
    assert(in);
    in >> choices[choices_idx];
  }

  groups.fill(GROUP_UNDEFINED);

  for (int choices_idx = 1; choices_idx <= student_count; ++choices_idx) {
    // Link students until they constitute a cycle or
    // meet a student in another group.
    auto choice = choices_idx;
    while (groups[choice] == GROUP_UNDEFINED) {
      groups[choice] = choices_idx;
      choice_queue.push(choice);
      choice = choices[choice];
    }
    // If the last student's choice is one in another group.
    if (groups[choice] != choices_idx) {
      choice = GROUP_NONE;
    }
    // Exclude who is not in the cycle from the group.
    while (!choice_queue.empty() && choice_queue.front() != choice) {
      groups[choice_queue.front()] = GROUP_NONE;
      choice_queue.pop();
    }
    // Group students in a cycle with the number `choice`.
    while (!choice_queue.empty()) {
      groups[choice_queue.front()] = choice;
      choice_queue.pop();
    }
  }

  out << std::count(groups.cbegin(), groups.cend(), GROUP_NONE);
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}