// https://www.acmicpc.net/problem/9252
// LCS 2


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "ACAYKP CAPCAK "

  // Custom test cases
  "CAPCAK ACAYKP "
  "A B "
  "A A "
  "ABA AAA "
  "ABCA BACA "
  "BACA ABCA "
  "ABC CBA "
  "BBAACC ABCABC "
  "ABCDEF ABCDEF "
  "ABE CDE "
);

static std::istringstream test_answer(
  // Given by the text
  "4 ACAK "

  // Custom
  "4 ACAK "
  "0 "
  "1 A "
  "2 AA "
  "3 BCA "
  "3 ACA "
  "1 A "
  "3 BAC "
  "6 ABCDEF "
  "1 E "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

constexpr Int LENGTH_LIMIT = 1'000;

static std::string str0, str1;

static std::array<std::array<Int, LENGTH_LIMIT>, LENGTH_LIMIT> overlap_counts;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  in >> str0 >> str1;
  if (!in) {
    return false;
  }

  Int result = 0;
  Int result_str0_index = 0;
  Int result_str1_index = 0;
  result = overlap_counts.at(0).at(0) = str0.at(0) == str1.at(0) ? 1 : 0;
  for (Int str1_index = 1; str1_index < (long long)str1.size(); ++str1_index) {
    auto& overlap_count = overlap_counts.at(0).at(str1_index);
    const auto& left = overlap_counts.at(0).at(str1_index - 1);
    overlap_count = left == 1 || str0.at(0) == str1.at(str1_index) ? 1 : 0;
    if (result < overlap_count) {
      result = overlap_count;
      result_str0_index = 0;
      result_str1_index = str1_index;
    }
  }
  for (Int str0_index = 1; str0_index < (long long)str0.size(); ++str0_index) {
    auto& overlap_count = overlap_counts.at(str0_index).at(0);
    const auto& up = overlap_counts.at(str0_index - 1).at(0);
    overlap_count = up == 1 || str0.at(str0_index) == str1.at(0) ? 1 : 0;
    if (result < overlap_count) {
      result = overlap_count;
      result_str0_index = str0_index;
      result_str1_index = 0;
    }
  }
  for (Int str0_index = 1; str0_index < (long long)str0.size(); ++str0_index) {
    const auto& previous_str0_overlap_counts = overlap_counts.at(str0_index - 1);
    auto& str0_overlap_counts = overlap_counts.at(str0_index);
    for (Int str1_index = 1; str1_index < (long long)str1.size(); ++str1_index) {
      auto& overlap_count = str0_overlap_counts.at(str1_index);
      const auto& up = previous_str0_overlap_counts.at(str1_index);
      const auto& left = str0_overlap_counts.at(str1_index - 1);
      const auto& up_left = previous_str0_overlap_counts.at(str1_index - 1);
        overlap_count =
          str0.at(str0_index) == str1.at(str1_index) ? up_left + 1 : std::max(up, left);
      if (result < overlap_count) {
        result = overlap_count;
        result_str0_index = str0_index;
        result_str1_index = str1_index;
      }
    }
  }
  out << result << "\n";

  if (result == 0) {
    out << "\n";
    return true;
  }

  std::string result_str;
  while (result_str0_index > 0 && result_str1_index > 0) {
    const auto& previous_str0_overlap_counts = overlap_counts.at(result_str0_index - 1);
    const auto& str0_overlap_counts = overlap_counts.at(result_str0_index);
    const auto& up = previous_str0_overlap_counts.at(result_str1_index);
    const auto& left = str0_overlap_counts.at(result_str1_index - 1);
    const auto& up_left = previous_str0_overlap_counts.at(result_str1_index - 1);
    const auto& overlap_count = str0_overlap_counts.at(result_str1_index);
    if (up < left) {
      --result_str1_index;
    } else if (up > left) {
      --result_str0_index;
    } else {
      if (overlap_count > up_left && up == up_left) {
        result_str += str0.at(result_str0_index--);
      }
      --result_str1_index;
    }
  }
  if (overlap_counts.at(result_str0_index).at(result_str1_index) == 1) {
    result_str += result_str0_index == 0 ? str0.at(0) : str1.at(0);
  }
  std::reverse(result_str.begin(), result_str.end());
  out << result_str << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}