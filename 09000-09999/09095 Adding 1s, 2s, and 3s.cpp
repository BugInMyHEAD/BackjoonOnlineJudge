// https://www.acmicpc.net/problem/9095

#include <iostream>
#include <sstream>
#include <iterator>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // Number of test cases (3 is given by the text)
  "3 "
  // Test cases given by the text
  "4 "
  "7 "
  "10 "
);
static std::istringstream test_answer(
  // Given by the text
  "7 "
  "44 "
  "274 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static constexpr bool IS_TEST_CASE_COUNT_GIVEN = true;

static std::array<int, 12> arr { 0, 1, 2, 4 };

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_out) {
    if (!test_answer) {
      std::cerr << "Too much output.\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
          << "expected: \"" << answer_string << "\"\n"
          << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_answer) {
    std::cerr << "Not all the test cases have been tried.\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

static bool solve_case() {
  int number = 0;
  in >> number;
  out << arr[number] << "\n";
  return true;
}

static void precalculate() {
  for (size_t i1 = 4; i1 < std::size(arr); ++i1) {
    arr[i1] = arr[i1 - 1] + arr[i1 - 2] + arr[i1 - 3];
  }
}

static void solve() {
  precalculate();
  if (IS_TEST_CASE_COUNT_GIVEN) {
    int test_case_count = 0;
    in >> test_case_count;
    for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
  } else {
    while (solve_case()) ;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}