// https://www.acmicpc.net/problem/9202
// Boggle


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class JudgingFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5\n"
  "ICPC\n"
  "ACM\n"
  "CONTEST\n"
  "GCPC\n"
  "PROGRAMM\n"
  "\n"
  "3\n"
  "ACMA\n"
  "APCA\n"
  "TOGI\n"
  "NEST\n"
  "\n"
  "PCMM\n"
  "RXAI\n"
  "ORCN\n"
  "GPCG\n"
  "\n"
  "ICPC\n"
  "GCPC\n"
  "ICPC\n"
  "GCPC"

  // Custom test cases
  "\n"
  "1\n"
  "AAAAAAAA\n"
  "\n"
  "3\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA"
  "\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA"
  "\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA\n"
  "AAAA"
);

static std::istringstream test_answer(
  // Given by the text
  "8 CONTEST 4\n"
  "14 PROGRAMM 4\n"
  "2 GCPC 2\n"

  // Custom
  "11 AAAAAAAA 1\n"
  "11 AAAAAAAA 1\n"
  "11 AAAAAAAA 1\n"
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr JudgingFormat JUDGING_FORMAT = JudgingFormat::ONE;

using Int = long long;

static constexpr Int BOGGLE_WIDTH = 4;
static constexpr Int WORD_LENGTH_MAXN = 8;
static constexpr Int WORD_TOT_MAXN = 300'000;
static constexpr Int NOT_LAST = -1;


struct TrieNode {

  Int dict_index = NOT_LAST;
  std::array<TrieNode*, 1 + 'Z' - 'A'> children { nullptr };


  void build(const std::string& word, Int dict_index) {
    if (word.empty()) {
      this->dict_index = dict_index;
      return;
    }
    build(word, dict_index, 0, this);
  }


  // needless
  Int find(const std::string& word) const {
    if (word.empty()) { return dict_index; }
    return find(word, 0, this);
  }


private:


  void build(const std::string& word, Int dict_index, size_t word_ind, TrieNode* node) {
    const auto& ch = word.at(word_ind);
    auto& child = node->children.at(ch - 'A');
    if (!child) {
      child = new TrieNode;
    }
    if (word_ind + 1 < word.size()) {
      build(word, dict_index, word_ind + 1, child);
    } else {
      child->dict_index = dict_index;
    }
  }


  // needless
  Int find(const std::string& word, size_t word_ind, const TrieNode* node) const {
    const auto& ch = word.at(word_ind);
    auto& child = node->children.at(ch - 'A');
    if (!child) {
      return -1;
    }
    if (word_ind + 1 < word.size()) {
      return find(word, word_ind + 1, child);
    } else {
      return child->dict_index;
    }
  }

};


struct Coord2 {

  Int y = 0;
  Int x = 0;


  Coord2 operator + (const Coord2& other) const {
    return { y + other.y, x + other.x };
  }
};


template <typename T>
using Array2Base = std::array<std::array<T, BOGGLE_WIDTH>, BOGGLE_WIDTH>;

template <typename T>
struct Array2 : Array2Base<T> {

  using Array2Base<T>::at;


  T& at(size_t row, size_t col) {
    return at(row).at(col);
  }


  T& at(const Coord2& coord) {
    return at(coord.y, coord.x);
  }


  void fill2(const T& t) {
    for (auto& row : *this) {
      row.fill(t);
    }
  }


  bool covers(size_t row, size_t col) const {
    return row < this->size() && col < (*this)[row].size();
  }


  bool covers(const Coord2& coord) const {
    return covers(coord.y, coord.x);
  }

};


static constexpr std::array<Coord2, 8> offsets {
  Coord2
  { -1, -1 }, { -1, 0 }, { -1, 1 },
  { 0, -1 }, { 0, 1 },
  { 1, -1 }, { 1, 0 }, { 1, 1 }
};

static constexpr std::array<Int, WORD_LENGTH_MAXN + 1> points {
  0, 0, 0, 1, 1, 2, 3, 5, 11
};

static Int word_tot;
static TrieNode root;
static Int boggle_tot;
static Array2<char> boggle;

static Int top_score;
static Int top_word_cnt;
static std::string longest;
static Array2<bool> visit_checks;
static std::string dfs_word;
static std::array<bool, WORD_TOT_MAXN> dict;


static void dfs(const Coord2& coord, Int lvl, const TrieNode* node) {
  if (lvl <= 0 || !boggle.covers(coord)) {
    return;
  }

  const auto& ch = boggle.at(coord);
  const auto child = node->children.at(ch - 'A');
  auto& visit_check = visit_checks.at(coord);
  if (!child || visit_check) {
    return;
  }
  visit_check = true;
  dfs_word += ch;

  if (child->dict_index >= 0) {
    bool& used = dict.at(child->dict_index);
    if (!used) {
      used = true;
      top_score += points.at(dfs_word.size());
      ++top_word_cnt;
      if (
        longest.size() < dfs_word.size()
        ||
        longest.size() == dfs_word.size() && longest > dfs_word
        ) {
        longest = dfs_word;
      }
    }
  }

  for (const auto& offset : offsets) {
    dfs(coord + offset, lvl - 1, child);
  }

  dfs_word.pop_back();
  visit_check = false;
}


static void invoke_dfs() {
  top_score = 0;
  top_word_cnt = 0;
  longest.clear();
  dict.fill(false);
  for (Int row_ind = 0; row_ind < BOGGLE_WIDTH; ++row_ind) {
    for (Int col_ind = 0; col_ind < BOGGLE_WIDTH; ++col_ind) {
      visit_checks.fill2(false);
      dfs_word.clear();
      dfs({ row_ind, col_ind }, WORD_LENGTH_MAXN, &root);
    }
  }
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  in >> word_tot;
  if (!in) {
    return false;
  }

  for (Int word_cnt = 0; word_cnt < word_tot; ++word_cnt) {
    std::string word;
    in >> word;
    root.build(word, word_cnt);
  }

  in >> boggle_tot;

  for (Int boggle_cnt = 0; boggle_cnt < boggle_tot; ++boggle_cnt) {
    for (Int row_ind = 0; row_ind < BOGGLE_WIDTH; ++row_ind) {
      std::string row_str;
      in >> row_str;
      assert(row_str.size() == BOGGLE_WIDTH);
      std::copy(row_str.cbegin(), row_str.cend(), boggle.at(row_ind).begin());
    }
    invoke_dfs();
    out << top_score << " " << longest << " " << top_word_cnt << "\n";
  }

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (JUDGING_FORMAT) {
    case JudgingFormat::GIVEN: in >> test_case_total; break;
    case JudgingFormat::ONE: test_case_total = 1; break;
    case JudgingFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}