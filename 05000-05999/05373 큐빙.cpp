﻿// https://www.acmicpc.net/problem/5373
// 큐빙


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text
  "4 "

  // Test cases given by the text
  "1 "
  "L- "

  "2 "
  "F+ B+ "

  "4 "
  "U- D- L+ R+ "

  "10 "
  "L- U- L+ U- L- U- U- L+ U+ U+ "

  // Custom test cases
  "1 "
  "L+ "

  "1 "
  "R- "

  "1 "
  "R+ "

  "1 "
  "F- "

  "1 "
  "F+ "

  "1 "
  "B- "

  "1 "
  "B+ "
);

static std::istringstream test_answer(
  // Given by the text
  "rww "
  "rww "
  "rww "

  "bbb "
  "www "
  "ggg "

  "gwg "
  "owr "
  "bwb "

  "gwo "
  "www "
  "rww "

  // Custom
  "oww "
  "oww "
  "oww "

  "wwo "
  "wwo "
  "wwo "

  "wwr "
  "wwr "
  "wwr "

  "www "
  "www "
  "bbb "

  "www "
  "www "
  "ggg "

  "ggg "
  "www "
  "www "

  "bbb "
  "www "
  "www "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

using Int = int;

using Coord2 = std::pair<Int, Int>;

static constexpr Int WIDTH_LIMIT = 3;
static constexpr Int AREA_LIMIT = WIDTH_LIMIT * WIDTH_LIMIT;
static constexpr Int VOLUME_LIMIT = AREA_LIMIT * WIDTH_LIMIT;


struct Block {
  char up = 'w';
  char down = 'y';
  char front = 'r';
  char back = 'o';
  char left = 'g';
  char right = 'b';


  void cw_up() {
    std::swap(back, right);
    std::swap(back, front);
    std::swap(back, left);
  }


  void cw_front() {
    std::swap(up, right);
    std::swap(up, down);
    std::swap(up, left);
  }


  void cw_left() {
    std::swap(up, front);
    std::swap(up, down);
    std::swap(up, back);
  }
};


struct IndexCalculator {
  Int idx0_weight;
  Int idx1_weight;
  Int idx2_weight;


  Int operator () (Int idx0, Int idx1, Int idx2) const {
    return idx0_weight * idx0 + idx1_weight * idx1 + idx2_weight * idx2;
  }
};


static constexpr IndexCalculator IC_UP { WIDTH_LIMIT, 1, AREA_LIMIT };
static constexpr IndexCalculator IC_FRONT { AREA_LIMIT, 1, WIDTH_LIMIT };
static constexpr IndexCalculator IC_LEFT { AREA_LIMIT, WIDTH_LIMIT, 1 };


using CubeBase = std::array<Block, VOLUME_LIMIT>;


struct Cube : CubeBase {

  void cw(const IndexCalculator& ic, Int fixed_idx) {
    std::swap(at(ic(0, 0, fixed_idx)), at(ic(0, 2, fixed_idx)));
    std::swap(at(ic(0, 0, fixed_idx)), at(ic(2, 2, fixed_idx)));
    std::swap(at(ic(0, 0, fixed_idx)), at(ic(2, 0, fixed_idx)));
    std::swap(at(ic(0, 1, fixed_idx)), at(ic(1, 2, fixed_idx)));
    std::swap(at(ic(0, 1, fixed_idx)), at(ic(2, 1, fixed_idx)));
    std::swap(at(ic(0, 1, fixed_idx)), at(ic(1, 0, fixed_idx)));
  }


  void operate(
    Int idx, const IndexCalculator& ic, void (Block::*rotation)(), Int rep_num
  ) {
    for (Int rep_cnt = 0; rep_cnt < rep_num; ++rep_cnt) {
      cw(ic, idx);
      for (Int idx0 = 0; idx0 < WIDTH_LIMIT; ++idx0) {
        for (Int idx1 = 0; idx1 < WIDTH_LIMIT; ++idx1) {
          (at(ic(idx0, idx1, idx)).*rotation)();
        }
      }
    }
  }
  

  void operate(char face, char rotation) {
    if (face == 'D' || face == 'B' || face == 'R') {
      rotation = rotation == '+' ? '-' : '+';
    }
    const auto rep_num = rotation == '+' ? 1 : 3;
    switch (face) {
      case 'U' : operate(0, IC_UP, &Block::cw_up, rep_num); break;
      case 'D' : operate(2, IC_UP, &Block::cw_up, rep_num); break;
      case 'F' : operate(2, IC_FRONT, &Block::cw_front, rep_num); break;
      case 'B' : operate(0, IC_FRONT, &Block::cw_front, rep_num); break;
      case 'L' : operate(0, IC_LEFT, &Block::cw_left, rep_num); break;
      case 'R' : operate(2, IC_LEFT, &Block::cw_left, rep_num); break;
      default : throw std::logic_error("switch error");
    }
  }
};


static Int operation_tot;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  operation_tot = -1;

  in >> operation_tot;
  if (!in) {
    return false;
  }
  assert(0 <= operation_tot);

  Cube cube;

  for (Int operation_cnt = 0; operation_cnt < operation_tot; ++operation_cnt) {
    std::string operation_str;
    in >> operation_str;
    assert(operation_str.size() == 2);
    cube.operate(operation_str.at(0), operation_str.at(1));
  }

  for (Int row_cnt = 0; row_cnt < WIDTH_LIMIT; ++row_cnt) {
    for (Int col_cnt = 0; col_cnt < WIDTH_LIMIT; ++col_cnt) {
      out << cube.at(IC_UP(row_cnt, col_cnt, 0)).up;
    }
    out << "\n";
  }

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}