// https://www.acmicpc.net/problem/17135
// 캐슬 디펜스


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 5 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "

  "5 5 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "

  "5 5 2 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "

  "5 5 5 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "

  "6 5 1 "
  "1 0 1 0 1 "
  "0 1 0 1 0 "
  "1 1 0 0 0 "
  "0 0 0 1 1 "
  "1 1 0 1 1 "
  "0 0 1 0 0 "

  "6 5 2 "
  "1 0 1 0 1 "
  "0 1 0 1 0 "
  "1 1 0 0 0 "
  "0 0 0 1 1 "
  "1 1 0 1 1 "
  "0 0 1 0 0 "

  // Custom test cases
  "2 7 2 "
  "0 0 1 0 1 0 1 "
  "1 0 1 0 1 0 0 "

  "3 3 4 "
  "1 1 1 "
  "1 1 1 "
  "1 1 1 "

  "3 3 4 "
  "0 0 0 "
  "0 0 0 "
  "0 0 0 "

  "15 15 10 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
);

static std::istringstream test_answer(
  // Given by the text
  "3 3 5 15 9 14 "

  // Custom
  "5 9 0 0 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


template <typename M, typename C>
static void fill_matrix(M& matrix, const C& value) {
  for (auto& row : matrix) {
    row.fill(value);
  }
}


using Int = int;
enum class Enemy { EMPTY, ALIVE, DEAD };

constexpr Int WIDTH_LIMIT = 15;
constexpr Int ARCHER_LIMIT = 3;
constexpr Int NOT_IN_RANGE = std::numeric_limits<Int>::max() / 2;
constexpr auto NOT_IN_RANGE_PAIR = std::make_pair(NOT_IN_RANGE, NOT_IN_RANGE);

using Arr1 = std::array<Enemy, WIDTH_LIMIT>;
using Arr2 = std::array<Arr1, WIDTH_LIMIT>;

static Int row_total; // N
static Int col_total; // M
static Int fire_range; // D
static Arr2 stage;

static std::array<Int, ARCHER_LIMIT> archers;
static std::array<std::pair<Int, Int>, ARCHER_LIMIT> coords;


static std::pair<Int, Int> find_nearest_enemy(Int turn_remainder, Int archer) {
  if (turn_remainder <= 0) {
    return NOT_IN_RANGE_PAIR;
  }

  const auto frontline = turn_remainder - 1;

  for (Int distance = 0; distance < fire_range; ++distance) {
    Int row = frontline;
    Int col = archer - distance;

    while (col <= archer + distance && col < col_total) {
      if (row >= 0 && col >= 0 && stage.at(row).at(col) == Enemy::ALIVE) {
        return { row, col };
      }

      row += col < archer ? -1 : 1;
      ++col;
    }
  }

  return NOT_IN_RANGE_PAIR;
}


static bool is_in_range(const std::pair<Int, Int>& coord) {
  return coord != NOT_IN_RANGE_PAIR;
}


static Int simulate() {
  for (auto& stage_row : stage) {
    for (auto& enemy : stage_row) {
      if (enemy == Enemy::DEAD) {
        enemy = Enemy::ALIVE;
      }
    }
  }

  Int result = 0;

  for (Int turn_remainder = row_total; turn_remainder > 0; --turn_remainder) {
    for (Int archer_count = 0; archer_count < ARCHER_LIMIT; ++archer_count) {
      const auto& archer = archers.at(archer_count);
      const auto coord = find_nearest_enemy(turn_remainder, archer);
      coords.at(archer_count) = coord;
    }

    for (const auto& coord : coords) {
      if (!is_in_range(coord)) {
        continue;
      }

      auto& enemy = stage.at(coord.first).at(coord.second);
      if (enemy == Enemy::ALIVE) {
        ++result;
        enemy = Enemy::DEAD;
      }
    }
  }

  return result;
}


static Int dfs(Int castle_total = col_total, int level = ARCHER_LIMIT) {
  if (castle_total < level) {
    return -1;
  }
  if (level <= 0) {
    return simulate();
  }

  const auto next_level = level - 1;
  Int result = -1;
  for (Int castle_count = 0; castle_count < castle_total; ++castle_count) {
    archers.at(next_level) = castle_count;
    result = std::max(result, dfs(castle_count, next_level));
  }
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  row_total = col_total = fire_range = -1;
  in >> row_total >> col_total >> fire_range;
  if (!in) {
    return false;
  }
  assert(row_total > 0 && col_total > 0 && fire_range > 0);

  for (Int row_count = 0; row_count < row_total; ++row_count) {
    auto& enemy_row = stage.at(row_count);

    for (Int col_count = 0; col_count < col_total; ++col_count) {
      Int enemy_number = -1;
      in >> enemy_number;
      assert(enemy_number >= 0);

      auto& enemy = enemy_row.at(col_count);
      switch (enemy_number) {
        case 0 : enemy = Enemy::EMPTY; break;
        case 1 : enemy = Enemy::ALIVE; break;
      }
    }
  }

  out << dfs() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}