// https://www.acmicpc.net/problem/17142
// 연구소 3


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "7 3 "
  "2 0 0 0 1 1 0 "
  "0 0 1 0 1 2 0 "
  "0 1 1 0 1 0 0 "
  "0 1 0 0 0 0 0 "
  "0 0 0 2 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 0 0 2 "

  "7 3 "
  "2 0 2 0 1 1 0 "
  "0 0 1 0 1 2 0 "
  "0 1 1 2 1 0 0 "
  "2 1 0 0 0 0 2 "
  "0 0 0 2 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 2 0 2 "

  "7 4 "
  "2 0 2 0 1 1 0 "
  "0 0 1 0 1 2 0 "
  "0 1 1 2 1 0 0 "
  "2 1 0 0 0 0 2 "
  "0 0 0 2 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 2 0 2 "

  "7 5 "
  "2 0 2 0 1 1 0 "
  "0 0 1 0 1 2 0 "
  "0 1 1 2 1 0 0 "
  "2 1 0 0 0 0 2 "
  "0 0 0 2 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 2 0 2 "

  "7 3 "
  "2 0 2 0 1 1 0 "
  "0 0 1 0 1 0 0 "
  "0 1 1 1 1 0 0 "
  "2 1 0 0 0 0 2 "
  "1 0 0 0 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 2 0 2 "

  "7 2 "
  "2 0 2 0 1 1 0 "
  "0 0 1 0 1 0 0 "
  "0 1 1 1 1 0 0 "
  "2 1 0 0 0 0 2 "
  "1 0 0 0 0 1 1 "
  "0 1 0 0 0 0 0 "
  "2 1 0 0 2 0 2 "

  "5 1 "
  "2 2 2 1 1 "
  "2 1 1 1 1 "
  "2 1 1 1 1 "
  "2 1 1 1 1 "
  "2 2 2 1 1 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "4 4 4 3 7 -1 0 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 52;
static constexpr Int VIRUS_LIMIT = 10;

static constexpr Int TIME_INF = std::numeric_limits<Int>::max() / 2;

static constexpr Int EMPTY = 0;
static constexpr Int WALL = 1;
static constexpr Int INACTIVE_VIRUS = 2;
static constexpr Int ACTIVE_VIRUS = 3;

using BoardRow = std::array<Int, WIDTH_LIMIT>;
using BoardBase = std::array<BoardRow, WIDTH_LIMIT>;


struct Coordinate {
  Int row = 0;
  Int col = 0;

  Coordinate operator + (const Coordinate& other) const {
    return { row + other.row, col + other.col };
  }
};


class Board : public BoardBase {

public :

  using BoardBase::at;

  Int& at(Int row, Int col) {
    return BoardBase::at(row).at(col);
  }

  Int& at(const Coordinate& c) {
    return at(c.row, c.col);
  }
};


constexpr std::array<Coordinate, 4> offsets {
  Coordinate { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 }
};


static Coordinate board_size; // N
static Int active_virus_total; // M
static Board board;
static Int empty_total;
static std::vector<Coordinate> viruses;

static std::queue<Coordinate> que;
static Board bfs_board;
static std::vector<Coordinate> active_viruses;


static Int invoke_bfs() {
  if (empty_total <= 0) {
    // Minor optimization
    return 0;
  }

  while (!que.empty()) {
    que.pop();
  }

  bfs_board = board;

  Int time = 0;
  Int level_element_count = active_virus_total;
  Int next_level_element_count = 0;
  Int empty_count = empty_total;

  for (const auto& active_virus : active_viruses) {
    que.push(active_virus);
  }

  while (!que.empty()) {
    assert(level_element_count > 0);

    const auto virus_coordinate = que.front();
    que.pop();

    for (const auto& offset : offsets) {
      const auto virus_candidate_coordinate = virus_coordinate + offset;
      auto& virus_candidate = bfs_board.at(virus_candidate_coordinate);
      if (virus_candidate == EMPTY || virus_candidate == INACTIVE_VIRUS) {
        if (virus_candidate == EMPTY) {
          --empty_count;
        }
        virus_candidate = ACTIVE_VIRUS;
        que.emplace(virus_candidate_coordinate);
        ++next_level_element_count;
      }
    }

    if (--level_element_count <= 0) {
      level_element_count = next_level_element_count;
      next_level_element_count = 0;
      ++time;
      assert(empty_count >= 0);
      if (empty_count <= 0) {
        break;
      }
    }
  }

  for (Int row = 1; row <= board_size.row; ++row) {
    for (Int col = 1; col <= board_size.col; ++col) {
      if (bfs_board.at(row, col) == EMPTY) {
        return TIME_INF;
      }
    }
  }
  return time;
}


static Int invoke_dfs(
  Int virus_total = viruses.size(),
  Int level = active_virus_total
) {
  if (empty_total <= 0) {
    // Minor optimization
    return 0;
  }
  if (virus_total < level) {
    return TIME_INF;
  }
  if (level == 0) {
    return invoke_bfs();
  }

  Int result = TIME_INF;
  for (Int viruses_idx = 0; viruses_idx < virus_total; ++viruses_idx) {
    const auto& virus_coordinate = viruses.at(viruses_idx);
    auto& cell = board.at(virus_coordinate);
    assert(cell == INACTIVE_VIRUS);
    cell = ACTIVE_VIRUS;
    active_viruses.push_back(virus_coordinate);
    result = std::min(result, invoke_dfs(viruses_idx, level - 1));
    active_viruses.pop_back();
    cell = INACTIVE_VIRUS;
    if (result <= 1) {
      // Minor optimization
      assert(result > 0);
      break;
    }
  }
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  board_size.row = board_size.col = active_virus_total = -1;
  for (auto& board_row : board) {
    board_row.fill(1);
  }
  viruses.clear();
  empty_total = 0;
  active_viruses.clear();

  in >> board_size.row >> active_virus_total;
  if (!in) {
    return false;
  }
  board_size.col = board_size.row;
  assert(board_size.row >= 0 && board_size.col >= 0 && active_virus_total >= 0);

  for (Int row = 1; row <= board_size.row; ++row) {
    for (Int col = 1; col <= board_size.col; ++col) {
      auto& cell = board.at(row, col);
      in >> cell;
      switch (cell) {
        case EMPTY : ++empty_total; break;
        case INACTIVE_VIRUS : viruses.push_back({ row, col }); break;
        default: break;
      }
    }
  }

  const auto result = invoke_dfs();
  out << (result == TIME_INF ? -1 : result) << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
  );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}