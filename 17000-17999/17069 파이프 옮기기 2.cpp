// https://www.acmicpc.net/problem/17069
// 파이프 옮기기 2

// You can also use this source code as it is to solve
// https://www.acmicpc.net/problem/17070
// 파이프 옮기기 1


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 "
  "0 0 0 "
  "0 0 0 "
  "0 0 0 "

  "4 "
  "0 0 0 0 "
  "0 0 0 0 "
  "0 0 0 0 "
  "0 0 0 0 "

  "5 "
  "0 0 1 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "

  "6 "
  "0 0 0 0 0 0 "
  "0 1 0 0 0 0 "
  "0 0 0 0 0 0 "
  "0 0 0 0 0 0 "
  "0 0 0 0 0 0 "
  "0 0 0 0 0 0 "

  "22 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "1 3 0 13 4345413252 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = long long;

static constexpr Int WIDTH_LIMIT = 34;

struct Tile {
  Int south = 0;
  Int south_east = 0;
  Int east = 0;
  bool obstacle = false;
};

static Int width;
static std::array<std::array<Tile, WIDTH_LIMIT>, WIDTH_LIMIT> tiles;


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = -1;
  for (auto& row : tiles) {
    row.fill(Tile());
  }

  in >> width;
  if (!in) {
    return false;
  }
  assert(width > 0);

  for (Int row = 1; row <= width; ++row) {
    for (Int col = 1; col <= width; ++col) {
      Int number = -1;
      in >> number;
      assert(number >= 0);
      tiles.at(row).at(col).obstacle = number;
    }
  }

  tiles.at(width + 1).at(width + 1).south_east = 1;
  for (Int row = width; row > 0; --row) {
    for (Int col = width; col > 0; --col) {
      auto& tile = tiles.at(row).at(col);
      if (tile.obstacle) {
        continue;
      }
      const auto& south = tiles.at(row + 1).at(col);
      const auto& east = tiles.at(row).at(col + 1);
      const auto& south_east = tiles.at(row + 1).at(col + 1);
      if (!south.obstacle) {
        tile.south = south.south + south.south_east;
      }
      if (!east.obstacle) {
        tile.east = east.east + east.south_east;
      }
      if (!south.obstacle && !east.obstacle && !south_east.obstacle) {
        tile.south_east = south_east.south + south_east.east + south_east.south_east;
      }
    }
  }

  out << tiles.at(1).at(1).east << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}