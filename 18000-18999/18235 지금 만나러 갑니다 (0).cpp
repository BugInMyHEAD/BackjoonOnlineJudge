// https://www.acmicpc.net/problem/18235
// O(N), DFS

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "10 4 10 "
  "2 1 2 "
  "7 2 6 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "2 -1 2 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static size_t length; // N

static bool is_in_range(size_t position) {
  return 1 <= position && position <= length;
}

static int dfs(size_t a, size_t b, int time = 0) {
  if (a == b) {
    return time;
  }
  if (!(is_in_range(a) && is_in_range(b))) {
    return -1;
  }

  if (a > b) {
    std::swap(a, b);
  }
  const auto distance = b - a;
  const auto jump = size_t(1) << time;
  // `jump * 4` means the distance change in the next iteration
  // by jumping to be farther or closer.
  const auto remainder = distance % (jump * 4);

  size_t next_a0 = a;
  size_t next_b0 = b;
  size_t next_a1 = a;
  size_t next_b1 = b;
  if (remainder == 0) {
    // Following 2 cases keep the distance the same.
    next_a0 -= jump;
    next_b0 -= jump;
    next_a1 += jump;
    next_b1 += jump;
  } else if (remainder == jump * 2) {
    // `jump * 2` means distance change in this iteration
    // by jumping to be farther or closer. //
    // Being farther as much as `jump * 2`
    next_a0 -= jump;
    next_b0 += jump;
    // Being closer as much as `jump * 2`
    next_a1 += jump;
    next_b1 -= jump;
  } else {
    return -1;
  }

  const auto next0 = dfs(next_a0, next_b0, time + 1);
  const auto next1 = dfs(next_a1, next_b1, time + 1);
  const auto result_min = std::min(next0, next1);
  const auto result_max = std::max(next0, next1);
  // `result_min` can be -1, if then, return `result_max` which can be a normal value.
  return result_min < 0 ? result_max : result_min;
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t a = 0; // A
  size_t b = 0; // B
  in >> length >> a >> b;
  if (!in) {
    return false;
  }

  out << dfs(a, b);
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}