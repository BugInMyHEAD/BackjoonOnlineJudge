// https://www.acmicpc.net/problem/18235
// O(N), BFS

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "10 4 10 "
  "2 1 2 "
  "7 2 6 "

  // Custom test cases
  "500000 250000 250001 "
);

static std::istringstream test_answer(
  // Given by the text
  "2 -1 2 "

  // Custom
  "-1 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static size_t length; // N

static bool is_in_range(size_t position) {
  return 1 <= position && position <= length;
}

static int bfs(size_t a, size_t b) {
  std::queue<std::pair<size_t, size_t>> que;
  size_t level_element_count = 1;
  size_t next_level_element_count = 0;
  int time = 0;
  que.emplace(a, b);

  /**
   * @return true if next_a == next_b, false otherwise.
   */
  const auto jump_if_in_range = [&] (size_t next_a, size_t next_b) {
    if (next_a == next_b) {
      return true;
    }

    if (is_in_range(next_a) && is_in_range(next_b)) {
      que.emplace(next_a, next_b);
      ++next_level_element_count;
    }
    return false;
  };

  while (!que.empty()) {
    const auto current_a = std::min(que.front().first, que.front().second);
    const auto current_b = std::max(que.front().first, que.front().second);
    assert(current_a <= current_b);

    const auto distance = current_b - current_a;
    const auto jump = size_t(1) << time;
    // `jump * 4` means the distance change in the next iteration
    // by jumping to be farther or closer.
    const auto remainder = distance % (jump * 4);

    if (remainder == 0) {
      // Following 2 cases keep the distance the same.
      if (jump_if_in_range(current_a - jump, current_b - jump)) {
        break;
      }
      if (jump_if_in_range(current_a + jump, current_b + jump)) {
        break;
      }
    } else if (remainder == jump * 2) {
      // `jump * 2` means distance change in this iteration
      // by jumping to be farther or closer. //
      // Being farther as much as `jump * 2`
      if (jump_if_in_range(current_a - jump, current_b + jump)) {
        break;
      }
      // Being closer as much as `jump * 2`
      if (jump_if_in_range(current_a + jump, current_b - jump)) {
        break;
      }
    }

    que.pop();
    if (--level_element_count <= 0) {
      level_element_count = next_level_element_count;
      next_level_element_count = 0;
      ++time;
    }
  }

  return que.empty() ? -1 : time + 1;
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t a = 0; // A
  size_t b = 0; // B
  in >> length >> a >> b;
  if (!in) {
    return false;
  }

  out << bfs(a, b);
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}