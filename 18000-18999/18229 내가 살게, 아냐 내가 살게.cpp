// https://www.acmicpc.net/problem/18229

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 5 20 "
  "3 5 2 1 4 "
  "1 8 2 5 8 "
  "1 5 2 3 3 "
  "1 1 8 9 9 "

  "2 5 100 "
  "1 1 1 1 1 "
  "50 50 50 50 50 "

  "1 4 5 "
  "1 2 2 1 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "2 5 "
  "2 2 "
  "1 3 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<int, 101> arr;

static void precalculate() {
  // No op
}

static bool solve_case() {
  int row_total = 0; // N
  int col_total = 0; // M
  long long goal = 0; // K
  in >> row_total >> col_total >> goal;
  if (!in) {
    return false;
  }

  arr.fill(std::numeric_limits<int>::max());

  for (int row_count = 1; row_count <= row_total; ++row_count) {
    long long hand = 0;
    for (int col_count = 1; col_count <= col_total; ++col_count) {
      long long inc = 0;
      in >> inc;
      hand += inc;
      if (arr[row_count] == std::numeric_limits<int>::max() && hand >= goal) {
        arr[row_count] = col_count;
      }
    }
  }

  const auto result = std::min_element(arr.cbegin(), arr.cend());
  out << result - arr.cbegin() << " " << *result;
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}