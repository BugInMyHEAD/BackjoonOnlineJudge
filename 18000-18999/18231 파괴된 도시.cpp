// https://www.acmicpc.net/problem/18231

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 10 "
  "1 2 "
  "1 3 "
  "1 4 "
  "1 5 "
  "2 3 "
  "2 4 "
  "2 5 "
  "3 4 "
  "3 5 "
  "4 5 "
  "4 "
  "1 2 3 4 "

  "5 3 "
  "1 2 "
  "3 2 "
  "5 4 "
  "4 "
  "1 2 4 5 "

  "11 14 "
  "1 2 "
  "1 3 "
  "1 9 "
  "1 10 "
  "1 4 "
  "1 6 "
  "9 10 "
  "9 8 "
  "3 10 "
  "10 11 "
  "6 8 "
  "6 7 "
  "6 5 "
  "4 5 "
  "10 "
  "10 3 1 9 11 8 6 7 4 5 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "-1 "
  "3 1 4 5 " // "2 1 4 " -- Special judge
  "? " // "3 5 6 10 " -- Special judge

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<bool, 2001> destructed_city;
static std::array<bool, 2001> simulation;
static std::array<bool, 2001> bombing_target;
static std::array<std::vector<size_t>, 2001> adjacency_lists;

static bool are_all_adjecent_cities_destructed(size_t adjacency_list_index) {
  return
    destructed_city[adjacency_list_index]
    && std::all_of(
      adjacency_lists[adjacency_list_index].cbegin(), adjacency_lists[adjacency_list_index].cend(),
      [] (const auto& city) { return destructed_city[city]; }
    );
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t city_total = 0; // N
  size_t road_total = 0; // M
  in >> city_total >> road_total;
  if (!in) {
    return false;
  }

  destructed_city.fill(false);
  simulation.fill(false);
  bombing_target.fill(false);
  for (auto& city : adjacency_lists) {
    city.resize(0);
  }

  for (size_t road_count = 1; road_count <= road_total; ++road_count) {
    size_t city0 = 0;
    size_t city1 = 0;
    in >> city0 >> city1;
    assert(city0 != 0 && city1 != 0);
    adjacency_lists[city0].emplace_back(city1);
    adjacency_lists[city1].emplace_back(city0);
  }

  size_t destructed_city_total = 0; // K
  in >> destructed_city_total;
  assert(destructed_city_total != 0);

  for (
    size_t destructed_city_count = 1;
    destructed_city_count <= destructed_city_total;
    ++destructed_city_count
  ) {
    size_t destructed_city_index = 0;
    in >> destructed_city_index;
    assert(destructed_city_index != 0);
    destructed_city[destructed_city_index] = true;
  }

  for (size_t city_count = 1; city_count <= city_total; ++city_count) {
    if (are_all_adjecent_cities_destructed(city_count)) {
      bombing_target[city_count] = true;
    } else {
      bombing_target[city_count] = false;
    }
  }

  for (size_t city_count = 1; city_count <= city_total; ++city_count) {
    if (bombing_target[city_count]) {
      simulation[city_count] = true;
      for (const auto& city : adjacency_lists[city_count]) {
        simulation[city] = true;
      }
    }
  }

  if (
    std::equal(destructed_city.cbegin(), destructed_city.cend(), simulation.cbegin())
  ) {
    const auto bombing_count =
      std::count(bombing_target.cbegin(), bombing_target.cend(), true);
    out << bombing_count << "\n";
    for (size_t city_count = 1; city_count <= city_total; ++city_count) {
      if (bombing_target[city_count]) {
        out << city_count << " ";
      }
    }
  } else {
    out << -1;
  }
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}