// https://www.acmicpc.net/problem/18230

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 4 3 "
  "1 2 3 4 "
  "4 5 6 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "15 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<long long, 2000> panel;
static std::priority_queue<long long> t21s;
static std::priority_queue<long long, std::vector<long long>, std::greater<long long>> t22s;
static std::priority_queue<long long> t22s_temp;
static std::priority_queue<long long> result;

static void precalculate() {
  // No op
}

static bool solve_case() {
  int length = 0; // N
  int t21_total = 0; // A
  int t22_total = 0; // B
  in >> length >> t21_total >> t22_total;
  if (!in) {
    return false;
  }

  panel.fill(0);

  for (int t21s_index = 0; t21s_index < t21_total; ++t21s_index) {
    long long prettiness = 0;
    in >> prettiness;
    t21s.emplace(prettiness);
  }
  for (int t22s_index = 0; t22s_index < t22_total; ++t22s_index) {
    long long prettiness = 0;
    in >> prettiness;
    t22s_temp.emplace(prettiness);
  }

  long long current_prettiness = 0;
  int current_length = 0;
  if (length % 2 == 1) {
    current_prettiness += t21s.top();
    t21s.pop();
    current_length += 1;
  }
  while (current_length != length && !t22s_temp.empty()) {
    current_prettiness += t22s_temp.top();
    t22s.emplace(t22s_temp.top());
    t22s_temp.pop();
    current_length += 2;
  }
  while (current_length != length && !t21s.empty()) {
    current_prettiness += t21s.top();
    t21s.pop();
    current_length += 1;
  }
  result.emplace(current_prettiness);
  while (!t22s.empty() && !t21s.empty()) {
    current_prettiness -= t22s.top();
    t22s.pop();
    current_prettiness += t21s.top();
    t21s.pop();
    if (t21s.empty()) {
      break;
    }
    current_prettiness += t21s.top();
    t21s.pop();
    result.emplace(current_prettiness);
  }

  out << result.top();
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}