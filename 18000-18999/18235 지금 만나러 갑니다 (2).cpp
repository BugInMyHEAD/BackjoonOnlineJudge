// https://www.acmicpc.net/problem/18235
// O(NlogN)

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>
#include <bitset>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "10 4 10 "
  "2 1 2 "
  "7 2 6 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "2 -1 2 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

using Bitset = std::bitset<500001>;

static Bitset a_bs;
static Bitset b_bs;
static Bitset mask_bs;
static size_t length; // N

static void initialize(Bitset& bs, size_t position) {
  bs.reset();
  bs.set(position);
}

static void jump(Bitset& bs, size_t distance) {
  bs = (bs << distance | bs >> distance) & mask_bs;
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t a = 0; // A
  size_t b = 0; // B
  in >> length >> a >> b;
  if (!in) {
    return false;
  }

  mask_bs.reset();
  for (size_t mask_bs_index = 1; mask_bs_index <= length; ++mask_bs_index) {
    mask_bs.set(mask_bs_index);
  }

  initialize(a_bs, a);
  initialize(b_bs, b);
  size_t day = 1;
  size_t distance = 1;
  while (a_bs.any() && b_bs.any()) {
    jump(a_bs, distance);
    jump(b_bs, distance);
    if ((a_bs & b_bs).any()) {
      break;
    }
    ++day;
    distance *= 2;
  }

  if (a_bs.any() && b_bs.any()) {
    out << day;
  } else {
    out << -1;
  }
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}