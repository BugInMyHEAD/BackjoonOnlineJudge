// https://www.acmicpc.net/problem/18232

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 1 "
  "1 5 "
  "1 4 "

  "10 3 "
  "2 5 "
  "1 6 "
  "1 3 "
  "2 8 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "2 3 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<std::vector<size_t>, 300001> adjacency_lists;
static std::array<bool, 300001> visits;
static std::queue<size_t> que;

static void link(size_t adjacency_lists_index0, size_t adjacency_lists_index1) {
  adjacency_lists[adjacency_lists_index0].emplace_back(adjacency_lists_index1);
  adjacency_lists[adjacency_lists_index1].emplace_back(adjacency_lists_index0);
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t station_total = 0; // N
  size_t long_link_total = 0; // M
  size_t start = 0; // S
  size_t end = 0; // E
  in >> station_total >> long_link_total >> start >> end;
  if (!in) {
    return false;
  }
  assert(station_total != 0 && start != 0 && end != 0);

  for (auto& station : adjacency_lists) {
    station.resize(0);
  }
  visits.fill(false);
  while (!que.empty()) {
    que.pop();
  }

  for (size_t long_link_count = 1; long_link_count <= long_link_total; ++long_link_count) {
    size_t station0 = 0;
    size_t station1 = 0;
    in >> station0 >> station1;
    assert(station0 != 0 && station1 != 0);
    link(station0, station1);
  }
  for (size_t station_count = 2; station_count <= station_total; ++station_count) {
    link(station_count - 1, station_count);
  }

  int level_element_count = 1;
  int next_level_element_count = 0;
  int time = 0;
  que.emplace(start);
  visits[start] = true;

  while (!que.empty() && que.front() != end) {
    for (const auto& station : adjacency_lists[que.front()]) {
      if (!visits[station]) {
        que.emplace(station);
        visits[station] = true;
        ++next_level_element_count;
      }
    }
    que.pop();
    if (--level_element_count <= 0) {
      ++time;
      level_element_count = next_level_element_count;
      next_level_element_count = 0;
    }
  }

  out << time;
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}