// https://www.acmicpc.net/problem/18234

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "2 2 "
  "3 4 "
  "1 2 "

  "3 5 "
  "1 3 "
  "2 9 "
  "3 7 "

  "6 10 "
  "1 10 "
  "2 9 "
  "3 8 "
  "3 5 "
  "6 6 "
  "5 6 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "8 69 324 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<long long, 200000> supplements; // p_i array
static long long carrot_total; // N
static long long period; // T

static void precalculate() {
  // No op
}

static bool solve_case() {
  in >> carrot_total >> period;
  if (!in) {
    return false;
  }

  auto result = 0LL;

  for (auto carrot_count = 0LL; carrot_count < carrot_total; ++carrot_count) {
    auto carrot = 0LL;
    in >> carrot >> supplements.at(carrot_count); // w_i, p_i
    result += carrot;
  }

  std::sort(supplements.begin(), supplements.begin() + carrot_total, std::greater<long long>());

  for (auto carrot_count = 0LL; carrot_count < carrot_total; ++carrot_count) {
    // `period - 1`: Except for the first day the duck plants the carrots.
    result += supplements.at(carrot_count) * (period - 1 - carrot_count);
  }

  out << result;
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}