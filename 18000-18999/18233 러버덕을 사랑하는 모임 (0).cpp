// https://www.acmicpc.net/problem/18233
// Selecting recipients(combination) with std::prev_permutation

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 3 10 "
  "2 3 "
  "1 2 "
  "9 18 "
  "19 20 "

  "5 2 20 "
  "10 20 "
  "15 16 "
  "1 8 "
  "17 22 "
  "2 3 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "-1 "
  "19 0 1 0 0 " // "0 0 0 17 3 " -- Special judge

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
struct Point2D {
  T y = 0;
  T x = 0;

  bool operator == (const Point2D& other) const {
    return this->y == other.y && this->x == other.x;
  }

  Point2D operator + (const Point2D& other) const {
    return { this->y + other.y, this->x + other.x };
  }

  Point2D operator - (const Point2D& other) const {
    return { this->y - other.y, this->x - other.x };
  }

  Point2D& operator += (const Point2D& other) {
    this->y += other.y;
    this->x += other.x;
    return *this;
  }

  Point2D& operator -= (const Point2D& other) {
    this->y -= other.y;
    this->x -= other.x;
    return *this;
  }
};

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

static std::array<std::pair<int, int>, 20> members;
static std::array<bool, 20> selections;

static void precalculate() {
  // No op
}

static bool solve_case() {
  size_t member_total = 0; // N
  size_t recipient_total = 0; // P
  int gift_total = 0; // E
  in >> member_total >> recipient_total >> gift_total;
  if (!in) {
    return false;
  }
  assert(member_total != 0 && recipient_total != 0 && gift_total != 0);

  for (size_t member_count = 0; member_count < member_total; ++member_count) {
    auto& member = members.at(member_count);
    in >> member.first >> member.second;
  }

  if (member_total < recipient_total) {
    out << -1;
  } else {
    selections.fill(false);
    std::fill(selections.begin(), selections.begin() + recipient_total, true);

    int gift_min = 0;
    int gift_max = 0;
    bool decision_to_distribute = false;

    do {
      gift_min = 0;
      gift_max = 0;
      for (size_t member_count = 0; member_count < member_total; ++member_count) {
        if (selections[member_count]) {
          gift_min += members[member_count].first;
          gift_max += members[member_count].second;
        }
      }
      if (gift_min <= gift_total && gift_total <= gift_max) {
        decision_to_distribute = true;
        break;
      }
    } while (std::prev_permutation(selections.begin(), selections.begin() + member_total));

    if (decision_to_distribute) {
      auto extra_gift = gift_total - gift_min;
      for (size_t member_count = 0; member_count < member_total; ++member_count) {
        if (selections.at(member_count)) {
          const auto bonus_candidate =
            members.at(member_count).second - members.at(member_count).first;
          const auto bonus =
            std::min(bonus_candidate, extra_gift);
          extra_gift -= bonus;
          out << members.at(member_count).first + bonus;
        } else {
          out << 0;
        }
        out << " ";
      }
    } else {
      out << -1;
    }
  }

  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}