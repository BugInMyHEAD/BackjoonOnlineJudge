// https://www.acmicpc.net/problem/2775

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (nothing is given by the text)
  "2 "

  // Test cases given by the text
  "1 3 2 3 "

  // Custom test cases
  " "
);
static std::istringstream test_answer(
  // Given by the text
  "6 10 "

  // Custom
  " "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static std::array<std::array<long long, 15>, 15> arr;

static void precalculate() {
  std::iota(arr[0].begin(), arr[0].end(), 0);
  for (size_t arr_index1 = 1; arr_index1 < arr.size(); ++arr_index1) {
    arr[arr_index1][0] = 0;
    for (size_t arr_index0 = 1; arr_index0 < arr[arr_index1].size(); ++arr_index0) {
      arr[arr_index1][arr_index0] =
        arr[arr_index1][arr_index0 - 1] + arr[arr_index1 - 1][arr_index0];
    }
  }
}

long long ceil_div(long long dividend, long long divisor) {
  return (dividend + divisor - 1) / divisor;
}

static bool solve_case() {
  long long level, room_number_on_level; // k, n
  in >> level >> room_number_on_level;
  if (!in) {
    return false;
  }

  out << arr[level][room_number_on_level];
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}