// https://www.acmicpc.net/problem/2941

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <vector>
#include <cstring>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (nothing is given by the text)
  // Test cases given by the text
  "ljes=njak ddz=z= nljj c=c= "
  // Custom test cases
);
static std::istringstream test_answer(
  // Given by the text
  "6 3 3 2 "
  // Custom
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

enum class CombinationResult { FAIL, PENDING, SUCCESS };

static CombinationResult combine(const std::string& str) {
  const char*const combinations[] = {
    "c=", "c-",
    "dz=", "d-",
    "lj", "nj",
    "s=", "z="
  };

  std::vector<const char*> possible_cases;
  possible_cases.insert(
    possible_cases.cbegin(), std::cbegin(combinations), std::cend(combinations)
  );
  const auto removing_result =
    std::remove_if(
      possible_cases.begin(), possible_cases.end(),
      [&](const auto& el) { return ::strstr(el, str.c_str()) == nullptr; }
    );
  // erase-remove idiom: https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom
  possible_cases.erase(removing_result, possible_cases.cend());
  auto finding_result =
    std::find_if(
      possible_cases.cbegin(), possible_cases.cend(),
      [&](const auto& el) { return str == el; }
    );

  if (possible_cases.empty()) {
    return CombinationResult::FAIL;
  } else if (finding_result == possible_cases.cend()) {
    return CombinationResult::PENDING;
  } else {
    return CombinationResult::SUCCESS;
  }
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  std::string token;
  in >> token;
  if (!in) {
    return false;
  }

  size_t letter_count = 0;
  std::string pending_letters;
  for (const auto& token_el : token) {
    pending_letters.push_back(token_el);
    switch (combine(pending_letters)) {
      case CombinationResult::FAIL : {
        letter_count += pending_letters.size() - 1;
        pending_letters.clear();
        pending_letters.push_back(token_el);
      } break;
      case CombinationResult::PENDING : {
        // No op
      } break;
      case CombinationResult::SUCCESS : {
        ++letter_count;
        pending_letters.clear();
      } break;
      default :
        throw std::logic_error("");
    }
  }
  letter_count += pending_letters.size();
  out << letter_count;
  out << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}