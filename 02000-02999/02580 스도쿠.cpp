// https://www.acmicpc.net/problem/2580
// ������


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class JudgingFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "0 3 5 4 6 9 2 7 8 "
  "7 8 2 1 0 5 6 0 9 "
  "0 6 0 2 7 8 1 3 5 "
  "3 2 1 0 4 6 8 9 7 "
  "8 0 4 9 1 3 5 0 6 "
  "5 9 6 8 2 0 4 1 3 "
  "9 1 7 6 5 2 0 8 0 "
  "6 0 3 7 0 1 9 5 2 "
  "2 5 8 3 9 4 7 6 0 "

  // Custom test cases
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
);

static std::istringstream test_answer(
  // Given by the text
  "1 3 5 4 6 9 2 7 8 "
  "7 8 2 1 3 5 6 4 9 "
  "4 6 9 2 7 8 1 3 5 "
  "3 2 1 5 4 6 8 9 7 "
  "8 7 4 9 1 3 5 2 6 "
  "5 9 6 8 2 7 4 1 3 "
  "9 1 7 6 5 2 3 8 4 "
  "6 4 3 7 8 1 9 5 2 "
  "2 5 8 3 9 4 7 6 1 "

  // Custom

  // won't match; just for calculate time
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr JudgingFormat JUDGING_FORMAT = JudgingFormat::ONE;

using Int = int;

static constexpr Int BOARD_WIDTH = 9;
static constexpr Int ZONE_WIDTH = 3;
static constexpr Int EMPTY = 0;


template <typename T>
using Array2Base = std::array<std::array<T, BOARD_WIDTH>, BOARD_WIDTH>;

template <typename T>
struct Array2 : Array2Base<T> {

  using Array2Base<T>::at;


  T& at(size_t row, size_t col) {
    return at(row).at(col);
  }


  void fill2(const T& t) {
    for (auto& row : *this) {
      row.fill(t);
    }
  }


  bool covers(size_t row, size_t col) const {
    return row < this->size() && col < (*this)[row].size();
  }

};


static Array2<Int> board;


static Int zone_index(Int row_ind, Int col_ind) {
  return row_ind / ZONE_WIDTH * ZONE_WIDTH + col_ind / ZONE_WIDTH;
}


static Int zone_to_row_index(Int zone_ind) {
  return zone_ind / ZONE_WIDTH * ZONE_WIDTH;
}


static Int zone_to_col_index(Int zone_ind) {
  return zone_ind % ZONE_WIDTH * ZONE_WIDTH;
}


static bool duplicate_exists_in_zone(Int zone_ind, Int candidate) {
  const auto row_ind_minn = zone_to_row_index(zone_ind);
  const auto col_ind_minn = zone_to_col_index(zone_ind);
  const auto row_ind_maxx = row_ind_minn + ZONE_WIDTH;
  const auto col_ind_maxx = col_ind_minn + ZONE_WIDTH;
  for (Int row_ind = row_ind_minn; row_ind < row_ind_maxx; ++row_ind) {
    for (Int col_ind = col_ind_minn; col_ind < col_ind_maxx; ++col_ind) {
      if (board.at(row_ind, col_ind) == candidate) {
        return true;
      }
    }
  }
  return false;
}


static bool duplicate_exists_on_row(Int row_ind, Int candidate) {
  for (Int col_ind = 0; col_ind < BOARD_WIDTH; ++col_ind) {
    if (board.at(row_ind, col_ind) == candidate) {
      return true;
    }
  }
  return false;
}


static bool duplicate_exists_on_col(Int col_ind, Int candidate) {
  for (Int row_ind = 0; row_ind < BOARD_WIDTH; ++row_ind) {
    if (board.at(row_ind, col_ind) == candidate) {
      return true;
    }
  }
  return false;
}


static bool dfs(Int board_ind) {
  if (board_ind >= BOARD_WIDTH * BOARD_WIDTH) {
    return true;
  }

  const auto row_ind = board_ind / BOARD_WIDTH;
  const auto col_ind = board_ind % BOARD_WIDTH;
  auto& cell = board.at(row_ind, col_ind);

  if (cell != EMPTY) {
    return dfs(board_ind + 1);
  }

  for (Int candidate = 1; candidate <= 9; ++candidate) {
    if (duplicate_exists_on_row(row_ind, candidate)) continue;
    if (duplicate_exists_on_col(col_ind, candidate)) continue;
    const auto zone_ind = zone_index(row_ind, col_ind);
    if (duplicate_exists_in_zone(zone_ind, candidate)) continue;
    cell = candidate;
    if (dfs(board_ind + 1)) {
      return true;
    }
    cell = EMPTY;
  }
  return false;
}


static void invoke_dfs() {
  dfs(0);
}


static bool solve_case(int test_case_count) {
  for (Int row_ind = 0; row_ind < BOARD_WIDTH; ++row_ind) {
    for (Int col_ind = 0; col_ind < BOARD_WIDTH; ++col_ind) {
      in >> board.at(row_ind, col_ind);
      if (!in) {
        return false;
      }
    }
  }

  invoke_dfs();

  for (Int row_ind = 0; row_ind < BOARD_WIDTH; ++row_ind) {
    for (Int col_ind = 0; col_ind < BOARD_WIDTH; ++col_ind) {
      out << board.at(row_ind, col_ind) << " ";
      //std::cout << board.at(row_ind, col_ind) << " ";
    }
    out << "\n";
    //std::cout << "\n";
  }
  //std::cout << "\n";

  return true;
}


static void precalculate() {
  // No op
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (JUDGING_FORMAT) {
    case JudgingFormat::GIVEN: in >> test_case_total; break;
    case JudgingFormat::ONE: test_case_total = 1; break;
    case JudgingFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}