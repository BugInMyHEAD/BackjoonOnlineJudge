// https://www.acmicpc.net/problem/2178

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <queue>

enum class TestFormat { ONE, GIVEN, CONDITION };

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "4 6 "
  "101111 "
  "101010 "
  "101011 "
  "111011 "

  "4 6 "
  "110110 "
  "110110 "
  "111111 "
  "111101 "

  "2 25 "
  "1011101110111011101110111 "
  "1110111011101110111011101 "

  "7 7 "
  "1011111 "
  "1110001 "
  "1000001 "
  "1000001 "
  "1000001 "
  "1000001 "
  "1111111 "

  // Custom test cases
);

static std::istringstream test_answer(
  // Given by the text
  "15 "
  "9 "
  "38 "
  "13 "

  // Custom
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

template <typename T>
static T ceil_div(T dividend, T divisor) {
  return (dividend + divisor - 1) / divisor;
}

struct Cell {
  char terrain = '0';
  int visit_num = 0;
};

// Padding around the edges
static std::array<std::array<Cell, 102>, 102> arr;
// <row_index, col_index>
static std::queue<std::pair<int, int>> que;

static void visit(int row_index, int col_index, int visit_num) {
  if (arr[row_index][col_index].terrain == '1' && arr[row_index][col_index].visit_num <= 0) {
    arr[row_index][col_index].visit_num = visit_num;
    que.emplace(row_index, col_index);
  }
}

static void prepare_bfs() {
  while (!que.empty()) {
    que.pop();
  }
  visit(1, 1, 1);
}

static int invoke_bfs(int goal_row_index, int goal_col_index) {
  while (que.front().first != goal_row_index || que.front().second != goal_col_index) {
    const auto visit_num = arr[que.front().first][que.front().second].visit_num + 1;
    visit(que.front().first - 1, que.front().second + 0, visit_num);
    visit(que.front().first + 0, que.front().second - 1, visit_num);
    visit(que.front().first + 0, que.front().second + 1, visit_num);
    visit(que.front().first + 1, que.front().second + 0, visit_num);
    que.pop();
  }
  return arr[goal_row_index][goal_col_index].visit_num;
}

static void precalculate() {
  // No op
}

static bool solve_case() {
  int row = 0;
  int col = 0;
  in >> row >> col;
  if (!in) {
    return false;
  }

  for (auto& arr_el : arr) {
    arr_el.fill(Cell());
  }

  for (int row_index = 1; row_index <= row; ++row_index) {
    for (int col_index = 1; col_index <= col; ++col_index) {
      char terrain;
      in >> terrain;
      arr[row_index][col_index].terrain = terrain;
    }
    in.ignore(1);
  }

  prepare_bfs();
  out << invoke_bfs(row, col) << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}