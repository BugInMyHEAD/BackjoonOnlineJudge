#include <iostream>
#include <queue>
#include <array>
#include <algorithm>


char buf[1 << 17];
size_t idx, nidx;


static char read() {
	if (idx == nidx) {
		nidx = std::fread(buf, 1, 1 << 17, stdin);
		idx = 0;
	}
	return buf[idx++];
}


static int readInt() {
	int sum = 0;
	bool flag = false;
	char now = read();
	while (now == ' ' || now == '\n')
		now = read();
	if (now == '-') {
		flag = true;
		now = read();
	}
	while (now != ' ' && now != '\n') {
		sum *= 10;
		sum += now - '0';
		now = read();
	}
	return flag ? -sum : sum;
}


int main() {
	//std::ios::sync_with_stdio(false);
	//std::cin.tie(nullptr);

	std::priority_queue<int, std::vector<int>, std::greater<int>> pq;
	std::array<int, 1500> arr;
	//std::array<int, 15000> q;
	//size_t qNumel = 0;
	int n;
	n = readInt();//std::cin >> n;
	//for (int i1 = 0; i1 < n; ++i1) {
	//	pq.push(readInt());
	//}
	for (int i1 = 0; i1 < n; ++i1) {
		//for (int i3 = 0; i3 < n; ++i3) {
		//	arr[i3] = readInt();//std::cin >> arr[i3];
		//}
		//int i2 = n / (n - i1);
		//std::partial_sort(std::begin(arr), std::begin(arr) + i2, std::begin(arr) + n, std::greater<>());
		//std::copy(std::cbegin(arr), std::cbegin(arr) + i2, std::begin(q) + qNumel);
		//qNumel += i2;
		//for (int i3 = 0; i3 < i2; ++i3) {
		for (int i3 = 0; i3 < n; ++i3) {
			int i4 = readInt();
			if (std::size(pq) < n) {
				pq.push(i4);
			}
			else if (pq.top() < i4) {
				pq.push(i4);
				pq.pop();
			}
		}
	}
	//std::partial_sort(std::begin(q), std::begin(q) + n, std::begin(q) + qNumel, std::greater<>());
	std::cout << pq.top() << std::endl;
	//printf("%d\n", pq.top());

	return 0;
}

//Testcase in:
//5
//12 7 9 15 5
//13 8 11 19 6
//21 10 26 31 16
//48 14 28 35 25
//52 20 32 41 49

//Testcase out:
//35