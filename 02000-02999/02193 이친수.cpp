// https://www.acmicpc.net/problem/2193

#include <iostream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

enum class TestFormat { ONE, START, END };

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  "3 "
);
static std::istringstream test_answer(
  "2 "
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

static std::array<std::array<long long, 2>, 90> arr;

static void precalculate() {
  arr[0][0] = 0;
  arr[0][1] = 1;
  arr[1][0] = 2;
  arr[1][1] = 1;
  for (size_t i1 = 2; i1 < arr.size(); ++i1) {
    arr[i1][0] = arr[i1 - 1][0] + arr[i1 - 1][1];
    arr[i1][1] = arr[i1 - 1][0];
    assert(arr[i1][0] >= 0);
    assert(arr[i1][1] >= 0);
  }
}

static bool solve_case() {
  int number = 0;
  in >> number;
  if (!in) {
    return false;
  }

  auto result =
    std::accumulate(arr[number - 1].cbegin() + 1, arr[number - 1].cend(), 0LL);
  out << result << "\n";

  return true;
}

static void solve() {
  precalculate();
  
  switch (TEST_FORMAT) {
    case TestFormat::START : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif // ONLINE_JUDGE
    } break;

    case TestFormat::END : {
      while (solve_case) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}