// https://www.acmicpc.net/problem/7578
// ����


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class JudgingFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "5 "
  "132 392 311 351 231 "
  "392 351 132 311 231 "

  // Custom test cases
  "3 "
  "1 2 3 "
  "3 2 1 "

  "3 "
  "1 2 3 "
  "3 1 2 "

  "3 "
  "3 1 2 "
  "1 2 3 "
  
  "3 "
  "3 1 2 "
  "3 2 1 "
  
  "3 "
  "3 1 2 "
  "3 2 1 "
  
  "3 "
  "3 1 2 "
  "3 1 2 "

  "4 "
  "1 2 3 4 "
  "4 2 1 3 "
  
  "2 "
  "2 1000000 "
  "1000000 2 "
  
  "2 "
  "1000000 2 "
  "1000000 2 "

  "4 "
  "1 2 3 4 "
  "3 1 4 2 "

  "4 "
  "4 3 2 1 "
  "1 3 2 4 "

  "5 "
  "1 2 3 4 5 "
  "5 1 2 3 4 "

  "5 "
  "1 2 3 4 5 "
  "2 3 4 5 1 "
);

static std::istringstream test_answer(
  // Given by the text
  "3 "

  // Custom
  "3 2 2 1 1 0 "
  "4 1 0 3 5 4 4 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr JudgingFormat JUDGING_FORMAT = JudgingFormat::ONE;

using Int = long long;

static constexpr Int ID_TOT_MAXX = 1'000'001;
static constexpr Int NUMBER_TOT_MAXN = 500'000;
static constexpr Int SEGMENT_TREE_LEAF_IND_MAXX = 1 << 19;
static_assert(SEGMENT_TREE_LEAF_IND_MAXX >= NUMBER_TOT_MAXN);

static Int number_tot;
static std::array<Int, ID_TOT_MAXX> orders;
static std::array<Int, NUMBER_TOT_MAXN> b_to_a_inds;

static std::array<Int, SEGMENT_TREE_LEAF_IND_MAXX * 2> segment_tree;


void update(
  Int b_to_a_ind,
  Int current = 0, Int range_minn = 0, Int range_maxx = SEGMENT_TREE_LEAF_IND_MAXX
  ) {
  if (range_minn >= range_maxx || current >= segment_tree.size()) {
    return;
  }
  ++segment_tree.at(current);
  const Int range_mid = (range_minn + range_maxx) / 2;
  const Int right = 2 * (current + 1);
  const Int left = right - 1;
  if (b_to_a_ind < range_mid) {
    update(b_to_a_ind, left, range_minn, range_mid);
  } else {
    update(b_to_a_ind, right, range_mid, range_maxx);
  }
}


Int query(
  Int a_ind_minn, Int a_ind_maxx,
  Int current = 0, Int range_minn = 0, Int range_maxx = SEGMENT_TREE_LEAF_IND_MAXX
  ) {
  if (a_ind_maxx <= range_minn || range_maxx <= a_ind_minn) {
    return 0;
  }
  if (a_ind_minn <= range_minn && range_maxx <= a_ind_maxx) {
    return segment_tree.at(current);
  }
  const Int range_mid = (range_minn + range_maxx) / 2;
  const Int right = 2 * (current + 1);
  const Int left = right - 1;
  return
    query(a_ind_minn, a_ind_maxx, left, range_minn, range_mid)
    + query(a_ind_minn, a_ind_maxx, right, range_mid, range_maxx);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  number_tot = -1;
  segment_tree.fill(0);

  in >> number_tot;
  if (!in) {
    return false;
  }
  assert(0 <= number_tot);

  for (Int number_cnt = 0; number_cnt < number_tot; ++number_cnt) {
    Int number = -1;
    in >> number;
    assert(0 <= number);
    orders.at(number) = number_cnt;
  }
  for (Int b_ind = 0; b_ind < number_tot; ++b_ind) {
    Int number = -1;
    in >> number;
    assert(0 <= number);
    b_to_a_inds.at(b_ind) = orders.at(number);
  }

  Int result = 0;
  for (Int b_ind = 0; b_ind < number_tot; ++b_ind) {
    const Int a_ind = b_to_a_inds.at(b_ind);
    result += query(a_ind, SEGMENT_TREE_LEAF_IND_MAXX);
    update(a_ind);
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (JUDGING_FORMAT) {
    case JudgingFormat::GIVEN: in >> test_case_total; break;
    case JudgingFormat::ONE: test_case_total = 1; break;
    case JudgingFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}