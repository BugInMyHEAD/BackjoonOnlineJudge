// https://www.acmicpc.net/problem/12100
// 2048 (Easy)


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 "
  "2 2 2 "
  "4 4 4 "
  "8 8 8 "

  // Custom test cases
  "1 "
  "32 "

  "2 "
  "2 2 "
  "2 2 "

  "20 "
  "2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 "
);

static std::istringstream test_answer(
  // Given by the text
  "16 "

  // Custom
  "32 8 32 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


template <typename M, typename C>
static void fill_matrix(M& matrix, const C& value) {
  for (auto& row : matrix) {
    row.fill(value);
  }
}


using Int = int;

constexpr Int WIDTH_LIMIT = 20;
constexpr Int LEVEL_MAX = 5;

using Arr1 = std::array<Int, WIDTH_LIMIT>;
using Arr2 = std::array<Arr1, WIDTH_LIMIT>;
using Arr3 = std::array<Arr2, LEVEL_MAX + 1>;

static Int width_total; // N
static Arr2 original_board;
static Arr3 dfs_boards;


static Int next_block_row_index(
  const Arr2& board, Int row, Int col, Int row_offset
  ) {
  while (0 <= row && row < width_total) {
    if (board.at(row).at(col) != 0) {
      return row;
    }
    row += row_offset;
  }
  return -1;
}


static void merge_up(Arr2& board) {
  for (Int col = 0; col < width_total; ++col) {
    Int destination_index = 0;
    Int receiver_index =
      next_block_row_index(board, 0, col, 1);
    Int thrower_index =
      receiver_index < 0 ? -1 : next_block_row_index(board, receiver_index + 1, col, 1);
    while (receiver_index >= 0 && thrower_index >= 0) {
      const auto& receiver_block = board.at(receiver_index).at(col);
      const auto& thrower_block = board.at(thrower_index).at(col);
      if (receiver_block == thrower_block) {
        board.at(destination_index++).at(col) = 2 * receiver_block;
        receiver_index = next_block_row_index(board, thrower_index + 1, col, 1);
      } else {
        board.at(destination_index++).at(col) = receiver_block;
        receiver_index = thrower_index;
      }
      thrower_index =
        receiver_index < 0 ? -1 : next_block_row_index(board, receiver_index + 1, col, 1);
    }
    if (receiver_index >= 0) {
      board.at(destination_index++).at(col) = board.at(receiver_index).at(col);
    }
    while (destination_index < width_total) {
      board.at(destination_index++).at(col) = 0;
    }
  }
}


static void rotate_cw(Arr2& board) {
  static Arr2 temp;

  for (Int board_row_index = 0; board_row_index < width_total; ++board_row_index) {
    const auto temp_col_index = width_total - 1 - board_row_index;
    for (Int board_col_index = 0; board_col_index < width_total; ++board_col_index) {
      const auto temp_row_index = board_col_index;
      temp.at(temp_row_index).at(temp_col_index) = board.at(board_row_index).at(board_col_index);
    }
  }

  board = temp;
}


static Int get_board_max_element(const Arr2& board) {
  Int result = 0;
  for (Int board_row_index = 0; board_row_index < width_total; ++board_row_index) {
    for (Int board_col_index = 0; board_col_index < width_total; ++board_col_index) {
      result = std::max(result, board.at(board_row_index).at(board_col_index));
    }
  }
  return result;
}


static Int dfs(Int level) {
  if (level <= 0) {
    return get_board_max_element(dfs_boards.at(0));
  }

  const auto next_level = level - 1;
  auto& source_board = dfs_boards.at(level);
  auto& merged_board = dfs_boards.at(next_level);
  Int result = 0;
  for (Int rotation = 0; rotation < 4; ++rotation) {
    dfs_boards.at(next_level) = dfs_boards.at(level);
    merged_board = source_board;
    merge_up(merged_board);
    result = std::max(result, dfs(next_level));
    rotate_cw(source_board);
  }
  return result;
}


static Int invoke_dfs(Int level = LEVEL_MAX) {
  dfs_boards.at(level) = original_board;
  return dfs(level);
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width_total = -1;
  in >> width_total;
  if (!in) {
    return false;
  }
  assert(width_total > 0);

  for (Int row_count = 0; row_count < width_total; ++row_count) {
    auto& board_row = original_board.at(row_count);

    for (Int col_count = 0; col_count < width_total; ++col_count) {
      Int number = -1;
      in >> number;
      assert(number >= 0);

      auto& block = board_row.at(col_count);
      block = number;
    }
  }

  out << invoke_dfs() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}