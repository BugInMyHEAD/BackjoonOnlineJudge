﻿// https://www.acmicpc.net/problem/16234
// 인구 이동


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "2 20 50 "
  "50 30 "
  "20 40 "

  "2 40 50 "
  "50 30 "
  "20 40 "

  "2 20 50 "
  "50 30 "
  "30 40 "

  // Custom test cases
  "2 20 50 "
  "100 50 "
  "25 25 "
);

static std::istringstream test_answer(
  // Given by the text
  "1 0 1 "

  // Custom
  "2 "
);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

using Int = int32_t;

static constexpr Int WIDTH_LIMIT = 52;
static constexpr Int POPULATION_INVALID = std::numeric_limits<Int>::min() / 2;


struct Coord2 {
  Int y = 0;
  Int x = 0;


  Coord2 operator + (const Coord2& other) const {
    return { y + other.y, x + other.x };
  }


  Coord2& operator += (const Coord2& other) {
    return *this = *this + other;
  }
};


static constexpr std::array<Coord2, 4> offsets {
  Coord2 { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 }
};


template <typename T>
using GridBase = std::array<std::array<T, WIDTH_LIMIT>, WIDTH_LIMIT>;


template <typename T>
struct Grid : public GridBase<T> {

  using GridBase<T>::at;
  using GridBase<T>::fill;


  T& at(Int row, Int col) {
    return at(row).at(col);
  }


  T& at(const Coord2& c) {
    return at(c.y, c.x);
  }


  void fill(T t) {
    for (auto& row : *this) {
      row.fill(t);
    }
  }
};


static Int width; // N
static Int difference_min; // L
static Int difference_max; // R
static Grid<Int> grid;

static std::vector<Coord2> dfs_result;
static Grid<bool> dfs_visit_checks;


static bool is_unifiable(const Coord2& a, const Coord2& b) {
  const auto difference = std::abs(grid.at(a) - grid.at(b));
  return difference_min <= difference && difference <= difference_max;
}


static bool dfs(const Coord2& c, bool acc) {
  auto& visit_check = dfs_visit_checks.at(c);
  const auto& population = grid.at(c);
  if (visit_check || population == POPULATION_INVALID) {
    return false;
  }

  visit_check = true;
  dfs_result.push_back(c);

  for (const auto& offset : offsets) {
    const auto next_coord = c + offset;
    if (is_unifiable(c, next_coord)) {
      acc |= dfs(next_coord, true);
    }
  }
  return acc;
}


static Int invoke_dfs() {
  Int result = -1;
  bool migration = true;
  while (migration) {
    ++result;
    migration = false;
    dfs_visit_checks.fill(false);
    for (Int row_count = 1; row_count <= width; ++row_count) {
      for (Int col_count = 1; col_count <= width; ++col_count) {
        dfs_result.clear();
        migration |= dfs({ row_count, col_count }, false);
        if (!dfs_result.empty()) {
          const auto avg =
            std::accumulate(
              dfs_result.cbegin(), dfs_result.cend(), 0,
              [](const auto& a, const auto& b) { return a + grid.at(b); }
            ) / Int(dfs_result.size());
          for (auto& coord : dfs_result) {
            grid.at(coord) = avg;
          }
        }
      }
    }
  }
  assert(result <= 2000);
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = difference_min = difference_max -1;
  grid.fill(POPULATION_INVALID);

  in >> width >> difference_min >> difference_max;
  if (!in) {
    return false;
  }
  assert(0 <= width && 0 <= difference_min && difference_max);

  for (Int row_count = 1; row_count <= width; ++row_count) {
    for (Int col_count = 1; col_count <= width; ++col_count) {
      in >> grid.at(row_count, col_count);
    }
  }

  out << invoke_dfs() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}