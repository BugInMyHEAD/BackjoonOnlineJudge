﻿// https://www.acmicpc.net/problem/16236
// 아기 상어


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "3 "
  "0 0 0 "
  "0 0 0 "
  "0 9 0 "

  "3 "
  "0 0 1 "
  "0 0 0 "
  "0 9 0 "

  "4 "
  "4 3 2 1 "
  "0 0 0 0 "
  "0 0 9 0 "
  "1 2 3 4 "

  "6 "
  "5 4 3 2 3 4 "
  "4 3 2 3 4 5 "
  "3 2 9 5 6 6 "
  "2 1 2 3 4 5 "
  "3 2 1 6 5 4 "
  "6 6 6 6 6 6 "

  "6 "
  "6 0 6 0 6 1 "
  "0 0 0 0 0 2 "
  "2 3 4 5 6 6 "
  "0 0 0 0 0 2 "
  "0 2 0 0 0 0 "
  "3 9 3 0 0 1 "

  "6 "
  "1 1 1 1 1 1 "
  "2 2 6 2 2 3 "
  "2 2 5 2 2 3 "
  "2 2 2 4 6 3 "
  "0 0 0 0 0 6 "
  "0 0 0 0 0 9 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "0 3 14 60 48 39 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

using Int = int32_t;

static constexpr Int WIDTH_LIMIT = 22;
static constexpr Int WALL = std::numeric_limits<Int>::max() / 2;


struct Coord2 {
  Int y = 0;
  Int x = 0;


  Coord2 operator + (const Coord2& other) const {
    return { y + other.y, x + other.x };
  }


  Coord2& operator += (const Coord2& other) {
    return *this = *this + other;
  }
};


static constexpr std::array<Coord2, 4> offsets {
  Coord2
  { -1, 0 }, { 0, -1 }, { 0, 1 }, { 1, 0 }
};


template <typename T>
using GridBase = std::array<std::array<T, WIDTH_LIMIT>, WIDTH_LIMIT>;


template <typename T>
struct Array2 : public GridBase<T> {

  using GridBase<T>::at;


  T& at(Int row, Int col) {
    return at(row).at(col);
  }


  T& at(const Coord2& c) {
    return at(c.y, c.x);
  }


  void fill2(T t) {
    for (auto& row : *this) {
      row.fill(t);
    }
  }
};


static Int width; // N
static Array2<Int> grid;

static std::queue<Coord2> shark_positions;
static Array2<bool> visit_checks;
static Int shark;
static Int eaten_fish_count;
static std::vector<Coord2> eaten_fish_candidates;


static Int invoke_bfs() {
  visit_checks.fill2(false);
  eaten_fish_candidates.clear();

  Int time = 0;
  Int level_element_count = Int(shark_positions.size());
  Int next_level_element_count = 0;

  while (!shark_positions.empty()) {
    if (level_element_count-- <= 0) {
      if (!eaten_fish_candidates.empty()) {
        break;
      }
      ++time;
      level_element_count += next_level_element_count;
      next_level_element_count = 0;
    }
    const auto coord = shark_positions.front();
    shark_positions.pop();
    visit_checks.at(coord) = true;
    auto& fish = grid.at(coord);
    assert(fish <= shark);

    if (fish != 0 && fish < shark) {
      eaten_fish_candidates.push_back(coord);
    }

    if (!eaten_fish_candidates.empty()) {
      continue;
    }
    
    for (const auto& offset : offsets) {
      const auto next_coord = coord + offset;
      auto& next_visit_check = visit_checks.at(next_coord);
      if (next_visit_check || shark < grid.at(next_coord)) {
        continue;
      }
      next_visit_check = true;
      shark_positions.push(next_coord);
      ++next_level_element_count;
    }
  }
  while (!shark_positions.empty()) {
    shark_positions.pop();
  }

  if (eaten_fish_candidates.empty()) {
    return 0;
  }

  const auto& fish_position = *std::min_element(
    eaten_fish_candidates.cbegin(), eaten_fish_candidates.cend(),
    [](const Coord2& a, const Coord2& b) { return a.y < b.y || a.y == b.y && a.x < b.x; }
  );
  grid.at(fish_position) = 0;
  shark_positions.push(fish_position);
  if (++eaten_fish_count >= shark) {
    ++shark;
    eaten_fish_count = 0;
  }
  eaten_fish_candidates.clear();
  return time;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = -1;
  grid.fill2(WALL);
  while (!shark_positions.empty()) {
    shark_positions.pop();
  }
  shark = 2;
  eaten_fish_count = 0;

  in >> width;
  if (!in) {
    return false;
  }
  assert(0 <= width);

  for (Int row_count = 1; row_count <= width; ++row_count) {
    for (Int col_count = 1; col_count <= width; ++col_count) {
      auto& cell = grid.at(row_count, col_count);
      in >> cell;
      if (cell == 9) {
        shark_positions.push({ row_count, col_count });
        cell = 0;
      }
    }
  }

  Int result = 0;
  while (true) {
    const auto time = invoke_bfs();
    if (time <= 0) {
      break;
    }
    result += time;
  }
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}