// https://www.acmicpc.net/problem/16985
// Maaaaaaaaaze


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>
#include <cmath>
#include <stdexcept>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  // #0
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "

  // #1
  "1 1 1 1 1 "
  "1 0 0 0 1 "
  "1 0 0 0 1 "
  "1 0 0 0 1 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 1 1 1 0 "
  "0 1 0 1 0 "
  "0 1 1 1 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 1 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 1 1 1 0 "
  "0 1 0 1 0 "
  "0 1 1 1 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "1 0 0 0 1 "
  "1 0 0 0 1 "
  "1 0 0 0 1 "
  "1 1 1 1 1 "

  // #2
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 1 1 1 1 "

  // #3
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "
  "1 1 1 1 1 "

  // #4
  "0 0 0 1 0 "
  "0 0 0 0 0 "
  "1 0 1 1 1 "
  "0 0 0 1 0 "
  "0 0 1 0 0 "
  "0 1 0 0 0 "
  "1 1 0 0 0 "
  "1 0 0 1 0 "
  "0 1 1 1 0 "
  "0 1 0 1 0 "
  "0 0 1 0 0 "
  "1 0 0 0 0 "
  "0 1 0 0 0 "
  "0 0 1 0 0 "
  "1 1 1 0 0 "
  "1 0 0 0 1 "
  "1 0 0 0 0 "
  "0 0 1 0 1 "
  "0 1 1 0 0 "
  "0 1 0 0 0 "
  "0 0 0 1 0 "
  "1 0 0 0 0 "
  "0 0 1 0 0 "
  "0 1 0 0 1 "
  "0 1 0 0 0 "

  // #5
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "1 0 0 0 1 "
  "0 0 1 0 0 "
  "0 0 1 1 1 "
  "0 1 0 0 1 "
  "0 0 0 0 1 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 1 0 0 0 "
  "0 1 0 0 1 "
  "1 0 0 1 0 "
  "0 0 0 1 0 "
  "0 1 1 0 0 "
  "0 1 0 0 0 "
  "1 0 1 0 0 "
  "0 0 0 0 0 "
  "1 0 0 0 0 "
  "0 0 0 1 0 "
  "1 0 0 0 0 "
  "0 0 0 1 0 "
  "0 0 0 0 1 "
  "1 1 0 0 0 "
  "1 0 0 1 1 "
  "1 0 0 0 0 "

  // #6
  "1 1 0 0 0 "
  "0 0 0 0 1 "
  "0 0 1 0 0 "
  "0 0 0 0 0 "
  "0 0 0 0 0 "
  "0 0 1 1 1 "
  "1 0 0 0 0 "
  "0 0 1 0 0 "
  "0 0 1 1 1 "
  "0 0 1 0 0 "
  "0 0 0 0 0 "
  "0 0 1 0 1 "
  "0 0 0 0 0 "
  "0 0 0 1 0 "
  "0 0 1 0 1 "
  "0 0 1 0 0 "
  "1 0 0 0 0 "
  "0 0 1 1 0 "
  "1 0 1 0 0 "
  "0 0 1 0 1 "
  "0 0 1 1 0 "
  "1 1 0 1 1 "
  "0 0 0 0 1 "
  "0 1 0 1 0 "
  "0 1 0 0 0 "

  // #7
  "0 0 1 0 0 "
  "0 0 0 0 0 "
  "1 1 0 0 0 "
  "0 0 1 0 0 "
  "1 1 1 0 0 "
  "0 0 0 0 1 "
  "1 0 0 0 0 "
  "0 1 0 0 1 "
  "0 0 0 0 0 "
  "0 1 0 1 0 "
  "1 0 0 0 1 "
  "1 1 1 1 1 "
  "1 1 0 0 0 "
  "0 0 0 1 0 "
  "0 0 0 1 0 "
  "0 0 0 1 1 "
  "0 0 1 0 0 "
  "0 1 1 1 0 "
  "1 0 0 0 0 "
  "0 1 1 0 1 "
  "0 1 0 0 0 "
  "0 0 0 1 0 "
  "1 0 0 0 0 "
  "0 0 0 1 0 "
  "0 0 0 1 0 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "12 -1 12 12 22 -1 16 18 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


template <typename M, typename C>
static void fill_matrix(M& matrix, const C& value) {
  for (auto& row : matrix) {
    row.fill(value);
  }
}


using Int = int;

constexpr Int WIDTH_LIMIT = 5;
constexpr Int DISTANCE_MIN = 3 * (WIDTH_LIMIT - 1);
constexpr auto DISTANCE_INF = std::numeric_limits<Int>::max() / 2;

using Arr1 = std::array<Int, WIDTH_LIMIT>; // col
using Arr2 = std::array<Arr1, WIDTH_LIMIT>; // row
using Arr3 = std::array<Arr2, WIDTH_LIMIT>; // level

static Arr3 original_boards, reordered_boards;

static std::array<bool, WIDTH_LIMIT> reordering_dfs_visit_checks;

struct Coordinate {
  Int level, row, col;

  bool is_valid() const {
    return 0 <= level && level < WIDTH_LIMIT
      && 0 <= row && row < WIDTH_LIMIT
      && 0 <= col && col < WIDTH_LIMIT;
  }

  bool operator == (const Coordinate& other) const {
    return this->level == other.level && this->row == other.row && this->col == other.col;
  }

  Coordinate operator + (const Coordinate& other) const {
    return { this->level + other.level, this->row + other.row, this->col + other.col };
  }
};

static std::queue<Coordinate> coordinate_queue;
static Arr3 bfs_visit_orders;


static Int& at(Arr3& arr3, const Coordinate& coordinate) {
  return arr3.at(coordinate.level).at(coordinate.row).at(coordinate.col);
}


static Int bfs() {
  constexpr Coordinate destination = { WIDTH_LIMIT - 1, WIDTH_LIMIT - 1, WIDTH_LIMIT - 1 };

  while (!coordinate_queue.empty()) {
    const auto coordinate = coordinate_queue.front();
    coordinate_queue.pop();

    if (!coordinate.is_valid() || !at(reordered_boards, coordinate)) {
      continue;
    }

    const auto& visit_order = at(bfs_visit_orders, coordinate);

    if (coordinate == destination) {
      while (!coordinate_queue.empty()) {
        coordinate_queue.pop();
      }
      return visit_order - 1;
    }

    constexpr std::array<Coordinate, 6> offsets {
      Coordinate
      { -1, 0, 0 }, { 1, 0, 0 },
      { 0, -1, 0 }, { 0, 1, 0 },
      { 0, 0, -1 }, { 0, 0, 1 }
    };

    for (const auto& offset : offsets) {
      const auto next_coordinate = coordinate + offset;
      if (!next_coordinate.is_valid()) {
        continue;
      }

      auto& next_visit_order = at(bfs_visit_orders, next_coordinate);
      if (next_visit_order || !at(reordered_boards, next_coordinate)) {
        continue;
      }
      next_visit_order = visit_order + 1;
      coordinate_queue.emplace(next_coordinate);
    }
  }

  return DISTANCE_INF;
}


static Int invoke_bfs() {
  while (!coordinate_queue.empty()) {
    coordinate_queue.pop();
  }
  for (auto& level : bfs_visit_orders) {
    for (auto& row : level) {
      row.fill(0);
    }
  }
  constexpr Coordinate starting = { 0, 0, 0 };
  coordinate_queue.emplace(starting);
  at(bfs_visit_orders, starting) = 1;
  return bfs();
}


static Int reordering_dfs(Int level = WIDTH_LIMIT) {
  if (level <= 0) {
    return invoke_bfs();
  }

  const auto reordered_boards_index = WIDTH_LIMIT - level;
  Int result = DISTANCE_INF;
  for (
    Int original_boards_index = 0;
    original_boards_index < WIDTH_LIMIT;
    ++original_boards_index
    ) {
    auto& visit_check = reordering_dfs_visit_checks.at(original_boards_index);
    if (visit_check) {
      continue;
    }
    visit_check = true;
    reordered_boards.at(reordered_boards_index) = original_boards.at(original_boards_index);
    result = std::min(result, reordering_dfs(level - 1));
    if (result == DISTANCE_MIN) {
      return DISTANCE_MIN;
    }
    visit_check = false;
  }
  return result;
}


static void rotate_cw(Arr2& board) {
  static Arr2 temp;

  for (Int board_row_index = 0; board_row_index < WIDTH_LIMIT; ++board_row_index) {
    const auto temp_col_index = WIDTH_LIMIT - 1 - board_row_index;
    for (Int board_col_index = 0; board_col_index < WIDTH_LIMIT; ++board_col_index) {
      const auto temp_row_index = board_col_index;
      temp.at(temp_row_index).at(temp_col_index) = board.at(board_row_index).at(board_col_index);
    }
  }

  board = temp;
}


static Int rotating_dfs(Int level = WIDTH_LIMIT) {
  if (level <= 0) {
    reordering_dfs_visit_checks.fill(false);
    return reordering_dfs();
  }

  Int result = DISTANCE_INF;
  const auto board_level = WIDTH_LIMIT - level;
  for (Int rotation = 0; rotation < 4; ++rotation) {
    result = std::min(result, rotating_dfs(level - 1));
    if (result == DISTANCE_MIN) {
      return DISTANCE_MIN;
    }
    rotate_cw(original_boards.at(board_level));
  }
  return result;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  reordering_dfs_visit_checks.fill(false);

  for (Int level_count = 0; level_count < WIDTH_LIMIT; ++level_count) {
    for (Int row_count = 0; row_count < WIDTH_LIMIT; ++row_count) {
      for (Int col_count = 0; col_count < WIDTH_LIMIT; ++col_count) {
        Int number = -1;
        in >> number;
        if (!in) {
          return false;
        }
        assert(number >= 0);

        auto& block = original_boards.at(level_count).at(row_count).at(col_count);
        block = number;
      }
    }
  }

  const Int rotating_dfs_result = rotating_dfs();
  out << (rotating_dfs_result >= DISTANCE_INF ? -1 : rotating_dfs_result) << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = std::numeric_limits<int>::max();
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}