// https://www.acmicpc.net/problem/16197
// 두 동전


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "1 2 "
  "oo "

  "6 2 "
  ".# "
  ".# "
  ".# "
  "o# "
  "o# "
  "## "

  "6 2 "
  ".. "
  ".. "
  ".. "
  "o# "
  "o# "
  "## "

  "5 3 "
  "### "
  ".o. "
  "### "
  ".o. "
  "### "

  "5 3 "
  "### "
  ".o. "
  "#.# "
  ".o. "
  "### "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "1 4 3 -1 3 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;


using Int = int;

static constexpr Int WIDTH_LIMIT = 22;

using BoolArr1 = std::array<bool, WIDTH_LIMIT>;
using BoolArr2 = std::array<BoolArr1, WIDTH_LIMIT>;
using BoolArr3 = std::array<BoolArr2, WIDTH_LIMIT>;
using BoolArr4 = std::array<BoolArr3, WIDTH_LIMIT>;

struct Coordinate {
  Int row, col;

  bool operator == (const Coordinate& other) const {
    return row == other.row && col == other.col;
  }

  Coordinate operator + (const Coordinate& other) const {
    return { row + other.row, col + other.col };
  }
};

struct State {
  Coordinate coin0, coin1;
};

static std::array<std::array<char, WIDTH_LIMIT>, WIDTH_LIMIT> board;
static Coordinate board_size;
static std::array<Coordinate, 2> initial_state;
static Int initial_state_size;

static std::queue<State> que;
static BoolArr4 visit_checks;


static void visit_checks_clear() {
  for (auto& coin0_row : visit_checks) {
    for (auto& coin0_col : coin0_row) {
      for (auto& coin1_row : coin0_col) {
        coin1_row.fill(false);
      }
    }
  }
}


static bool& visit_checks_at(const State& s) {
  return visit_checks.at(s.coin0.row).at(s.coin0.col).at(s.coin1.row).at(s.coin1.col);
}


static char& board_at(Int row, Int col) {
  return board.at(row).at(col);
}


static char& board_at(const Coordinate& c) {
  return board_at(c.row, c.col);
}


static State move(const State& s, const Coordinate& offset) {
  State result = { s.coin0 + offset, s.coin1 + offset };
  if (board_at(result.coin0) == '#') {
    if (board_at(result.coin1) == '#') {
      return s;
    }
  }
  switch ((board_at(result.coin0) == '#') << 1 | (board_at(result.coin1) == '#')) {
    case 0b00 : return result;
    case 0b11 : return s;
    case 0b01 : return result.coin0 == s.coin1 ? s : State { result.coin0, s.coin1 };
    case 0b10 : return result.coin1 == s.coin0 ? s : State { s.coin0, result.coin1 };
    default : throw std::logic_error("switch error");
  }
}


static bool is_winning(const State& s) {
  return (board_at(s.coin0) == '*') ^ (board_at(s.coin1) == '*');
}


static bool is_game_over(const State& s) {
  return board_at(s.coin0) == '*' || board_at(s.coin1) == '*';
}


static Int invoke_bfs() {
  while (!que.empty()) {
    que.pop();
  }
  visit_checks_clear();

  que.push({ initial_state.at(0), initial_state.at(1) });
  visit_checks_at(que.front()) = true;
  Int result = 0;
  Int level_element_count = 1;
  Int next_level_element_count = 0;
  bool win = false;

  while (result <= 10 && !que.empty()) {
    const auto state = que.front();
    que.pop();

    if (is_winning(state)) {
      win = true;
      break;
    }

    constexpr std::array<Coordinate, 4> offsets {
      Coordinate { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 }
    };

    if (!is_game_over(state)) for (const auto& offset : offsets) {
      const auto next_state = move(state, offset);
      auto& visit_check = visit_checks_at(next_state);
      if (!visit_check) {
        visit_check = true;
        ++next_level_element_count;
        que.emplace(next_state);
      }
    }

    if (--level_element_count <= 0) {
      level_element_count = next_level_element_count;
      next_level_element_count = 0;
      ++result;
    }
  }

  return win ? result : -1;
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  for (auto& row : board) {
    row.fill('*'); // outside of board
  }
  initial_state_size = 0;

  in >> board_size.row >> board_size.col;
  if (!in) {
    return false;
  }

  for (Int row = 1; row <= board_size.row; ++row) {
    std::string one_row;
    in >> one_row;
    assert((long long)(one_row.size()) == board_size.col);
    for (Int col = 1; col <= board_size.col; ++col) {
      auto& ch = one_row.at(col - 1);
      auto& cell = board_at(row, col);
      if (ch == 'o') {
        cell = '.';
        initial_state.at(initial_state_size++) = { row, col };
      } else {
        cell = ch;
      }
    }
  }

  out << invoke_bfs() << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION : test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}