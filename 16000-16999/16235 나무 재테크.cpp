﻿// https://www.acmicpc.net/problem/16235
// 나무 재테크


#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <cmath>
#include <cstring>
#include <stdexcept>

#include <array>
#include <bitset>
#include <vector>
#include <stack>
#include <queue>
#include <set>
#include <unordered_map>


enum class TestFormat { ONE, GIVEN, CONDITION };


#ifdef ONLINE_JUDGE

static std::istream& in = std::cin;
static std::ostream& out = std::cout;

#else // ONLINE_JUDGE

static std::istringstream test_in(
  // The number of test cases given by the text

  // Test cases given by the text
  "1 1 1 "
  "1 "
  "1 1 1 "

  "1 1 4 "
  "1 "
  "1 1 1 "

  "5 2 1 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  "5 2 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  "5 2 3 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  "5 2 4 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  "5 2 5 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  "5 2 6 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 3 2 3 2 "
  "2 1 3 "
  "3 2 3 "

  // Custom test cases

);

static std::istringstream test_answer(
  // Given by the text
  "1 0 2 15 13 13 13 85 "

  // Custom

);

static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;

#endif // ONLINE_JUDGE


static constexpr TestFormat TEST_FORMAT = TestFormat::ONE;

using Int = int32_t;

static constexpr Int WIDTH_LIMIT = 12;


struct Coord2 {
  Int y = 0;
  Int x = 0;


  Coord2 operator + (const Coord2& other) const {
    return { y + other.y, x + other.x };
  }


  Coord2& operator += (const Coord2& other) {
    return *this = *this + other;
  }
};


static constexpr std::array<Coord2, 8> offsets {
  Coord2
  { -1, -1 }, { -1, 0 }, { -1, 1 },
  { 0, -1 }, { 0, 1 },
  { 1, -1 }, { 1, 0 }, { 1, 1 }
};


template <typename T>
using GridBase = std::array<std::array<T, WIDTH_LIMIT>, WIDTH_LIMIT>;


template <typename T>
struct Array2 : public GridBase<T> {

  using GridBase<T>::at;
  using GridBase<T>::fill;


  T& at(Int row, Int col) {
    return at(row).at(col);
  }


  T& at(const Coord2& c) {
    return at(c.y, c.x);
  }


  void fill2(T t) {
    for (auto& row : *this) {
      row.fill(t);
    }
  }
};


struct Cell {
  Int nutrient = 5;
  Int supplement = 0;
  std::deque<Int> trees;


  bool is_valid() const {
    return supplement != 0;
  }


  void supply() {
    nutrient += supplement;
  }


  void grow_and_die() {
    Int next_size = 0;
    while (next_size < trees.size()) {
      auto& tree = trees.at(next_size);
      if (nutrient < tree) {
        break;
      }
      nutrient -= tree++;
      ++next_size;
    }
    while (next_size < trees.size()) {
      nutrient += trees.back() / 2;
      trees.pop_back();
    }
  }


  Int get_breedable_tree_total() const {
    return Int(std::count_if(trees.cbegin(), trees.cend(), [](const auto& age) {
      return age % 5 == 0;
    }));
  }
};


static Int width; // N
static Int initial_tree_total; // M
static Int end_year; // K
static Array2<Cell> grid;


template <typename F>
static void for_valid_grid(F f) {
  for (Int row_count = 1; row_count <= width; ++row_count) {
    for (Int col_count = 1; col_count <= width; ++col_count) {
      f(Coord2 { row_count, col_count });
    }
  }
}


static void precalculate() {
  // No op
}


static bool solve_case(int test_case_count) {
  width = initial_tree_total = end_year -1;
  grid.fill2(Cell());

  in >> width >> initial_tree_total >> end_year;
  if (!in) {
    return false;
  }
  assert(0 <= width && 0 <= initial_tree_total && end_year);

  for_valid_grid([](const Coord2& c) {
    in >> grid.at(c).supplement;
  });

  for (Int tree_count = 0; tree_count < initial_tree_total; ++tree_count) {
    Coord2 coord { -1, -1 };
    Int age = -1;
    in >> coord.y >> coord.x >> age;
    assert(0 <= coord.y && 0 <= coord.x && 0 <= age);
    grid.at(coord).trees.push_back(age);
    assert(grid.at(coord).trees.size() == 1);
  }

  for (Int year = 0; year < end_year; ++year) {
    for_valid_grid([](const Coord2& c) {
      grid.at(c).grow_and_die();
    });
    for_valid_grid([](const Coord2& c) {
      const auto breedable_tree_total = grid.at(c).get_breedable_tree_total();
      for (const auto& offset : offsets) {
        auto& there = grid.at(c + offset);
        if (!there.is_valid()) {
          continue;
        }
        for (Int tree_count = 0; tree_count < breedable_tree_total; ++tree_count) {
          there.trees.push_front(1);
        }
      }
      grid.at(c).supply();
    });
  }

  Int result = 0;
  for_valid_grid([&](const Coord2& c) {
    result += Int(grid.at(c).trees.size());
  });
  out << result << "\n";

  return true;
}


static void solve() {
  precalculate();

  int test_case_total = -1;
  switch (TEST_FORMAT) {
    case TestFormat::GIVEN: in >> test_case_total; break;
    case TestFormat::ONE: test_case_total = 1; break;
    case TestFormat::CONDITION: test_case_total = std::numeric_limits<int>::max(); break;
  }
#ifndef ONLINE_JUDGE
  test_case_total = std::numeric_limits<int>::max();
#endif // ONLINE_JUDGE

  for (
    int test_case_count = 0;
    test_case_count < test_case_total && solve_case(test_case_count);
    ++test_case_count
    );
}


static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  int token_count = 0;
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "#" << token_count << "\n"
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
    ++token_count;
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}


int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}