// https://www.acmicpc.net/problem/8958

#include <iostream>
#include <sstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <array>

#ifndef ONLINE_JUDGE
static std::istringstream test_in(
  // The number of test cases (5 is given by the text)
  "5\n"
  // Test cases given by the text
  "OOXXOXXOOO\n"
  "OOXXOOXXOO\n"
  "OXOXOXOXOXOXOX\n"
  "OOOOOOOOOO\n"
  "OOOOXOOOOXOOOOX\n"
);
static std::istringstream test_answer(
  // Given by the text
  "10\n"
  "9\n"
  "7\n"
  "55\n"
  "30\n"
);
static std::stringstream test_out;
static std::istream& in = test_in;
static std::ostream& out = test_out;
#else // ONLINE_JUDGE
static std::istream& in = std::cin;
static std::ostream& out = std::cout;
#endif // ONLINE_JUDGE

enum class TestFormat { ONE, GIVEN, CONDITION };
static constexpr TestFormat TEST_FORMAT = TestFormat::GIVEN;

static void precalculate() {
  // No op
}

static bool solve_case() {
  std::string ox_string;
  in >> ox_string;
  if (!in) {
    return false;
  }

  int sum = 0;
  int consequent_o = 0;
  for (const auto& ox_string_el : ox_string) {
    if (ox_string_el == 'O') {
      ++consequent_o;
    } else {
      consequent_o = 0;
    }
    sum += consequent_o;
  }
  out << sum << "\n";

  return true;
}

static void solve() {
  precalculate();

  switch (TEST_FORMAT) {
    case TestFormat::GIVEN : {
      int test_case_count = 0;
      in >> test_case_count;
      for (int i1 = 0; i1 < test_case_count && solve_case(); ++i1) ;
    } break;

    case TestFormat::ONE : {
#ifndef ONLINE_JUDGE
      while (solve_case()) ;
#else // ONLINE_JUDGE
      solve_case();
#endif
    } break;

    case TestFormat::CONDITION : {
      while (solve_case()) ;
    } break;
  }
}

static void test() {
#ifndef ONLINE_JUDGE
  std::cerr << "Test start\n\n";
  while (test_answer) {
    if (!test_out) {
      std::cerr << "Not all test cases have been tried\n";
      break;
    }
    std::string answer_string, out_string;
    test_answer >> answer_string;
    test_out >> out_string;
    if (answer_string != out_string) {
      std::cerr
        << "expected: \"" << answer_string << "\"\n"
        << "actual:   \"" << out_string << "\"\n\n";
    }
  }
  if (test_out) {
    std::cerr << "Too much output\n";
  }
  std::cerr << "Test end\n";
#endif // ONLINE_JUDGE
}

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);

  solve();
  test();
}